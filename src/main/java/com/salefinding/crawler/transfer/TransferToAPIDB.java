package com.salefinding.crawler.transfer;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.api.*;
import com.salefinding.models.crawler.ClCategory;
import com.salefinding.models.crawler.ClItem;
import com.salefinding.models.crawler.ClItemImage;
import com.salefinding.models.crawler.ClItemStock;
import com.salefinding.models.crawler.gen.ClItemCountryDetailGen;
import com.salefinding.repositories.ApiRepoFactory;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;

public class TransferToAPIDB {

  private Logger logger = LoggerFactory.getLogger(TransferToAPIDB.class);

  private CrawlerRepoFactory crawlerRepoFactory;

  private ApiRepoFactory apiRepoFactory;

  private static TransferToAPIDB transferToAPIDB;

  public static TransferToAPIDB getInstance() {
    if (transferToAPIDB == null) {
      synchronized (TransferToAPIDB.class) {
        if (transferToAPIDB == null) {
          transferToAPIDB = new TransferToAPIDB();
        }
      }
    }
    return transferToAPIDB;
  }

  private TransferToAPIDB() {
    crawlerRepoFactory = CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER);
    apiRepoFactory = ApiRepoFactory.getInstance(Constants.DB_API);
  }

  public synchronized void transfer(int batchSize) {
    transfer(batchSize, null);
  }

  public synchronized void transfer(int batchSize, Long brandId) {
    transferCategories(batchSize, brandId);
    transferItems(batchSize, brandId);
  }

  private synchronized void transferCategories(int batchSize, Long brandId) {
    boolean stillHaveRecords;
    Long lastId = 0L;
    try {
      logger.info("START transfer Categories for brandId = {}!", brandId);
      apiRepoFactory.getCategoryRepository().getServer().beginTransaction();

      logger.info("Set unavailable for all categories for brandId = {}!", brandId);
      apiRepoFactory.getCategoryRepository().setUnAvailableForAll(brandId);

      do {
        stillHaveRecords = false;
        List<ClCategory> clCategories;
        if (brandId != null) {
          clCategories =
              crawlerRepoFactory.getCategoryRepo().getAllAvailableCats(batchSize, lastId, brandId);
        } else {
          clCategories =
              crawlerRepoFactory.getCategoryRepo().getAllAvailableCats(batchSize, lastId);
        }

        if (clCategories != null && clCategories.size() > 0) {
          stillHaveRecords = true;
          for (ClCategory clCategory : clCategories) {
            logger.info(
                "Transferring category {}-{}-{}-{}.",
                clCategory.getBrand().getName(),
                clCategory.getCat1(),
                clCategory.getCat2(),
                clCategory.getCat3());
            lastId = clCategory.getId();
            ApCategory apCategory =
                apiRepoFactory.getCategoryRepository().findByUuid(clCategory.getUuid());
            if (apCategory == null) { // not exist category
              apCategory =
                  new ApCategory(
                      clCategory.getBrandId(),
                      clCategory.getCat1(),
                      clCategory.getCat2(),
                      clCategory.getCat3(),
                      clCategory.getCat4(),
                      clCategory.getCat5());
              apCategory.setUuid(clCategory.getUuid());
            }

            apCategory.setAvailable(clCategory.getAvailable());

            apiRepoFactory.getCategoryRepository().saveCategory(apCategory);
          }
        }
      } while (stillHaveRecords);

      apiRepoFactory.getCategoryRepository().getServer().commitTransaction();
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, "Error when transferring categories", e, brandId);
      apiRepoFactory.getCategoryRepository().getServer().rollbackTransaction();
    } finally {
      apiRepoFactory.getCategoryRepository().getServer().endTransaction();
    }

    logger.info("END transfer Categories!");
  }

  private synchronized void transferItems(int batchSize, Long brandId) {
    boolean stillHaveRecords;
    Long lastId = 0L;
    try {
      logger.info("START transfer Items for brandId = {}!", brandId);
      apiRepoFactory.getItemRepository().getServer().beginTransaction();

      logger.info("Set unavailable for all items for brandId = {}!", brandId);
      apiRepoFactory.getItemRepository().setUnavailableForAllItems(brandId);

      logger.info("Set unavailable for all item details for brandId = {}!", brandId);
      apiRepoFactory.getItemRepository().setUnavailableForAllItemDetails(brandId);

      do {
        stillHaveRecords = false;
        List<ClItem> clItems;
        if (brandId != null) {
          clItems = crawlerRepoFactory.getItemRepo().getAllItems(batchSize, lastId, brandId);
        } else {
          clItems = crawlerRepoFactory.getItemRepo().getAllItems(batchSize, lastId);
        }

        if (clItems != null && clItems.size() > 0) {
          stillHaveRecords = true;
          for (ClItem clItem : clItems) {
            logger.info("Transferring item {}-{}.", clItem.getId(), clItem.getName());
            lastId = clItem.getId();

            // Update max sale rate
            clItem.setMaxSaleRate(
                clItem.getItemCountryDetailList().stream()
                    .max(Comparator.comparingInt(ClItemCountryDetailGen::getSaleRate))
                    .orElse(clItem.getItemCountryDetailList().get(0))
                    .getSaleRate());

            ApCategory apCategory =
                apiRepoFactory.getCategoryRepository().findByUuid(clItem.getCategory().getUuid());

            if (apCategory == null) {
              crawlerRepoFactory
                  .getErrorSummaryRepo()
                  .saveError(
                      Constants.RUN_UID, new Exception("Cannot find ApCategory for item."), clItem);
              continue;
            }

            ApItem apItem = apiRepoFactory.getItemRepository().findItemByUuid(clItem.getUuid());
            if (apItem == null) {
              apItem = new ApItem(clItem.getBrandId());
              apItem.setUuid(clItem.getUuid());
            }
            apItem.setBrandId(clItem.getBrandId());
            apItem.setCode(clItem.getCode());
            apItem.setName(clItem.getName());
            apItem.setCategoryId(apCategory.getId());
            apItem.setMaxSaleRate(clItem.getMaxSaleRate());
            apItem.setAvailable(clItem.getAvailable());

            apiRepoFactory.getItemRepository().save(apItem);
            crawlerRepoFactory.getItemRepo().save(clItem);

            final Long apItemId = apItem.getId();

            // transfer item details
            clItem
                .getItemCountryDetailList()
                .forEach(
                    clItemCountryDetail -> {
                      logger.debug(
                          "Transferring item detail {}-{}.",
                          clItemCountryDetail.getId(),
                          clItemCountryDetail.getName());
                      ApItemCountryDetail apDetail =
                          apiRepoFactory
                              .getItemRepository()
                              .findItemDetailByUuid(clItemCountryDetail.getUuid());

                      if (apDetail == null) {
                        apDetail =
                            new ApItemCountryDetail(
                                apItemId,
                                clItemCountryDetail.getBrandId(),
                                clItemCountryDetail.getCountryId());
                        apDetail.setUuid(clItemCountryDetail.getUuid());
                      }

                      apDetail.setCategoryId(apCategory.getId());
                      apDetail.setCode(clItemCountryDetail.getCode());
                      apDetail.setName(clItemCountryDetail.getName());
                      apDetail.setImage(clItemCountryDetail.getImage());
                      apDetail.setOldPrice(clItemCountryDetail.getOldPrice());
                      apDetail.setPrice(clItemCountryDetail.getPrice());
                      apDetail.setSpecialPrice(clItemCountryDetail.getSpecialPrice());
                      apDetail.setSaleRate(clItemCountryDetail.getSaleRate());
                      apDetail.setUrl(clItemCountryDetail.getUrl());
                      apDetail.setAvailable(clItemCountryDetail.getAvailable());

                      apiRepoFactory.getItemRepository().save(apDetail);

                      final Long apItemDetailId = apDetail.getId();

                      try {
                        // transfer item_stock
                        logger.debug(
                            "Delete all stocks of item {}-{}.", clItem.getId(), clItem.getName());
                        apiRepoFactory.getItemRepository().deleteAllStocksOfItem(apItemDetailId);
                        logger.debug(
                            "Transferring item stocks of item {}-{}.",
                            clItem.getId(),
                            clItem.getName());
                        for (ClItemStock clItemStock : clItemCountryDetail.getItemStockList()) {
                          ApItemStock apItemStock = new ApItemStock(apItemDetailId);
                          apItemStock.setColorCode(clItemStock.getColorCode());
                          apItemStock.setSizeCode(clItemStock.getSizeCode());
                          apItemStock.setStock(clItemStock.getStock());
                          apItemStock.setUuid(clItemStock.getUuid());

                          apiRepoFactory.getItemRepository().save(apItemStock);
                        }
                      } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                            .getErrorSummaryRepo()
                            .saveError(Constants.RUN_UID, ex);
                      }

                      // transfer item_image
                      try {
                        logger.debug(
                            "Delete all images of item {}-{}.", clItem.getId(), clItem.getName());
                        apiRepoFactory.getItemRepository().deleteAllImagesOfItem(apItemDetailId);
                        logger.debug(
                            "Transferring item images of item {}-{}.",
                            clItem.getId(),
                            clItem.getName());
                        for (ClItemImage clItemImage : clItemCountryDetail.getItemImageList()) {
                          ApItemImage apItemImage = new ApItemImage(apItemDetailId);
                          apItemImage.setThumbUrl(clItemImage.getThumbUrl());
                          apItemImage.setUrl(clItemImage.getUrl());
                          apItemImage.setDescription(clItemImage.getDescription());
                          apItemImage.setUuid(clItemImage.getUuid());

                          apiRepoFactory.getItemRepository().save(apItemImage);
                        }
                      } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                            .getErrorSummaryRepo()
                            .saveError(Constants.RUN_UID, ex);
                      }
                    });
          }
        }

      } while (stillHaveRecords);

      apiRepoFactory.getItemRepository().getServer().commitTransaction();
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, "Error when transferring items", e, brandId);
      apiRepoFactory.getItemRepository().getServer().rollbackTransaction();
    } finally {
      apiRepoFactory.getItemRepository().getServer().endTransaction();
    }

    logger.info("END transfer Items!");
  }
}
