package com.salefinding.crawler.transfer.wp;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.ClItem;
import com.salefinding.models.crawler.ClItemCountryDetail;
import com.salefinding.models.crawler.ClWpItem;
import com.salefinding.models.crawler.ClWpItemDetail;
import com.salefinding.models.crawler.gen.ClItemCountryDetailGen;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

public class UpdatePrice {

  private Logger logger = LoggerFactory.getLogger(UpdatePrice.class);

  private CrawlerRepoFactory crawlerRepoFactory;

  private static UpdatePrice transferToWP;

  public static UpdatePrice getInstance() {
    if (transferToWP == null) {
      synchronized (UpdatePrice.class) {
        if (transferToWP == null) {
          transferToWP = new UpdatePrice();
        }
      }
    }
    return transferToWP;
  }

  private UpdatePrice() {
    crawlerRepoFactory = CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER);
  }

  public static void main(String[] args) {
    UpdatePrice.getInstance().run(1000);
  }

  public void run(int batchSize) {
    updateProductPrices(batchSize);
    updateVariantPrices(batchSize);
  }

  private synchronized void updateProductPrices(int batchSize) {
    boolean stillHaveRecords;
    Long lastId = 0L;

    try {
      do {
        stillHaveRecords = false;
        List<ClWpItem> wpItems = crawlerRepoFactory.getWpRepository().getWpItems(batchSize, lastId);

        if (wpItems.size() > 0) {
          stillHaveRecords = true;
          lastId = wpItems.get(wpItems.size() - 1).getId();

          wpItems.forEach(
              wpItem -> {
                if (wpItem.getPrice() == null
                    || BigDecimal.ZERO.compareTo(wpItem.getPrice()) == 0) {
                  ClItem clItem = wpItem.getClItem();

                  ClItemCountryDetail maxSaleDetail =
                      clItem.getItemCountryDetailList().stream()
                          .filter(ClItemCountryDetailGen::getAvailable)
                          .max(Comparator.comparingInt(ClItemCountryDetailGen::getSaleRate))
                          .orElse(clItem.getItemCountryDetailList().get(0));

                  wpItem.setPrice(maxSaleDetail.getPrice());

                  logger.info("Update price {} for product {}!", wpItem.getPrice(), wpItem.getId());

                  crawlerRepoFactory.getWpRepository().save(wpItem);
                }
              });
        }
      } while (stillHaveRecords);

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  private synchronized void updateVariantPrices(int batchSize) {
    boolean stillHaveRecords;
    Long lastId = 0L;

    try {
      do {
        stillHaveRecords = false;
        List<ClWpItemDetail> wpItemDetails =
            crawlerRepoFactory.getWpRepository().getWpItemDetails(batchSize, lastId);

        if (wpItemDetails.size() > 0) {
          stillHaveRecords = true;
          lastId = wpItemDetails.get(wpItemDetails.size() - 1).getId();

          wpItemDetails.forEach(
              wpItemDetail -> {
                if (wpItemDetail.getPrice() == null
                    || BigDecimal.ZERO.compareTo(wpItemDetail.getPrice()) == 0) {
                  ClItemCountryDetail itemCountryDetail = wpItemDetail.getClItemCountryDetail();

                  wpItemDetail.setPrice(itemCountryDetail.getPrice());

                  logger.info(
                      "Update price {} for variant {}!",
                      wpItemDetail.getPrice(),
                      wpItemDetail.getId());

                  crawlerRepoFactory.getWpRepository().save(wpItemDetail);
                }
              });
        }
      } while (stillHaveRecords);

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }
}
