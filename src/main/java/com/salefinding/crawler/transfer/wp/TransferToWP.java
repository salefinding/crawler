package com.salefinding.crawler.transfer.wp;

import com.fasterxml.jackson.databind.JsonNode;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.*;
import com.salefinding.models.crawler.gen.*;
import com.salefinding.pojo.wp.*;
import com.salefinding.repositories.CrawlerRepoFactory;
import com.salefinding.utils.SFObjectMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TransferToWP {

  private Logger logger = LoggerFactory.getLogger(TransferToWP.class);

  private final CrawlerRepoFactory crawlerRepoFactory;

  private final Map<Long, BigDecimal> currencies = new HashMap<>();

  private final Map<String, String> keyValues;

  private long transferItemLastId = 0;

  private long brandId;

  private String brandName;

  public static TransferToWP getInstance(long brandId) {
    return new TransferToWP(brandId);
  }

  private TransferToWP(long brandId) {
    crawlerRepoFactory = CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER);
    initCurrencies();
    keyValues =
        crawlerRepoFactory.getCommonRepository().getAllKeyValues().stream()
            .collect(Collectors.toMap(ClKeyValueGen::getKey, ClKeyValueGen::getValue));
    this.brandId = brandId;
    this.brandName = crawlerRepoFactory.getCommonRepository().getBrandById(brandId).getShortCode();
  }

  private void initCurrencies() {
    List<ClCountry> countries = crawlerRepoFactory.getCommonRepository().getAllCountries();
    countries.forEach(
        country -> currencies.put(country.getId(), country.getCurrency().getVndExchangeRate()));
  }

  public void transfer(int batchSize) {
    try {
      if (brandId < 1) {
        return;
      }
      Thread.currentThread().setName(brandName);
      transferCategories();
      transferItems(batchSize);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  private void transferCategories() throws Exception {
    try {
      logger.info("START transfer Categories for brandId = {}!", brandId);

      Map<WpCategory, List<ClWpCategory>> wpCategoryMap = new HashMap<>();
      crawlerRepoFactory
          .getWpRepository()
          .getWpCategories()
          .forEach(
              clWpCategory ->
                  wpCategoryMap
                      .computeIfAbsent(
                          clWpCategory.getWpCategory(), wpCategory -> new ArrayList<>())
                      .add(clWpCategory));

      List<ClCategory> clCategories =
          crawlerRepoFactory.getCategoryRepo().getAllAvailableCats(brandId);

      if (clCategories != null && clCategories.size() > 0) {
        List<ClWpCategory> clWpCategories =
            crawlerRepoFactory
                .getWpRepository()
                .getWpCategories(
                    clCategories.stream().map(ClCategoryGen::getId).collect(Collectors.toList()));

        Map<ClCategory, Set<ClWpCategory>> clCategoryMap = new HashMap<>();
        for (ClWpCategory clWpCategory : clWpCategories) {
          if (!clCategoryMap.containsKey(clWpCategory.getClCategory())) {
            clCategoryMap.put(clWpCategory.getClCategory(), new HashSet<>());
          }
          clCategoryMap.get(clWpCategory.getClCategory()).add(clWpCategory);
        }

        for (ClCategory clCategory : clCategories) {
          logger.info(
              "Transferring category {}-{}-{}.",
              clCategory.getBrand().getName(),
              clCategory.getCat1(),
              clCategory.getCat2());

          if (!clCategoryMap.containsKey(clCategory)) {
            clCategoryMap.put(clCategory, new HashSet<>());
          }

          transferCategory(clCategory, clCategoryMap.get(clCategory), wpCategoryMap);
        }
      }

    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, "Error when transferring categories", e, brandId);
      throw e;
    }

    logger.info("END transfer Categories!");
    crawlerRepoFactory.getWpRepository().getWpCategories();
  }

  private void transferItems(int batchSize) {
    try {
      logger.info("START transfer Items for brandId = {}!", brandId);

      logger.info("Set unavailable for all wp items for brandId = {}!", brandId);
      crawlerRepoFactory.getWpRepository().setClWpItemsStatus(brandId, false);

      logger.info("Set unavailable for all wp item details for brandId = {}!", brandId);
      crawlerRepoFactory.getWpRepository().setClWpItemDetailsStatus(brandId, false);

      BaseAttribute countryAttr = new BaseAttribute(1L);
      BaseAttribute sizeAttr = new BaseAttribute(3L);

      int nThreads = 1;
      ExecutorService executor = Executors.newFixedThreadPool(nThreads);

      List<Callable<Object>> callableList = new ArrayList<>();
      for (int i = 1; i <= nThreads; i++) {
        int temp = i;
        callableList.add(() -> doTransferItems(countryAttr, sizeAttr, temp));
      }

      executor.invokeAll(callableList);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, "Exception when transferring items.", e, brandId);
    }

    updatePrivateStatusForUnavailableItems(batchSize * 6);

    updatePrivateStatusForUnavailableItemVariants(batchSize * 6);

    logger.info("END transfer Items!");
  }

  private int doTransferItems(BaseAttribute countryAttr, BaseAttribute sizeAttr, int threadIndex) {
    String threadName = brandName + "-" + threadIndex;
    Thread.currentThread().setName(threadName);

    boolean stillHaveRecords = false;
    do {
      try {
        stillHaveRecords = false;
        ClItem clItem = getClItem();

        if (clItem != null) {
          stillHaveRecords = true;

          Product product =
              buildProduct(
                  countryAttr,
                  sizeAttr,
                  clItem,
                  crawlerRepoFactory.getWpRepository().getWpItemByItemId(clItem.getId()));

          if (product != null) {
            List<ProductVariation> productVariations = product.getVariations();
            product.setVariations(null);

            if (product.getId() != null) {
              WpApiHelper.updateProduct(product);
            } else {
              WpApiHelper.createProduct(product);
            }

            if (product.getId() == null) {
              logger.error(
                  "Error when transferring item {} - {}.", clItem.getId(), clItem.getName());
              crawlerRepoFactory
                  .getErrorSummaryRepo()
                  .saveError(
                      Constants.RUN_UID,
                      threadName + " - Error when transferring item " + clItem.getId(),
                      null,
                      clItem);
              continue;
            }

            ClWpItem clWpItem = product.clWpItem;

            // Update productId, images back to clWpItem
            clWpItem.setWpProductId(product.getId());
            clWpItem.setRemovedInWp(false);
            if (product.getImages() != null && product.getImages().size() > 0) {
              clWpItem.setClWpImages(
                  product.getImages().stream()
                      .map(
                          image -> {
                            ClWpImage wpImage =
                                crawlerRepoFactory
                                    .getWpRepository()
                                    .getWpImageByName(image.getName());
                            if (wpImage == null) {
                              wpImage = new ClWpImage(image);
                              crawlerRepoFactory.getWpRepository().save(wpImage);
                            }
                            return wpImage;
                          })
                      .collect(Collectors.toList()));
            }
            crawlerRepoFactory.getWpRepository().save(clWpItem);

            List<ClWpItemDetail> clWpItemDetails =
                crawlerRepoFactory.getWpRepository().getWpItemDetails(clWpItem.getId());

            List<ProductVariation> variations =
                productVariations.stream()
                    .filter(
                        v -> {
                          ClWpItemDetail clWpItemDetail =
                              clWpItemDetails.stream()
                                  .filter(d -> v.clItemDetail.getId().equals(d.getItemDetailId()))
                                  .findFirst()
                                  .orElse(null);

                          boolean allow = true;
                          if (clWpItemDetail == null) {
                            clWpItemDetail = new ClWpItemDetail(v.clItemDetail);
                            clWpItemDetail.setWpItemId(clWpItem.getId());
                            clWpItemDetail.setPrice(
                                calculatePrice(v.clItemDetail, v.clItemDetail.getPrice()));
                            clWpItemDetail.setAvailable(v.clItemDetail.getAvailable());
                            clWpItemDetails.add(clWpItemDetail);
                          } else {
                            v.setId(clWpItemDetail.getWpProductVariationId());
                            allow =
                                !calculatePrice(v.clItemDetail, v.clItemDetail.getPrice())
                                    .equals(clWpItemDetail.getPrice());
                            clWpItemDetail.setAvailable(v.getClItemDetail().getAvailable());
                          }
                          v.clWpItemDetail = clWpItemDetail;

                          return allow;
                        })
                    .collect(Collectors.toList());

            WpApiHelper.sendToWpABatchProductVariant(clWpItem.getWpProductId(), variations);

            // Update productVariantId back to clWpItemDetail
            updateWpItemDetails(clWpItem, clWpItemDetails, variations);

            clWpItemDetails.forEach(
                clWpItemDetail -> crawlerRepoFactory.getWpRepository().save(clWpItemDetail));
          }
        }
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
        crawlerRepoFactory
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID, threadName + " - Exception when doTransferItems.", e, brandId);
      }
    } while (stillHaveRecords);

    return 1;
  }

  private synchronized ClItem getClItem() {
    List<ClItem> clItems =
        crawlerRepoFactory.getItemRepo().getAllSaleItems(1, transferItemLastId, brandId);

    ClItem clItem = clItems.size() > 0 ? clItems.get(0) : null;
    if (clItem != null) {
      transferItemLastId = Math.max(transferItemLastId, clItem.getId());
    }

    return clItem;
  }

  private Product buildProduct(
      BaseAttribute countryAttr, BaseAttribute sizeAttr, ClItem clItem, ClWpItem clWpItem) {
    logger.info("Transferring item {} - {}.", clItem.getId(), clItem.getName());
    transferItemLastId = clItem.getId();

    clItem.setItemCountryDetailList(
        clItem.getItemCountryDetailList().stream()
            .filter(detail -> detail.getAvailable() && detail.getSaleRate() > 0)
            .collect(Collectors.toList()));
    if (clItem.getItemCountryDetailList().size() == 0) {
      logger.info(
          "Item {} - {} does NOT have valid item details.", clItem.getId(), clItem.getName());
      return null;
    }

    if (clWpItem == null) {
      clWpItem = new ClWpItem(clItem);
    } else {
      clWpItem.setAvailable(clItem.getAvailable());
    }

    ClItemCountryDetail minPriceDetail =
        clItem.getItemCountryDetailList().stream()
            .min(
                Comparator.comparingInt(
                    detail -> calculatePrice(detail, detail.getPrice()).intValue()))
            .orElse(clItem.getItemCountryDetailList().get(0));

    boolean shouldIgnore =
        calculatePrice(minPriceDetail, minPriceDetail.getPrice()).equals(clWpItem.getPrice());
    if (!shouldIgnore) {
      Product product = buildProduct(clItem, clWpItem, countryAttr, sizeAttr, minPriceDetail);

      if (product == null) {
        crawlerRepoFactory
            .getErrorSummaryRepo()
            .saveError(Constants.RUN_UID, "Cannot buildProduct.", null, clItem);
        return null;
      }

      if (clWpItem.getWpProductId() != null) {
        // Merge existed clWpItem to Product (generated by clItem)
        mergeClWpItemToProduct(product, clWpItem);
      }

      clWpItem.setPrice(calculatePrice(minPriceDetail, minPriceDetail.getPrice()));

      return product;
    }

    return null;
  }

  private void updateWpItemDetails(
      ClWpItem wpItem, List<ClWpItemDetail> clWpItemDetails, List<ProductVariation> variations) {
    variations.forEach(
        v -> {
          if (v.getId() == null) {
            logger.error(
                "Error when transferring item detail {} - {}.",
                v.getSku(),
                v.getClItemDetail().getName());
            crawlerRepoFactory
                .getErrorSummaryRepo()
                .saveError(
                    Constants.RUN_UID,
                    "Error when transferring item detail " + v.getClItemDetail().getId(),
                    null,
                    v.getClItemDetail());
          } else {
            ClWpItemDetail clWpItemDetail = v.clWpItemDetail;

            if (clWpItemDetail == null) {
              clWpItemDetail = new ClWpItemDetail(v.clItemDetail);
              clWpItemDetails.add(clWpItemDetail);
            }

            clWpItemDetail.setWpItemId(wpItem.getId());
            clWpItemDetail.setWpProductVariationId(v.id);
            clWpItemDetail.setPrice(calculatePrice(v.clItemDetail, v.clItemDetail.getPrice()));
            clWpItemDetail.setAvailable(v.clItemDetail.getAvailable());
          }
        });
  }

  private void updatePrivateStatusForUnavailableItems(int batchSize) {
    boolean stillHaveRecords;
    AtomicReference<Long> lastId = new AtomicReference<>(0L);

    logger.info("START Update private status for unavailable products.");

    do {
      stillHaveRecords = false;
      try {
        List<ClWpItem> clWpItems =
            crawlerRepoFactory
                .getWpRepository()
                .getWpItems(brandId, batchSize, lastId.get(), false);

        if (clWpItems != null && clWpItems.size() > 0) {
          stillHaveRecords = true;
          List<Product> products =
              clWpItems.stream()
                  .map(
                      clWpItem -> {
                        lastId.set(clWpItem.getId());

                        Product product = new Product();
                        product.setId(clWpItem.getWpProductId());
                        product.setStatus("private");

                        return product;
                      })
                  .collect(Collectors.toList());

          WpApiHelper.sendToWpABatchProduct(products);

          clWpItems.forEach(
              clWpItem -> {
                clWpItem.setRemovedInWp(true);
                crawlerRepoFactory.getWpRepository().save(clWpItem);
              });

          clWpItems.forEach(
              clWpItem -> {
                List<ClWpItemDetail> clWpItemsDetails =
                    crawlerRepoFactory.getWpRepository().getWpItemDetails(clWpItem.getId());

                List<Long> variantIds =
                    clWpItemsDetails.stream()
                        .map(ClWpItemDetailGen::getWpProductVariationId)
                        .collect(Collectors.toList());

                WpApiHelper.sendToWpToDeleteVariants(clWpItem.getWpProductId(), variantIds);

                clWpItemsDetails.forEach(
                    clWpItemDetail -> crawlerRepoFactory.getWpRepository().delete(clWpItemDetail));
              });
        }
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
        crawlerRepoFactory
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                String.format(
                    "Brand - %d : Error when updating private status for unavailable products",
                    brandId),
                e);
      }
    } while (stillHaveRecords);
  }

  private void updatePrivateStatusForUnavailableItemVariants(int batchSize) {
    boolean stillHaveRecords;
    AtomicReference<Long> lastId = new AtomicReference<>(0L);

    logger.info("START Update private status for unavailable product variations.");

    do {
      stillHaveRecords = false;
      try {
        List<ClWpItemDetail> clWpItemDetails =
            crawlerRepoFactory
                .getWpRepository()
                .getWpItemDetails(brandId, batchSize, lastId.get(), false);

        if (clWpItemDetails != null && clWpItemDetails.size() > 0) {
          stillHaveRecords = true;
          clWpItemDetails.stream()
              .filter(detail -> !detail.getAvailable())
              .forEach(
                  clWpItemDetail -> {
                    lastId.set(clWpItemDetail.getId());
                    WpApiHelper.sendToWpToDeleteVariants(
                        clWpItemDetail.getClWpItem().getWpProductId(),
                        Collections.singletonList(clWpItemDetail.getWpProductVariationId()));
                    crawlerRepoFactory.getWpRepository().delete(clWpItemDetail);
                  });
        }
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
        crawlerRepoFactory
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                String.format(
                    "Brand - %d : Error when updating private status for unavailable products",
                    brandId),
                e);
      }
    } while (stillHaveRecords);
  }

  private Product buildProduct(
      ClItem item,
      ClWpItem clWpItem,
      BaseAttribute countryBaseAttr,
      BaseAttribute sizeBaseAttr,
      ClItemCountryDetail minPriceDetail) {
    Product product = new Product(clWpItem);

    product.setName(item.getName());
    product.setSku(buildSku(item));
    product.setOnSale(minPriceDetail.getSaleRate() > 0);
    product.setTotalSales(minPriceDetail.getSaleRate());
    product.setRegularPrice(
        minPriceDetail.getOldPrice() != null
            ? calculatePriceToStr(minPriceDetail, minPriceDetail.getOldPrice())
            : calculatePriceToStr(minPriceDetail, minPriceDetail.getPrice()));
    product.setSalePrice(
        minPriceDetail.getPrice() != null
            ? calculatePriceToStr(minPriceDetail, minPriceDetail.getPrice())
            : null);
    product.setStatus(item.getAvailable() ? "publish" : "private");
    product.setStockStatus("instock");
    product.setType("variable");

    if (!minPriceDetail.getImage().contains("data:image")) {
      WpImage image = null;
      if (item.getBrandId() == 20) {
        image =
            new WpImage(
                minPriceDetail.getImage().substring(minPriceDetail.getImage().lastIndexOf('/') + 1),
                UploadImageHelper.uploadImage(minPriceDetail.getImage()));
      } else {
        String url = UploadImageHelper.getRealUrl(minPriceDetail.getImage());
        if (url != null) {
          image = new WpImage(url);
        }
      }
      if (image != null) {
        ClWpImage wpImage = crawlerRepoFactory.getWpRepository().getWpImageByName(image.getName());
        if (wpImage != null) {
          product.getImages().add(wpImage.getImage());
        } else {
          product.getImages().add(image);
        }
      }
    }

    List<ClWpCategory> clWpCategories =
        crawlerRepoFactory
            .getWpRepository()
            .getWpCategories(Collections.singletonList(item.getCategory().getId()));
    if (clWpCategories.size() == 0) {
      return null;
    }
    product.setCategories(
        clWpCategories.stream().map(ClWpCategory::getWpCategory).collect(Collectors.toList()));

    product.setVariations(new ArrayList<>());

    Attribute countryAttr =
        new Attribute(
            countryBaseAttr,
            1,
            true,
            true,
            new LinkedHashSet<>(
                item.getItemCountryDetailList().stream()
                    .filter(ClItemCountryDetailGen::getAvailable)
                    .map(detail -> detail.getCountry().getName())
                    .collect(Collectors.toList())));
    Attribute sizeAttr =
        new Attribute(
            sizeBaseAttr,
            2,
            true,
            true,
            new LinkedHashSet<>(
                item.getItemCountryDetailList().stream()
                    .filter(ClItemCountryDetailGen::getAvailable)
                    .flatMap(detail -> detail.getItemStockList().stream())
                    .filter(stock -> stock.getStock() > 0)
                    .filter(ClItemStockGen::getStatus)
                    .map(ClItemStockGen::getSizeCode)
                    .distinct()
                    .map(keyValues::get)
                    .collect(Collectors.toList())));

    product.setAttributes(Arrays.asList(countryAttr, sizeAttr));

    product.setDefaultAttributes(
        new ArrayList<DefaultAttribute>() {
          {
            add(new DefaultAttribute(countryBaseAttr, minPriceDetail.getCountry().getName()));
            add(
                new DefaultAttribute(
                    sizeBaseAttr,
                    sizeAttr.getOptions().stream()
                        .filter(Objects::nonNull)
                        .findFirst()
                        .orElse("")));
          }
        });

    Map<String, String> images = new HashMap<>();
    if (CollectionUtils.isNotEmpty(clWpItem.getClWpImages())) {
      clWpItem.getClWpImages().forEach(i -> images.put(i.getName(), i.getUrl()));
    }

    item.getItemCountryDetailList().stream()
        .filter(ClItemCountryDetailGen::getAvailable)
        .filter(
            detail ->
                product.getVariations().stream()
                    .noneMatch(
                        variation ->
                            variation.getClItemDetail().getCountryId().equals(detail.getCountryId())
                                && variation.getClItemDetail().getCode().equals(detail.getCode())))
        .forEach(
            detail -> {
              ProductVariation variation = new ProductVariation(detail);

              variation.setRegularPrice(
                  detail.getOldPrice() != null
                      ? calculatePriceToStr(detail, detail.getOldPrice())
                      : calculatePriceToStr(detail, detail.getPrice()));
              variation.setSalePrice(
                  detail.getPrice() != null
                      ? calculatePriceToStr(detail, detail.getPrice())
                      : null);
              variation.setStatus(detail.getAvailable() ? "publish" : "");
              variation.setOnSale(detail.getSaleRate() > 0);
              variation.setSku(buildSku(detail));
              variation.setDescription(
                  String.format(
                      "%s : <a href='%s' target='_blank'>%s</a>",
                      item.getBrand().getName(), detail.getUrl(), detail.getUrl()));
              variation.setAttributes(
                  new ArrayList<DefaultAttribute>() {
                    {
                      add(new DefaultAttribute(countryAttr, detail.getCountry().getName()));
                    }
                  });

              product.getVariations().add(variation);

              /*detail.getItemImageList().stream()
              .filter(image -> !images.containsKey(image.getName()))
              .filter(image -> !image.getUrl().contains("data:image"))
              .forEach(
                  image ->
                      images.put(
                          image.getName(),
                          detail.getBrandId() == 2
                              ? UploadImageHelper.uploadImage(image.getUrl())
                              : UploadImageHelper.getRealUrl(image.getUrl())));*/
              detail.getItemImageList().stream()
                  .filter(image -> !images.containsKey(image.getName()))
                  .filter(image -> !image.getUrl().contains("data:image"))
                  .forEach(image -> images.put(image.getName(), image.getUrl()));
            });

    for (Map.Entry<String, String> image : images.entrySet()) {
      if (image.getValue() != null) {
        ClWpImage wpImage = crawlerRepoFactory.getWpRepository().getWpImageByName(image.getKey());
        if (wpImage != null) {
          product.getImages().add(wpImage.getImage());
        } else {
          product.getImages().add(new WpImage(image.getKey(), image.getValue()));
        }
      }
    }

    return product;
  }

  private void transferCategory(
      ClCategory clCategory,
      Set<ClWpCategory> clWpCategories,
      Map<WpCategory, List<ClWpCategory>> wpCategoryMap)
      throws Exception {
    String brand = clCategory.getBrand().getName();
    String cat1 = clCategory.getCat1();
    String cat2 = clCategory.getCat2();

    List<ClWpCategory> shouldDeleteClWpCategories = new ArrayList<>();
    clWpCategories.stream()
        .filter(
            c ->
                !c.getWpCategory().name.equalsIgnoreCase(brand)
                    && !c.getWpCategory().name.equalsIgnoreCase(cat1)
                    && !c.getWpCategory().name.equalsIgnoreCase(cat2))
        .forEach(shouldDeleteClWpCategories::add);

    if (shouldDeleteClWpCategories.size() > 0) {
      List<WpCategory> shouldDeleteWpCategories = new ArrayList<>();
      shouldDeleteClWpCategories.forEach(
          clWpCategory -> {
            crawlerRepoFactory.getWpRepository().delete(clWpCategory);

            clWpCategories.remove(clWpCategory);

            wpCategoryMap.get(clWpCategory.getWpCategory()).remove(clWpCategory);
            if (wpCategoryMap.get(clWpCategory.getWpCategory()).size() == 0) {
              shouldDeleteWpCategories.add(clWpCategory.getWpCategory());
            }
          });

      if (shouldDeleteWpCategories.size() > 0) {
        WpApiHelper.sendToWpToDeleteCategories(shouldDeleteWpCategories);
      }
    }

    WpCategory brandCat = transferCat(clCategory, clWpCategories, wpCategoryMap, null, brand);

    WpCategory cat1Cat = transferCat(clCategory, clWpCategories, wpCategoryMap, brandCat, cat1);

    transferCat(clCategory, clWpCategories, wpCategoryMap, cat1Cat, cat2);
  }

  private WpCategory transferCat(
      ClCategory clCategory,
      Set<ClWpCategory> clWpCategories,
      Map<WpCategory, List<ClWpCategory>> wpCategoryMap,
      WpCategory parent,
      String name)
      throws Exception {
    WpCategory brandCat = getExistedWpCategory(wpCategoryMap, parent, name);
    if (brandCat == null) {
      brandCat = createCategoryOnWp(name, parent);

      ClWpCategory clWpCategory = new ClWpCategory(clCategory, brandCat);
      crawlerRepoFactory.getWpRepository().save(clWpCategory);

      clWpCategories.add(clWpCategory);
      wpCategoryMap.put(brandCat, new ArrayList<>(Collections.singletonList(clWpCategory)));
    } else {
      WpCategory finalBrandCat = brandCat;
      if (clWpCategories.stream()
          .noneMatch(clWpCategory -> clWpCategory.getWpCategoryId().equals(finalBrandCat.id))) {
        ClWpCategory clWpCategory = new ClWpCategory(clCategory, brandCat);
        crawlerRepoFactory.getWpRepository().save(clWpCategory);

        clWpCategories.add(clWpCategory);
        wpCategoryMap.get(brandCat).add(clWpCategory);
      }
    }

    return brandCat;
  }

  private WpCategory createCategoryOnWp(String name, WpCategory parent) throws Exception {
    WpCategory wpCategory;
    if (parent == null) {
      wpCategory = new WpCategory(name);
    } else {
      wpCategory = new WpCategory(name, parent.id, parent.description);
    }

    WpApiHelper.sendToWpToCreateCategory(wpCategory);

    return wpCategory;
  }

  private WpCategory getExistedWpCategory(
      Map<WpCategory, List<ClWpCategory>> wpCategoryMap, WpCategory parent, String name) {
    return wpCategoryMap.keySet().stream()
        .filter(
            wpCategory ->
                wpCategory.description.equals(
                    (parent != null ? parent.description + "-" : "") + name.toUpperCase()))
        .findFirst()
        .orElse(null);
  }

  private String calculatePriceToStr(ClItemCountryDetail detail, BigDecimal price) {
    return String.format("%d", calculatePrice(detail, price).intValue());
  }

  private BigDecimal calculatePrice(ClItemCountryDetail detail, BigDecimal price) {
    price = price.multiply(currencies.get(detail.getCountryId()));
    price = price.add(calculateInterest(price));

    try {
      JsonNode shippingFeeNode =
          SFObjectMapper.toJsonNode(
              detail.getCategoryCrawler().getCategory().getShippingFee(), true);
      price =
          price.add(
              BigDecimal.valueOf(
                  shippingFeeNode.get(detail.getCountry().getShortCode()).asLong(0)));
    } catch (Exception e) {
      // ignore any exceptions
    }

    return price;
  }

  private BigDecimal calculateInterest(BigDecimal price) {
    return price
        .multiply(Constants.INTEREST_RATE)
        .max(Constants.MIN_INTEREST)
        .min(Constants.MAX_INTEREST);
  }

  private String buildSku(ClItem item) {
    return item.getId() + "-" + item.getCode();
  }

  private String buildSku(ClItemCountryDetail detail) {
    return detail.getId() + "-" + detail.getCode() + "-" + detail.getCountry().getName();
  }

  private void mergeClWpItemToProduct(Product product, ClWpItem clWpItem) {
    product.setId(clWpItem.getWpProductId());
    if (clWpItem.getClWpImages().size() > 0) {
      Map<String, WpImage> temp =
          product.getImages().stream()
              .collect(Collectors.toMap(WpImage::getName, Function.identity()));
      clWpItem.getClWpImages().stream()
          .map(ClWpImageGen::getImage)
          .filter(image -> StringUtils.isNotBlank(image.getSrc()))
          .forEach(image -> temp.put(image.getName(), image));

      product.setImages(new LinkedHashSet<>(temp.values()));
    }
  }
}
