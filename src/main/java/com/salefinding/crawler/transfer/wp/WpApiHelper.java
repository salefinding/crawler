package com.salefinding.crawler.transfer.wp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.HttpClientManager;
import com.salefinding.crawler.utils.PropUtil;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.pojo.wp.Product;
import com.salefinding.pojo.wp.ProductVariation;
import com.salefinding.pojo.wp.WpCategory;
import com.salefinding.utils.SFObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class WpApiHelper {

  private static Logger logger = LoggerFactory.getLogger(WpApiHelper.class);

  private static String apiBaseUrl = PropUtil.getString(Constants.WP_API_URL);

  private static String apiKey = PropUtil.getString(Constants.WP_API_KEY);

  private static String apiSecret = PropUtil.getString(Constants.WP_API_SECRET);

  public static void createProduct(Product product) throws Exception {
    String url = apiBaseUrl + "wp-json/wc/v3/products";

    HttpClientManager.RequestEvent requestEvent =
        new HttpClientManager.RequestEvent(url, "POST", null);
    requestEvent.setBody(SFObjectMapper.toJsonNode(product, true).toString());

    HttpClientManager.HttpResponse response =
        sendWithRetry(
            () ->
                HttpClientManager.getInstance()
                    .sendHttpRequest(
                        requestEvent,
                        false,
                        300 * 1000,
                        new AbstractMap.SimpleEntry<>(apiKey, apiSecret)));

    if (response.isSuccessful()) {
      Product result = SFObjectMapper.toObject(response.getBody(), Product.class, true);
      product.setId(result.getId());
      product.setImages(result.getImages());
    }
  }

  public static void updateProduct(Product product) throws Exception {
    String url = apiBaseUrl + "wp-json/wc/v3/products/" + product.getId();

    HttpClientManager.RequestEvent requestEvent =
        new HttpClientManager.RequestEvent(url, "PUT", null);
    requestEvent.setBody(SFObjectMapper.toJsonNode(product, true).toString());

    HttpClientManager.HttpResponse response =
        sendWithRetry(
            () ->
                HttpClientManager.getInstance()
                    .sendHttpRequest(
                        requestEvent,
                        false,
                        300 * 1000,
                        new AbstractMap.SimpleEntry<>(apiKey, apiSecret)));

    if (response.isSuccessful()) {
      Product result = SFObjectMapper.toObject(response.getBody(), Product.class, true);
      product.setImages(result.getImages());
    }
  }

  public static void sendToWpABatchProduct(Collection<Product> products) throws IOException {
    String url = apiBaseUrl + "wp-json/wc/v3/products/batch";
    HttpClientManager.HttpResponse response =
        sendWithRetry(
            () ->
                sendABatchProducts(
                    url,
                    products.stream().filter(p -> p.getId() == null).collect(Collectors.toList()),
                    products.stream().filter(p -> p.getId() != null).collect(Collectors.toList())));

    if (response.isSuccessful()) {
      ObjectNode resNode = SFObjectMapper.toObject(response.getBody(), ObjectNode.class, true);
      logger.debug(resNode.toString());
      if (resNode.has("create")) {
        List<Product> createList =
            SFObjectMapper.toObject(resNode.get("create"), new TypeReference<>() {}, true);

        // Set back wp product Id and images
        createList.stream()
            .filter(Objects::nonNull)
            .forEach(
                product ->
                    products.stream()
                        .filter(Objects::nonNull)
                        .filter(p -> Objects.equals(p.getSku(), product.getSku()))
                        .findFirst()
                        .ifPresent(
                            p -> {
                              p.setId(product.getId());
                              p.setImages(product.getImages());
                            }));
      } else if (resNode.has("update")) {
        List<Product> updateList =
            SFObjectMapper.toObject(resNode.get("update"), new TypeReference<>() {}, true);

        // update Images from response to products
        updateList.stream()
            .filter(Objects::nonNull)
            .forEach(
                product ->
                    products.stream()
                        .filter(Objects::nonNull)
                        .filter(p -> Objects.equals(p.getSku(), product.getSku()))
                        .findAny()
                        .ifPresent(p -> p.setImages(product.getImages())));
      }
    }
  }

  public static void sendToWpABatchProductVariant(
      Long wpProductId, Collection<ProductVariation> variations) throws Exception {
    if (variations.size() == 0) {
      return;
    }

    String url = apiBaseUrl + "wp-json/wc/v3/products/" + wpProductId + "/variations/batch";
    HttpClientManager.HttpResponse response =
        sendWithRetry(
            () ->
                sendABatchProducts(
                    url,
                    variations.stream().filter(v -> v.getId() == null).collect(Collectors.toList()),
                    variations.stream()
                        .filter(v -> v.getId() != null)
                        .collect(Collectors.toList())));

    if (response.isSuccessful()) {
      ObjectNode resNode = SFObjectMapper.toObject(response.getBody(), ObjectNode.class, true);
      logger.debug(resNode.toString());
      if (resNode.has("create")) {
        List<ProductVariation> createList =
            SFObjectMapper.toObject(resNode.get("create"), new TypeReference<>() {}, true);

        // Set back wp variant Id
        createList.stream()
            .filter(Objects::nonNull)
            .forEach(
                rv ->
                    variations.stream()
                        .filter(Objects::nonNull)
                        .filter(v -> Objects.equals(v.sku, rv.getSku()))
                        .findFirst()
                        .ifPresent(v -> v.setId(rv.getId())));
      }
    }
  }

  private static HttpClientManager.HttpResponse sendABatchProducts(
      String url, Collection<Object> createObjs, Collection<Object> updateObjs) throws Exception {
    Map<String, Collection<Object>> body = new HashMap<>();

    body.put("create", createObjs);
    body.put("update", updateObjs);

    HttpClientManager.RequestEvent requestEvent = new HttpClientManager.RequestEvent();
    requestEvent.setUrl(url);
    requestEvent.setGet(false);
    requestEvent.setHttpMethod("POST");
    requestEvent.setBody(SFObjectMapper.toJsonNode(body, true).toString());

    logger.debug(requestEvent.toString());

    return HttpClientManager.getInstance()
        .sendHttpRequest(
            requestEvent, false, 300 * 1000, new AbstractMap.SimpleEntry<>(apiKey, apiSecret));
  }

  public static void sendToWpToCreateCategory(WpCategory wpCategory) throws Exception {
    HttpClientManager.RequestEvent requestEvent = new HttpClientManager.RequestEvent();
    requestEvent.setUrl(apiBaseUrl + "wp-json/wc/v3/products/categories");
    requestEvent.setGet(false);
    requestEvent.setHttpMethod("POST");
    requestEvent.setBody(SFObjectMapper.toJsonNode(wpCategory, true).toString());

    HttpClientManager.HttpResponse response =
        sendWithRetry(
            () ->
                HttpClientManager.getInstance()
                    .sendHttpRequest(
                        requestEvent,
                        false,
                        300 * 1000,
                        new AbstractMap.SimpleEntry<>(apiKey, apiSecret)));
    if (response.isSuccessful()) {
      WpCategory resCat = SFObjectMapper.toObject(response.getBody(), WpCategory.class, true);
      wpCategory.setId(resCat.getId());
    }
  }

  public static void sendToWpToDeleteCategories(List<WpCategory> wpCategories) throws Exception {
    sendBatchToDelete(
        apiBaseUrl + "wp-json/wc/v3/products/category/batch",
        wpCategories.stream().map(WpCategory::getId).collect(Collectors.toList()));
  }

  public static void sendToWpToDeleteVariants(Long productId, List<Long> ids) {
    try {
      sendBatchToDelete(
          apiBaseUrl + "wp-json/wc/v3/products/" + productId + "/variations/batch", ids);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  public static void sendBatchToDelete(String url, List<Long> ids) throws Exception {
    Map<String, List<Long>> body = new HashMap<>();
    body.put("delete", ids);

    HttpClientManager.RequestEvent requestEvent = new HttpClientManager.RequestEvent();
    requestEvent.setUrl(url);
    requestEvent.setGet(false);
    requestEvent.setHttpMethod("POST");
    requestEvent.setBody(SFObjectMapper.toJsonNode(body, true).toString());

    sendWithRetry(
        () ->
            HttpClientManager.getInstance()
                .sendHttpRequest(
                    requestEvent,
                    false,
                    300 * 1000,
                    new AbstractMap.SimpleEntry<>(apiKey, apiSecret)));
  }

  private static HttpClientManager.HttpResponse sendWithRetry(
      Callable<HttpClientManager.HttpResponse> callable) {
    HttpClientManager.HttpResponse response;
    Exception exception = null;
    for (int i = 0; i < RetryManageUtil.MAX_RETRY; i++) {
      try {
        response = callable.call();
        if (response.isSuccessful()) {
          return response;
        }
      } catch (Exception e) {
        exception = e;
      }
    }
    if (exception != null) {
      logger.error("Error when sending to WP.", exception);
    }
    return new HttpClientManager.HttpResponse(false);
  }
}
