package com.salefinding.crawler.transfer.wp;

import com.salefinding.crawler.models.ProxyModel;
import com.salefinding.crawler.utils.*;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class UploadImageHelper {

  private static Logger logger = LoggerFactory.getLogger(UploadImageHelper.class);

  private static String apiBaseUrl = PropUtil.getString(Constants.WP_API_URL);

  public static String getRealUrl(String src) {
    if (src.startsWith("//")) {
      src = "https:" + src;
    }

    HttpClientManager.RequestEvent requestEvent =
        new HttpClientManager.RequestEvent(src, true, null);

    Exception exception = null;
    for (int i = 0; i < RetryManageUtil.MAX_RETRY; i++) {
      try (Response response =
          HttpClientManager.getInstance()
              .sendHttpRequestReturnBase(requestEvent, false, 0, null, null, null, null, null)) {
        boolean isSuccessful = response.isSuccessful();

        if (isSuccessful) {
          return src;
        }
      } catch (Exception e) {
        exception = e;
      }
    }

    if (exception != null) {
      logger.error("Error when uploading image " + src, exception);
    }

    return null;
  }

  public static String uploadImage(String src) {
    if (src.startsWith("//")) {
      src = "https:" + src;
    }
    String name = src.substring(src.lastIndexOf('/') + 1);
    File file = new File("/var/www/wordpress/wp-content/uploads/temp/" + name);
    logger.debug("File: {}", file.getAbsolutePath());
    HttpClientManager.RequestEvent requestEvent =
        new HttpClientManager.RequestEvent(src, true, null);

    Exception exception = null;
    for (int i = 0; i < RetryManageUtil.MAX_RETRY; i++) {
      ProxyModel proxyModel = null;
      if (i < RetryManageUtil.MAX_RETRY - 1) {
        proxyModel = ProxyUtil.get().getRotatedModel();
      }

      try (Response response =
          HttpClientManager.getInstance()
              .sendHttpRequestReturnBase(
                  requestEvent, false, 0, null, null, null, null, proxyModel)) {
        if (response.body() != null) {
          try (InputStream imageReader = new BufferedInputStream(response.body().byteStream());
              OutputStream imageWriter = new BufferedOutputStream(new FileOutputStream(file))) {

            int readByte;
            while ((readByte = imageReader.read()) != -1) {
              imageWriter.write(readByte);
            }

            return apiBaseUrl + "wp-content/uploads/temp/" + name;
          }
        }
      } catch (Exception e) {
        exception = e;
      }
    }

    if (exception != null) {
      logger.error("Error when uploading image " + src, exception);
    }

    return null;
  }
}
