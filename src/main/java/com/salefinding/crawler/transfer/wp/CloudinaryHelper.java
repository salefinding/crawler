package com.salefinding.crawler.transfer.wp;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.HttpClientManager;
import com.salefinding.crawler.utils.PropUtil;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;

public class CloudinaryHelper {

  private static Logger logger = LoggerFactory.getLogger(CloudinaryHelper.class);

  private static String cloudinaryApiKey = PropUtil.getString(Constants.CLOUDINARY_API_KEY);

  private static String cloudinaryApiSecret = PropUtil.getString(Constants.CLOUDINARY_API_SECRET);

  private static Cloudinary cloudinary =
      new Cloudinary(
          ObjectUtils.asMap(
              "cloud_name",
              "ntanh1402",
              "api_key",
              cloudinaryApiKey,
              "api_secret",
              cloudinaryApiSecret));

  public static String uploadImage(String src) {
    if (src.startsWith("//")) {
      src = "https:" + src;
    }
    String name = src.substring(src.lastIndexOf('/') + 1);
    File file = new File(name);
    HttpClientManager.RequestEvent requestEvent =
        new HttpClientManager.RequestEvent(src, true, null);
    try (Response response =
        HttpClientManager.getInstance()
            .sendHttpRequestReturnBase(requestEvent, false, 0, null, null, null, null, null)) {
      if (response.body() != null) {
        try (InputStream imageReader = new BufferedInputStream(response.body().byteStream());
            OutputStream imageWriter = new BufferedOutputStream(new FileOutputStream(file))) {

          int readByte;
          while ((readByte = imageReader.read()) != -1) {
            imageWriter.write(readByte);
          }

          Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.emptyMap());

          return uploadResult.get("url").toString();
        } catch (Exception e) {
          logger.error(e.getMessage(), e);
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    } finally {
      file.delete();
    }

    return src;
  }

  public static void main(String[] args) {
    System.out.println(
        CloudinaryHelper.uploadImage(
            "https://www.adidas.com.my/dw/image/v2/bcbs_prd/on/demandware.static/-/Sites-adidas-products/default/dwa62942f8/zoom/DW5533_21_model.jpg"));
  }
}
