package com.salefinding.crawler.fetchers;

import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.models.PuppeteerModel;
import com.salefinding.crawler.utils.PuppeteerUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class PuppeteerFetcher extends CustomUrlPageFetcher {

  private static final Logger logger = LoggerFactory.getLogger(PuppeteerFetcher.class);

  public PuppeteerFetcher(ClBrandCountry brandCountry, CrawlConfig config)
      throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    super(brandCountry, config);
  }

  @Override
  protected String getRealToFetchUrl(WebURL webUrl) {
    String url = webUrl.getURL();

    try {
      URIBuilder startUrl = new URIBuilder(brandCountry.getStartUrl());

      URIBuilder toFetchUrlBuilder = new URIBuilder(url);
      toFetchUrlBuilder.addParameters(startUrl.getQueryParams());

      url = toFetchUrlBuilder.toString();
    } catch (URISyntaxException e) {
      logger.error(e.getMessage(), e);
    }

    return getPuppeteerModel()
        .getPuppeteerUrl(
            url, brandCountry.getWaitForElement(), brandCountry.getRepeat(), PuppeteerAction.HTML);
  }

  protected PuppeteerModel getPuppeteerModel() {
    return PuppeteerUtil.get().getModel(brandCountry.getId());
  }
}
