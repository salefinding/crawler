package com.salefinding.crawler.fetchers;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.fetcher.PageFetchResult;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.url.WebURL;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class JsoupPageFetcher extends PageFetcher {

  public JsoupPageFetcher(CrawlConfig config)
      throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    super(config);
  }

  @Override
  public PageFetchResult fetchPage(WebURL webUrl) throws IOException {
    JsoupPageFetcherResult fetcherResult = new JsoupPageFetcherResult();
    Connection connection = Jsoup.connect(webUrl.getURL());

    Connection.Response response = connection.execute();
    fetcherResult.setStatusCode(response.statusCode());
    fetcherResult.setFetchedUrl(webUrl.getURL());
    fetcherResult.setHeaders(response.headers());
    fetcherResult.setPageContentData(response.bodyAsBytes());

    return fetcherResult;
  }
}
