package com.salefinding.crawler.fetchers;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.fetcher.PageFetchResult;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class JsoupPageFetcherResult extends PageFetchResult {

  protected byte[] pageContentData;

  protected Map<String, String> headers;

  public JsoupPageFetcherResult() {
    super(false);
  }

  public JsoupPageFetcherResult(PageFetchResult pageFetchResult) {
    super(false);
    this.entity = pageFetchResult.getEntity();
    this.fetchedUrl = pageFetchResult.getFetchedUrl();
    this.movedToUrl = pageFetchResult.getMovedToUrl();
    this.statusCode = pageFetchResult.getStatusCode();
    this.responseHeaders = pageFetchResult.getResponseHeaders();
  }

  public boolean fetchContent(Page page, int maxBytes) {
    try {
      page.setContentType(headers.get("Content-Type"));
      page.setContentEncoding(headers.get("Content-Encoding"));
      page.setContentCharset("UTF-8");
      page.setContentData(pageContentData);
      return true;
    } catch (Exception e) {
      logger.info(
          "Exception while fetching content for: {} [{}]",
          page.getWebURL().getURL(),
          e.getMessage());
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, page);
    }
    return false;
  }
}
