package com.salefinding.crawler.fetchers;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.PropUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.exceptions.PageBiggerThanMaxSizeException;
import edu.uci.ics.crawler4j.fetcher.PageFetchResult;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.http.client.config.RequestConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public abstract class CustomUrlPageFetcher extends PageFetcher {

  private static final Logger logger = LoggerFactory.getLogger(CustomUrlPageFetcher.class);

  protected ClBrandCountry brandCountry;

  protected RequestConfig requestConfig;

  public CustomUrlPageFetcher(ClBrandCountry brandCountry, CrawlConfig config)
      throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    super(config);
    this.brandCountry = brandCountry;
    int timeout = PropUtil.getInt(Constants.HTTP_TIMEOUT, Constants.CONNECTION_TIMEOUT_MS);
    this.requestConfig =
        RequestConfig.custom()
            .setConnectionRequestTimeout(timeout)
            .setConnectTimeout(timeout)
            .setSocketTimeout(timeout)
            .build();
  }

  @Override
  public PageFetchResult fetchPage(WebURL webUrl)
      throws InterruptedException, PageBiggerThanMaxSizeException, IOException {
    String realUrl = getRealToFetchUrl(webUrl);
    logger.info("Fetch page {} via new url {}", webUrl.getURL(), realUrl);
    return fetchPage(webUrl, realUrl);
  }

  protected abstract String getRealToFetchUrl(WebURL webUrl);
}
