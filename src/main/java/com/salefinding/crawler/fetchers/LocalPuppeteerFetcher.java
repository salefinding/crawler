package com.salefinding.crawler.fetchers;

import com.salefinding.crawler.models.PuppeteerModel;
import com.salefinding.crawler.utils.LocalPuppeteerUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class LocalPuppeteerFetcher extends PuppeteerFetcher {

  private static final Logger logger = LoggerFactory.getLogger(LocalPuppeteerFetcher.class);

  public LocalPuppeteerFetcher(ClBrandCountry brandCountry, CrawlConfig config)
      throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    super(brandCountry, config);
  }

  @Override
  protected PuppeteerModel getPuppeteerModel() {
    return LocalPuppeteerUtil.get().getModel(brandCountry.getId());
  }
}
