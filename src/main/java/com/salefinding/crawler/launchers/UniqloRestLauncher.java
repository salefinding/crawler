package com.salefinding.crawler.launchers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.*;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import com.salefinding.utils.SFObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Slf4j
public class UniqloRestLauncher extends BaseCrawlerLauncher {

  public UniqloRestLauncher(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  protected void crawlItems(Set<ClCategoryCrawler> clCategoryCrawlerSet) {
    log.info("Start crawling items ...");
    Queue<ItemCrawlerDTO> itemCrawlerDTOList = new ConcurrentLinkedQueue<>();

    log.info("Start crawling category items!!!!");
    List<Runnable> runnableList =
        clCategoryCrawlerSet.stream()
            .map(
                clCategoryCrawler ->
                    (Runnable)
                        () -> {
                          try {
                            String categoryId = getCategoryId(clCategoryCrawler);
                            ArrayNode productNodes = getProductNodes(categoryId, 0, 50);
                            List<String> productIds = getProductIdList(productNodes);
                            for (String productId : productIds) {
                              itemCrawlerDTOList.add(
                                  new ItemCrawlerDTO(productId, clCategoryCrawler));
                            }
                          } catch (Exception e) {
                            log.error(
                                "Error while parsing category {}", clCategoryCrawler.getUrl(), e);
                            CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                                .getErrorSummaryRepo()
                                .saveError(
                                    Constants.RUN_UID,
                                    String.format(
                                        "Error while parsing category %s-%s-%s",
                                        brandCountry.getBrand().getShortCode().toUpperCase(),
                                        brandCountry.getCountry().getShortCode().toUpperCase(),
                                        clCategoryCrawler.getUrl()),
                                    e,
                                    clCategoryCrawler);
                          }
                        })
            .collect(Collectors.toList());
    runInThreadPool(runnableList);
    log.info("Finish crawling category items!!!!");

    log.info("Start crawling items!!!!");
    runnableList =
        itemCrawlerDTOList.stream()
            .map(
                itemCrawlerDTO ->
                    (Runnable)
                        () -> {
                          if (!DuplicationUtil.isExistedItem(
                              itemCrawlerDTO.getCategoryCrawler().getBrandCountryId(),
                              itemCrawlerDTO.getProductId())) {
                            try {
                              Item item =
                                  getItem(
                                      brandCountry,
                                      itemCrawlerDTO.getCategoryCrawler(),
                                      itemCrawlerDTO.productId);
                              if (item == null) {
                                log.error("Error while parsing item {}", itemCrawlerDTO.productId);
                                return;
                              }

                              if (saveCrawler.saveItem(item) > 0) {
                                log.info(
                                    "Crawled {} items.",
                                    brandCountry.increaseLastCrawlingItemCount(1));
                                DuplicationUtil.addItem(
                                    itemCrawlerDTO.getCategoryCrawler().getBrandCountryId(),
                                    itemCrawlerDTO.getProductId());
                                CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                                    .getCrawlingDataRepo()
                                    .updateSavedItem(
                                        brandCountry.getId(), Constants.RUN_UID, item.getUrl());
                              } else {
                                CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                                    .getCrawlingDataRepo()
                                    .updateVisited(
                                        brandCountry.getId(), Constants.RUN_UID, item.getUrl());
                              }
                            } catch (Exception e) {
                              log.error("Error while parsing item {}", itemCrawlerDTO.productId, e);
                              CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                                  .getErrorSummaryRepo()
                                  .saveError(
                                      Constants.RUN_UID,
                                      String.format(
                                          "Error while parsing item %s-%s-%s",
                                          brandCountry.getBrand().getShortCode().toUpperCase(),
                                          brandCountry.getCountry().getShortCode().toUpperCase(),
                                          itemCrawlerDTO.getProductId()),
                                      e,
                                      itemCrawlerDTO);
                            }
                          } else {
                            log.info(
                                "Needn't crawl {} as it duplicated", itemCrawlerDTO.getProductId());
                          }
                        })
            .collect(Collectors.toList());
    runInThreadPool(runnableList);
    log.info("End crawling items!!!!");
  }

  @Override
  protected Set<ClCategoryCrawler> parseCategories() throws Exception {
    Set<ClCategoryCrawler> categories = new HashSet<>();

    String apiUrl =
        brandCountry.getStartUrl().substring(0, brandCountry.getStartUrl().indexOf("en/"))
            + "api/commerce/v3/en/layouts";
    String response =
        UrlHelper.getResponseString(
            apiUrl, ProxyUtil.get().getModel(brandCountry.getId()), false, 10000);

    JsonNode layoutNode = SFObjectMapper.toJsonNode(response, false);
    ArrayNode cat1Nodes =
        (ArrayNode) layoutNode.get("result").get("menu").get(0).get("children").get(0).get("items");
    for (JsonNode cat1Node : cat1Nodes) {
      String cat1 = cat1Node.get("label").asText().toUpperCase();
      for (JsonNode cat2Node : cat1Node.get("content").get(0).get("children")) {
        String cat2 = cat2Node.get("headingText").asText();
        for (JsonNode cat3Node : cat2Node.get("children").get(0).get("children")) {
          String cat3 = cat3Node.get("label").asText();
          String url = brandCountry.getStartUrl() + cat3Node.get("url").asText().substring(1);
          ClCategoryCrawler category = createCategory(cat1, cat2, cat3, null, null, url, false);
          if (category != null) {
            log.info(
                "Adding Category {}-{}-{}-{} : {}",
                brandCountry.getBrand().getName(),
                category.getCat1(),
                category.getCat2(),
                category.getCat3(),
                category.getUrl());
            categories.add(category);
          }
        }
      }
    }
    return categories;
  }

  private String getCategoryId(ClCategoryCrawler clCategoryCrawler) {
    log.info("Start getCategoryId for {}", clCategoryCrawler.getUrl());
    String categoryPath = clCategoryCrawler.getUrl().replace(brandCountry.getStartUrl(), "/");
    String url =
        brandCountry.getStartUrl().substring(0, brandCountry.getStartUrl().indexOf("en/"))
            + "api/commerce/v3/en/cms?path="
            + URLEncoder.encode(categoryPath, StandardCharsets.UTF_8);

    String response = getApiResponse(url, "categoryId");
    JsonNode node = SFObjectMapper.toJsonNode(response, false);

    String categoryId = node.get("result").get("properties").get("categoryId").asText();

    log.info("CategoryId for {} is {}", clCategoryCrawler.getUrl(), categoryId);

    return categoryId;
  }

  private ArrayNode getProductNodes(String categoryId, int offset, int limit) {
    log.info("Start getProductNodes for {} from {} to {}", categoryId, offset, offset + limit);
    String url =
        String.format(
            "%s/api/commerce/v3/en/products?path=%s&limit=%d&offset=%d",
            brandCountry.getStartUrl().substring(0, brandCountry.getStartUrl().indexOf("/en/")),
            URLEncoder.encode(",," + categoryId, StandardCharsets.UTF_8),
            limit,
            offset);

    String response = getApiResponse(url, "items");
    JsonNode node = SFObjectMapper.toJsonNode(response, false);

    ArrayNode result = new ObjectMapper().createArrayNode();
    result.addAll((ArrayNode) node.get("result").get("items"));

    int count = node.get("result").get("pagination").get("count").asInt();
    if (count > limit) {
      log.info("Count = {} is larger than limit = {}. Call recursive.", count, limit);
      result.addAll(getProductNodes(categoryId, offset + limit, limit));
    }

    log.info("End getProductNodes for {} from {} to {}", categoryId, offset, count);

    return result;
  }

  private List<String> getProductIdList(ArrayNode productNodes) {
    log.info("Start getProductIdList from productNodes");
    List<String> result = new ArrayList<>();

    int count = 0;
    for (JsonNode productNode : productNodes) {
      result.add(productNode.get("productId").asText());
      count++;
    }

    log.info("End getProductIdList from productNodes with count = {}", count);
    return result;
  }

  private Item getItem(
      ClBrandCountry brandCountry, ClCategoryCrawler categoryCrawler, String productId) {
    log.info("Start getItem for {}", productId);
    String url =
        brandCountry.getStartUrl().substring(0, brandCountry.getStartUrl().indexOf("en/"))
            + "api/commerce/v3/en/products/"
            + productId;
    Item item = new Item(brandCountry);
    item.setUrl(brandCountry.getStartUrl() + "products/" + productId);
    item.setCategoryId(categoryCrawler.getCategoryId());
    item.setCategoryCrawlerId(categoryCrawler.getId());
    item.setCode(productId);

    String response = getApiResponse(url, productId);
    if (response == null) {
      log.error("Empty response. Stop crawling item!");
      return null;
    }
    if (!response.contains("items")) {
      log.error("Exception as item {} response is INVALID. Response: {}", item.getCode(), response);
      throw new RuntimeException("Exception as item response is INVALID.");
    }

    JsonNode root = SFObjectMapper.toJsonNode(response, false);

    JsonNode node = root.get("result").get("items").get(0);

    item.setName(node.get("name").asText());
    if (node.get("prices").get("promo").isNull()) {
      item.setPrice(CommonUtil.getPrice(node.get("prices").get("base").get("value").asText()));
    } else {
      item.setPrice(CommonUtil.getPrice(node.get("prices").get("promo").get("value").asText()));
      if (!node.get("prices").get("base").isNull()) {
        item.setOldPrice(CommonUtil.getPrice(node.get("prices").get("base").get("value").asText()));
      } else {
        item.setOldPrice(item.getPrice().multiply(BigDecimal.valueOf(1.25)));
      }
    }
    log.info("Price for {}: {}-{}", item.getCode(), item.getPrice(), item.getOldPrice());

    parseImages(item, node);
    parseStocks(item, node);

    log.info("End getItem for {}", productId);
    return item;
  }

  private void parseImages(Item item, JsonNode node) {
    log.info("Start parseImages for {}", item.getCode());
    JsonNode imageNode = node.get("images");
    ArrayNode main = (ArrayNode) imageNode.get("main");
    ArrayNode chip = (ArrayNode) imageNode.get("chip");
    ArrayNode sub = (ArrayNode) imageNode.get("sub");

    Set<Item.Image> images = new HashSet<>();

    for (JsonNode t : main) {
      String url = t.get("url").asText();
      if (url.endsWith(".jpg")) {
        if (item.getImage() == null) {
          item.setImage(url);
        }
        images.add(new Item.Image(url));
      }
    }

    for (JsonNode t : chip) {
      String url = t.get("url").asText();
      if (url.endsWith(".jpg")) {
        images.add(new Item.Image(url));
      }
    }
    for (JsonNode t : sub) {
      String url = t.get("url").asText();
      if (url.endsWith(".jpg")) {
        images.add(new Item.Image(url));
      }
    }

    item.setImageList(images);
    log.info("END parseImages for {}", item.getCode());
  }

  private void parseStocks(Item item, JsonNode node) {
    log.info("START parseStocks for {}", item.getCode());
    ArrayNode l2sNode = (ArrayNode) node.get("l2s");
    Set<Item.Stock> stocks = new HashSet<>();
    int total = 0;

    for (JsonNode t : l2sNode) {
      Item.Stock stock = new Item.Stock();
      try {
        stock.setColorCode(t.get("color").get("code").asText());
        stock.setSizeCode(t.get("size").get("code").asText());
        stock.setStock(t.get("stock").get("quantity").asInt(0));

        if (t.get("prices").get("promo").isNull()) {
          if (!t.get("prices").get("base").isNull()) {
            stock.setPrice(CommonUtil.getPrice(t.get("prices").get("base").get("value").asText()));
          }
        } else {
          if (!t.get("prices").get("base").isNull()) {
            stock.setOldPrice(
                CommonUtil.getPrice(t.get("prices").get("base").get("value").asText()));
          }
          stock.setPrice(CommonUtil.getPrice(t.get("prices").get("promo").get("value").asText()));
        }

        total += stock.getStock();
        stocks.add(stock);
      } catch (Exception e) {
        log.error(
            "Error while parsing stock for {} , {}-{} but continue parsing others.",
            item.getCode(),
            stock.getColorCode(),
            stock.getSizeCode(),
            e);
      }
    }

    if (total > 0) {
      log.info("Product {} is AVAILABLE.", item.getCode());
      item.setAvailable(true);
    } else {
      log.info("Product {} is NOT AVAILABLE.", item.getCode());
      item.setAvailable(false);
    }
    item.setStockList(stocks);
    log.info("END parseStocks for {}", item.getCode());
  }

  @AllArgsConstructor
  @Data
  static class ItemCrawlerDTO {
    private String productId;
    private ClCategoryCrawler categoryCrawler;
  }
}
