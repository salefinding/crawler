package com.salefinding.crawler.launchers;

import com.salefinding.crawler.CustomCrawlController;
import com.salefinding.crawler.crawlers.BaseCrawlerFactory;
import com.salefinding.crawler.savingData.ISaveCrawlerMethod;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.PropUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

import java.util.ArrayList;
import java.util.Set;

public class CrawlerLauncher extends BaseCrawlerLauncher {

  public CrawlerLauncher(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  protected Set<ClCategoryCrawler> parseCategories() throws Exception {
    return null;
  }

  @Override
  protected void crawlItems(Set<ClCategoryCrawler> clCategoryCrawlerSet) throws Exception {
    CrawlConfig config = new CrawlConfig();
    config.setIncludeHttpsPages(true);
    config.setCrawlStorageFolder(
        "data/brand_country-"
            + brandCountry.getId()
            + "_brand-"
            + brandCountry.getBrandId()
            + "_country-"
            + brandCountry.getCountryId());
    config.setAuthInfos(new ArrayList<>());
    config.setPolitenessDelay(brandCountry.getPolitenessDelay());
    config.setMaxDownloadSize(10 * 1024 * 1024);

    int timeout = PropUtil.getInt(Constants.HTTP_TIMEOUT, Constants.CONNECTION_TIMEOUT_MS);
    config.setSocketTimeout(timeout);
    config.setConnectionTimeout(timeout);

    config.setResumableCrawling(PropUtil.getBoolean(Constants.RESUMABLE_CRAWLING, true));

    config.setUserAgentString(
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");

    RobotstxtConfig robotstxtConfig =
        new RobotstxtConfig() {
          {
            setEnabled(false);
          }
        };
    RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, new PageFetcher(config));

    CrawlController controller = new CustomCrawlController(config, robotstxtServer, brandCountry);

    for (ClCategoryCrawler category : clCategoryCrawlerSet) {
      controller.addSeed(category.getUrl());
    }

    BaseCrawlerFactory factory =
        (BaseCrawlerFactory)
            Class.forName(brandCountry.getCrawlerFactoryClass())
                .getConstructor(ISaveCrawlerMethod.class, ClBrandCountry.class, Set.class)
                .newInstance(saveCrawler, brandCountry, clCategoryCrawlerSet);

    controller.start(factory, brandCountry.getNumberOfThread());

    /*if (brandCountry.getBrandId() != 2 &&
        crawlerRepoFactory.getBrandCountryRepo().getNumberSameBrandRunning(brandCountry.getBrandId()) == 0) {
        //Can run transferDB because all of brand country are stopped
        logger.info("START TRANSFER for {}.", brandCountry.getBrand().getName());
        TransferToWP.getInstance().transfer(3, brandCountry.getBrandId());
        logger.info("END TRANSFER for {}.", brandCountry.getBrand().getName());
    }*/

  }
}
