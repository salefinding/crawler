package com.salefinding.crawler.launchers;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.savingData.DatabaseSaveCrawler;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.enums.BrandCountryRunning;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.models.crawler.ClCrawlingStatistics;
import com.salefinding.repositories.CrawlerRepoFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Slf4j
public abstract class BaseCrawlerLauncher implements Runnable {

  protected ClBrandCountry brandCountry;

  protected CrawlerRepoFactory crawlerRepoFactory;

  protected DatabaseSaveCrawler saveCrawler;

  public BaseCrawlerLauncher(ClBrandCountry brandCountry) {
    this.brandCountry = brandCountry;
    this.crawlerRepoFactory = CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER);
    this.saveCrawler = new DatabaseSaveCrawler();
  }

  @Override
  public void run() {
    try {
      long startTime = System.currentTimeMillis();

      PuppeteerDockerHelper.get().restartPuppeteerContainers(false, false);

      log.info(
          "START crawling for {}-{} !",
          brandCountry.getBrand().getName(),
          brandCountry.getCountry().getName());

      CommonUtil.registerBatch(brandCountry);

      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getCategoryRepo()
          .setCategoriesStatus(brandCountry.getBrandId(), false, false);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getCategoryRepo()
          .setCategoryCrawlersCrawled(brandCountry.getId(), false, false);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getItemRepo()
          .setAllItemsStatus(brandCountry.getBrandId(), null, false, false);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getItemRepo()
          .setAllItemDetailsStatus(brandCountry.getId(), false, false);

      brandCountry.setRunning(BrandCountryRunning.RUNNING.getValue());
      brandCountry.setLastCrawlingTime(0L);
      brandCountry.setLastCrawlingItemCount(0);
      brandCountry.setLastCrawlingAt(Timestamp.from(Instant.now()));
      crawlerRepoFactory.getBrandCountryRepo().save(brandCountry);

      Set<ClCategoryCrawler> clCategoryCrawlerSet = crawlCategories();

      crawlItems(clCategoryCrawlerSet);

      log.info(
          "FINISH crawling for {}-{} !",
          brandCountry.getBrand().getName(),
          brandCountry.getCountry().getName());

      long endTime = System.currentTimeMillis();
      brandCountry.setLastCrawlingTime((endTime - startTime) / 1000);
      brandCountry.setRunning(BrandCountryRunning.STOP.getValue());
      crawlerRepoFactory.getBrandCountryRepo().save(brandCountry);
      crawlerRepoFactory.getCrawlingStatisticsRepo().save(new ClCrawlingStatistics(brandCountry));
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, "Error when running crawling!", e, brandCountry);
      brandCountry.setRunning(BrandCountryRunning.STOP.getValue());
      crawlerRepoFactory.getBrandCountryRepo().save(brandCountry);
    } finally {
      CommonUtil.unregisterBatch(brandCountry);
    }
  }

  protected Set<ClCategoryCrawler> crawlCategories() throws Exception {
    try {
      Set<ClCategoryCrawler> clCategoryCrawlerSet = parseCategories();
      clCategoryCrawlerSet = saveCrawler.saveCategories(clCategoryCrawlerSet);
      return clCategoryCrawlerSet;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      RetryManageUtil.increaseRetryTimes(brandCountry.getStartUrl(), false);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return crawlCategories();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for " + brandCountry.getBrand().getName()),
                brandCountry.getStartUrl());
        throw e;
      }
    }
  }

  protected abstract void crawlItems(Set<ClCategoryCrawler> clCategoryCrawlerSet) throws Exception;

  protected abstract Set<ClCategoryCrawler> parseCategories() throws Exception;

  protected ClCategoryCrawler createCategory(
      String cat1,
      String cat2,
      String cat3,
      String cat4,
      String cat5,
      String url,
      boolean getRealUrl)
      throws Exception {
    if ("featured".equalsIgnoreCase(cat2)) {
      return null;
    }

    if (StringUtils.isBlank(url)) {
      throw new Exception("URL is blank.");
    }

    if (url.startsWith("//")) {
      if ("https".equalsIgnoreCase(brandCountry.getStartUrl().substring(0, 5))) {
        url = "https:" + url;
      } else {
        url = "http:" + url;
      }
    }

    if (!url.startsWith(brandCountry.getStartUrl())) { // relative link
      URI uri = URI.create(brandCountry.getStartUrl());
      url = String.format("%s://%s", uri.getScheme(), uri.getHost()) + url;
    }

    // Get a real URL of category url
    if (getRealUrl) {
      CommonUtil.sleep(brandCountry.getPolitenessDelay(), log);
      url = UrlHelper.getRealUrl(url, null, brandCountry.getRepeat(), brandCountry.getId(), true);
    }

    ClCategoryCrawler category = new ClCategoryCrawler(brandCountry);

    category.setCat1(cat1 != null ? CommonUtil.getDictionaryValue(cat1.toUpperCase()) : null);
    category.setCat2(cat2 != null ? CommonUtil.getDictionaryValue(cat2.toUpperCase()) : null);
    category.setCat3(cat3 != null ? CommonUtil.getDictionaryValue(cat3.toUpperCase()) : null);
    category.setCat4(cat4 != null ? CommonUtil.getDictionaryValue(cat4.toUpperCase()) : null);
    category.setCat5(cat5 != null ? CommonUtil.getDictionaryValue(cat5.toUpperCase()) : null);
    category.setUrl(url);

    return category;
  }

  protected void runInThreadPool(List<Runnable> runnableList) {
    CommonUtil.runInThreadPool(
        runnableList,
        String.format(
                "%s-%s-Crawler-",
                brandCountry.getBrand().getShortCode(), brandCountry.getCountry().getShortCode())
            + "%d",
        brandCountry.getNumberOfThread());
  }

  protected String getApiResponse(String uri, String mustHave) {
    String response = CommonUtil.getResponse(brandCountry, uri, 0, mustHave, false);
    if (response != null && response.contains("</html>")) {
      Document doc = Jsoup.parse(response);
      return doc.getElementsByTag("pre").first().text();
    }
    return response;
  }

  protected Document getHtmlResponse(String uri, String mustHave, boolean usePuppeteer) {
    String response = CommonUtil.getResponse(brandCountry, uri, 0, mustHave, usePuppeteer);
    if (response != null && response.contains("</html>")) {
      return Jsoup.parse(response);
    }
    return null;
  }
}
