package com.salefinding.crawler.launchers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.DuplicationUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import com.salefinding.repositories.crawler.ClErrorSummaryRepository;
import com.salefinding.utils.SFObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class AdidasRestLauncher extends BaseCrawlerLauncher {

  private static final String SG_PRODUCTS_URL =
      "https://www.adidas.com.sg/on/demandware.static/-/Sites-CustomerFileStore/default/adidas-SG/en_SG/sitemaps/product/adidas-SG-en-sg-product.xml";
  private static final String SG_PRODUCT_DETAIL_URL_FORMAT =
      "https://www.adidas.com.sg/api/products/%s";
  private static final String SG_PRODUCT_STOCK_URL_FORMAT =
      "https://www.adidas.com.sg/api/products/%s/availability";
  private static final List<String> STATIC_MAIN_CATS =
      Arrays.asList("NAM", "NỮ", "TRẺ EM", "SƠ SINH");

  private final ClErrorSummaryRepository errorSummaryRepo;

  public AdidasRestLauncher(ClBrandCountry brandCountry) {
    super(brandCountry);
    errorSummaryRepo = crawlerRepoFactory.getErrorSummaryRepo();
  }

  @Override
  protected Set<ClCategoryCrawler> parseCategories() {
    return new java.util.HashSet<>(
        crawlerRepoFactory.getCategoryRepo().getCatCrawlers(brandCountry.getId()));
  }

  @Override
  protected void crawlItems(Set<ClCategoryCrawler> clCategoryCrawlerSet) {
    log.info("Start crawling items ...");
    Set<Item> items = crawlItemIdList();

    Map<String, ClCategoryCrawler> categoryCrawlers = new ConcurrentHashMap<>();
    clCategoryCrawlerSet.forEach(cat -> categoryCrawlers.put(buildCatKey(cat), cat));

    log.info("Start crawling items!!!!");
    List<Runnable> runnableList =
        items.stream()
            .filter(item -> !DuplicationUtil.isExistedItem(brandCountry.getId(), item.getCode()))
            .map(
                item ->
                    (Runnable)
                        () -> {
                          crawlItemDetail(item, categoryCrawlers);
                          DuplicationUtil.addItem(brandCountry.getId(), item.getCode());
                        })
            .collect(Collectors.toList());
    runInThreadPool(runnableList);

    for (ClCategoryCrawler cat : categoryCrawlers.values()) {
      cat = saveCrawler.saveCategory(cat);
      categoryCrawlers.put(buildCatKey(cat), cat);
    }

    runnableList =
        items.stream()
            .map(
                item ->
                    (Runnable)
                        () -> {
                          try {
                            if (item.getCategoryId() == null && !item.isErrorWhileCrawling()) {
                              if (item.getCategoryCrawler() == null) {
                                errorSummaryRepo.saveError(
                                    Constants.RUN_UID,
                                    String.format(
                                        "Error while saving item %s-%s-%s as no category",
                                        brandCountry.getBrand().getShortCode().toUpperCase(),
                                        brandCountry.getCountry().getShortCode().toUpperCase(),
                                        item.getCode()),
                                    null,
                                    item);
                              } else {
                                ClCategoryCrawler cat =
                                    categoryCrawlers.get(buildCatKey(item.getCategoryCrawler()));
                                item.setCategoryId(cat.getCategoryId());
                                item.setCategoryCrawlerId(cat.getId());
                                saveItem(item);
                              }
                            }
                          } catch (Exception e) {
                            log.error("Error while saving item {}", item.getCode(), e);
                            errorSummaryRepo.saveError(
                                Constants.RUN_UID,
                                String.format(
                                    "Error while saving item %s-%s-%s",
                                    brandCountry.getBrand().getShortCode().toUpperCase(),
                                    brandCountry.getCountry().getShortCode().toUpperCase(),
                                    item.getCode()),
                                e,
                                item);
                          }
                        })
            .collect(Collectors.toList());
    runInThreadPool(runnableList);

    log.info("End crawling items!!!!");
  }

  private Set<Item> crawlItemIdList() {
    Set<Item> result = new HashSet<>();
    String countryCode = brandCountry.getCountry().getShortCode();
    String url =
        SG_PRODUCTS_URL
            .replace("SG", countryCode.toUpperCase())
            .replace("sg", countryCode.toLowerCase());
    if ("VN".equalsIgnoreCase(countryCode)) {
      url = url.replace("en", "vi");
    }
    String response = getApiResponse(url, ".html");

    Pattern pattern = Pattern.compile("https://.*\\.html");
    Matcher matcher = pattern.matcher(response);
    while (matcher.find()) {
      String itemUrl = matcher.group();
      String code = itemUrl.substring(itemUrl.lastIndexOf('/') + 1, itemUrl.lastIndexOf(".html"));
      Item item = new Item(brandCountry);
      item.setCode(code);
      item.setUrl(itemUrl);

      result.add(item);
    }

    return result;
  }

  private void crawlItemDetail(Item item, Map<String, ClCategoryCrawler> categoryCrawlers) {
    log.info("Start crawling item detail for {} ...", item.getCode());
    try {
      String countryCode = brandCountry.getCountry().getShortCode();
      String url =
          String.format(
              SG_PRODUCT_DETAIL_URL_FORMAT.replace("sg", countryCode.toLowerCase()),
              item.getCode());
      String response = getApiResponse(url, item.getCode());
      if (response == null) {
        log.error("Empty response. Stop crawling item!");
        item.setErrorWhileCrawling(true);
        return;
      }
      if (!response.contains("breadcrumb_list")) {
        log.error(
            "Exception as item {} doesn't have category. Response: {}", item.getCode(), response);
        item.setErrorWhileCrawling(true);
        throw new RuntimeException("Exception as item doesn't have category");
      }

      JsonNode node = SFObjectMapper.toJsonNode(response, true);

      // Parse category
      parseCategory(item, node, categoryCrawlers);

      item.setName(node.get("name").asText());

      parsePrice(item, node);
      parseImages(item, node);
      parseStock(item);

      if (item.getCategoryId() != null) {
        saveItem(item);
      }

      log.info("End crawling item detail for {}.", item.getCode());
    } catch (Exception e) {
      log.error("Error while parsing item {}", item.getCode(), e);
      errorSummaryRepo.saveError(
          Constants.RUN_UID,
          String.format(
              "Error while parsing item %s-%s-%s",
              brandCountry.getBrand().getShortCode().toUpperCase(),
              brandCountry.getCountry().getShortCode().toUpperCase(),
              item.getCode()),
          e,
          item);
    }
  }

  private void parseCategory(
      Item item, JsonNode node, Map<String, ClCategoryCrawler> categoryCrawlers) {
    try {
      log.info("Start parsing category for {}", item.getCode());

      ArrayNode catsNode = (ArrayNode) node.get("breadcrumb_list");
      String cat1 = null, cat2 = null, cat3 = null, cat4 = null;
      String catUrl = "";

      if (catsNode.size() > 0) {
        JsonNode t = catsNode.get(0);
        cat1 = t.get("text").asText().toUpperCase();
        catUrl = t.get("link").asText();
      }
      if (catsNode.size() > 1) {
        JsonNode t = catsNode.get(1);
        cat2 = t.get("text").asText().toUpperCase();
        catUrl = t.get("link").asText();
      }
      if (catsNode.size() > 2) {
        JsonNode t = catsNode.get(2);
        cat3 = t.get("text").asText().toUpperCase();
        catUrl = t.get("link").asText();
      }
      if (catsNode.size() > 3) {
        JsonNode t = catsNode.get(3);
        cat4 = t.get("text").asText().toUpperCase();
        catUrl = t.get("link").asText();
      }

      ClCategoryCrawler category =
          createCategory(
              cat1,
              cat2,
              cat3,
              cat4,
              null,
              brandCountry.getStartUrl() + catUrl.substring(1),
              false);

      String key = buildCatKey(category);
      if (categoryCrawlers.containsKey(key)) {
        category = categoryCrawlers.get(key);
        item.setCategoryId(category.getCategoryId());
        item.setCategoryCrawlerId(category.getId());
      } else {
        categoryCrawlers.put(key, category);
      }
      item.setCategoryCrawler(category);

      log.info(
          "Category for {} : {}-{}-{}-{}-{}-{}",
          item.getCode(),
          category.getId(),
          brandCountry.getBrand().getName(),
          category.getCat1(),
          category.getCat2(),
          category.getCat3(),
          category.getUrl());
    } catch (Exception e) {
      log.error("Error while parsing category. Node = {}", node, e);
    }
  }

  private void parseImages(Item item, JsonNode node) {
    log.info("Start parsing images for {}", item.getCode());
    try {
      ArrayNode imagesNode = (ArrayNode) node.get("view_list");
      Set<Item.Image> images = new HashSet<>();
      for (JsonNode t : imagesNode) {
        String url = t.get("image_url").asText();
        if (url.endsWith(".jpg")) {
          if (item.getImage() == null) {
            item.setImage(url);
          }
          images.add(new Item.Image(url));
        }
      }

      item.setImageList(images);
      log.info("END parsing images for {}", item.getCode());
    } catch (Exception e) {
      log.error("Error while parsing images for {}", item.getCode(), e);
    }
  }

  private void parsePrice(Item item, JsonNode node) {
    log.info("Start parsing price for {}", item.getCode());
    JsonNode t = node.get("pricing_information");

    if (t.get("sale_price") != null && !t.get("sale_price").isNull()) {
      item.setPrice(CommonUtil.getPrice(t.get("sale_price").asText()));
      item.setOldPrice(CommonUtil.getPrice(t.get("standard_price").asText()));
    } else {
      item.setPrice(CommonUtil.getPrice(t.get("standard_price").asText()));
    }
    log.info("Price for {}: {}-{}", item.getCode(), item.getPrice(), item.getOldPrice());
  }

  private void parseStock(Item item) {
    log.info("Start parsing stock for {}", item.getCode());
    try {
      String url =
          String.format(
              SG_PRODUCT_STOCK_URL_FORMAT.replace(
                  "sg", brandCountry.getCountry().getShortCode().toLowerCase()),
              item.getCode());
      String response = getApiResponse(url, item.getCode());
      if (response == null) {
        log.error("Empty response. Stop crawling stock !");
        return;
      }
      if (!response.contains("variation_list")) {
        log.error("Cannot parse stock. Response: {}", response);
        throw new RuntimeException("Exception as cannot parse stock for " + item.getCode());
      }
      JsonNode node = SFObjectMapper.toJsonNode(response, true);

      int total = 0;
      if (node.has("variation_list") && !node.get("variation_list").isNull()) {
        for (JsonNode variation : node.get("variation_list")) {
          String sizeCode = variation.get("size").asText();
          int qty = variation.get("availability").asInt();
          item.getStockList().add(new Item.Stock(null, sizeCode, qty));
          total += qty;
        }
      }

      if (total > 0) {
        log.info("Product {} is AVAILABLE.", item.getCode());
        item.setAvailable(true);
      } else {
        log.info("Product {} is NOT AVAILABLE.", item.getCode());
        item.setAvailable(false);
      }

      log.info("End parsing stock for {}", item.getCode());
    } catch (Exception e) {
      log.error("Error while parsing stock for {}", item.getCode(), e);
      log.info("Product {} is NOT AVAILABLE.", item.getCode());
      item.setAvailable(false);
      errorSummaryRepo.saveError(
          Constants.RUN_UID,
          String.format(
              "Error while parsing stock for %s-%s-%s",
              brandCountry.getBrand().getShortCode().toUpperCase(),
              brandCountry.getCountry().getShortCode().toUpperCase(),
              item.getCode()),
          e,
          item);
    }
  }

  private String buildCatKey(ClCategoryCrawler category) {
    if (!STATIC_MAIN_CATS.contains(category.getCat1())) {
      category.setCat3(category.getCat2());
      category.setCat2(category.getCat1());
      category.setCat1("ĐỒ THỂ THAO");
    }
    return String.format("%s-%s-%s", category.getCat1(), category.getCat2(), category.getCat3());
  }

  private void saveItem(Item item) {
    if (saveCrawler.saveItem(item) > 0) {
      log.info("Crawled {} items.", brandCountry.increaseLastCrawlingItemCount(1));
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getCrawlingDataRepo()
          .updateSavedItem(brandCountry.getId(), Constants.RUN_UID, item.getUrl());
    } else {
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getCrawlingDataRepo()
          .updateVisited(brandCountry.getId(), Constants.RUN_UID, item.getUrl());
    }
  }
}
