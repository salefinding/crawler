package com.salefinding.crawler.launchers;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.DuplicationUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class CKLauncher extends BaseCrawlerLauncher {

  private static final String CATEGORY_XML_URL =
      "https://www.charleskeith.com/sg/sitemap_2-category.xml";
  private static final String ITEM_XML_URL = "https://www.charleskeith.com/sg/sitemap_1-image.xml";

  private Set<String> WOMEN_CATS =
      new HashSet<>(
          Arrays.asList("SHOES", "BAGS", "WALLETS", "SUNGLASSES", "JEWELLERY", "ACCESSORIES"));

  private Set<String> KID_CATS = new HashSet<>(Arrays.asList("SHOES", "BAGS"));

  protected int itemPerPage;

  public CKLauncher(ClBrandCountry brandCountry) {
    super(brandCountry);
    itemPerPage = 90;
  }

  @Override
  protected Set<ClCategoryCrawler> parseCategories() throws Exception {
    Set<ClCategoryCrawler> categories = new HashSet<>();

    for (String cat : WOMEN_CATS) {
      ClCategoryCrawler category =
          createCategory(
              "WOMEN",
              cat,
              null,
              null,
              null,
              String.format("%s%s?page=1", brandCountry.getStartUrl(), cat),
              false);
      if (!categories.contains(category)) {
        saveAllPagesOfCat(categories, category);
      }

      category =
          createCategory(
              "WOMEN",
              cat,
              "SALE",
              null,
              null,
              String.format("%ssale/%s?page=1", brandCountry.getStartUrl(), cat),
              false);
      if (!categories.contains(category)) {
        saveAllPagesOfCat(categories, category);
      }
    }

    for (String cat : KID_CATS) {
      ClCategoryCrawler category =
          createCategory(
              "KIDS",
              cat,
              null,
              null,
              null,
              String.format("%s%s?page=1", brandCountry.getStartUrl(), cat),
              false);
      if (!categories.contains(category)) {
        saveAllPagesOfCat(categories, category);
      }
    }

    return categories;
  }

  private void saveAllPagesOfCat(Set<ClCategoryCrawler> categories, ClCategoryCrawler category) {
    try {
      int count = 1;
      Document doc = getHtmlResponse(category.getUrl(), category.getUrl(), false);

      if (doc != null) {
        categories.add(category);
        log.info(
            "Adding Category {}-{}-{}-{} : {}",
            brandCountry.getBrand().getName(),
            category.getCat1(),
            category.getCat2(),
            category.getCat3(),
            category.getUrl());
      }

      Element footer =
          doc.getElementsByAttributeValueContaining("class", "product_list-footer").first();

      if (footer != null) {
        String[] noList =
            footer.text().replaceAll("[^0-9 ]", "").trim().replaceAll(" {2}", " ").split(" ");
        int total = Integer.parseInt(noList[noList.length - 1]);
        while (count * itemPerPage < total) {
          count += 1;
          ClCategoryCrawler newCat =
              createCategory(
                  category.getCat1(),
                  category.getCat2(),
                  category.getCat3(),
                  null,
                  null,
                  category.getUrl().replace("page=1", "page=" + count),
                  false);
          categories.add(newCat);
          log.info(
              "Adding Category {}-{}-{}-{} : {}",
              brandCountry.getBrand().getName(),
              newCat.getCat1(),
              newCat.getCat2(),
              newCat.getCat3(),
              newCat.getUrl());
        }
      }
    } catch (Exception e) {
      log.error("No more pages for the category " + category.getUrl(), e);
    }
  }

  @Override
  protected void crawlItems(Set<ClCategoryCrawler> categoryCrawlers) throws Exception {
    Map<String, Item> itemMap = retrieveItemMap();
    List<Item> items = new Vector<>();

    runInThreadPool(
        categoryCrawlers.stream()
            .map(
                clCategoryCrawler ->
                    ((Runnable)
                        () -> items.addAll(getItemUrlOfCategory(clCategoryCrawler, itemMap))))
            .collect(Collectors.toList()));

    runInThreadPool(
        items.stream()
            .filter(item -> !DuplicationUtil.isExistedItem(brandCountry.getId(), item.getCode()))
            .map(
                item ->
                    (Runnable)
                        () -> {
                          try {
                            crawlItemDetail(item);
                            saveCrawler.saveItem(item);
                            brandCountry.increaseLastCrawlingItemCount(1);
                            DuplicationUtil.addItem(
                                item.getCategoryCrawler().getBrandCountryId(), item.getCode());
                          } catch (Exception e) {
                            log.error(e.getMessage(), e);
                            crawlerRepoFactory
                                .getErrorSummaryRepo()
                                .saveError(
                                    Constants.RUN_UID,
                                    String.format(
                                        "Error while parsing item %s-%s-%s",
                                        brandCountry.getBrand().getShortCode().toUpperCase(),
                                        brandCountry.getCountry().getShortCode().toUpperCase(),
                                        item.getCode()),
                                    e,
                                    item);
                          }
                        })
            .collect(Collectors.toList()));
  }

  private Map<String, Item> retrieveItemMap() {
    Map<String, Item> result = new HashMap<>();

    String url = ITEM_XML_URL.replace("sg", brandCountry.getCountry().getShortCode());
    String xmlData = getApiResponse(url, brandCountry.getStartUrl());
    if (xmlData != null) {
      Document doc = Jsoup.parse(xmlData, "", Parser.xmlParser());
      for (Element e : doc.select("url")) {
        String itemUrl = e.getElementsByTag("loc").first().text();
        Item item = buildItem(itemUrl);

        for (Element imgE : e.getElementsByTag("image:image")) {
          String imgUrl = imgE.getElementsByTag("image:loc").first().text();
          if (StringUtils.isEmpty(item.getImage())) {
            item.setImage(imgUrl);
          }
        }

        result.put(item.getUrl(), item);
      }
    }

    return result;
  }

  private Set<Item> getItemUrlOfCategory(
      ClCategoryCrawler categoryCrawler, Map<String, Item> itemMap) {
    Set<Item> result = new HashSet<>();
    String prefix = "https://" + brandCountry.getStartUrl().split("/")[2];
    Pattern filter = Pattern.compile(brandCountry.getFilter());

    try {
      Document doc = getHtmlResponse(categoryCrawler.getUrl(), categoryCrawler.getUrl(), false);
      if (doc == null) {
        log.error("Cannot get items as invalid response.");
        return result;
      }
      Elements links = doc.select("a[href]");
      for (Element link : links) {
        String url = link.attr("href");
        if (url.startsWith(prefix)) {
          continue;
        }
        url = prefix + url;

        Item item = null;
        if (itemMap.containsKey(url)) {
          item = itemMap.get(url);
        } else if (filter.matcher(url).matches()) {
          item = buildItem(url);
        }

        if (item != null) {
          item.setCategoryCrawler(categoryCrawler);
          item.setCategoryId(categoryCrawler.getCategoryId());
          item.setCategoryCrawlerId(categoryCrawler.getId());
          result.add(item);
        }
      }

      log.info("Found {} items in category {}", result.size(), categoryCrawler.getUrl());
    } catch (Exception e) {
      log.error("Error while getting item from category {}", categoryCrawler.getUrl(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(
              Constants.RUN_UID,
              String.format("Error while getting item from category %s", categoryCrawler.getUrl()),
              e,
              categoryCrawler);
    }
    return result;
  }

  private void crawlItemDetail(Item item) {
    Document doc = getHtmlResponse(item.getUrl(), item.getCode(), false);
    if (doc == null) {
      log.error("Cannot get item detail as invalid response.");
      return;
    }

    String name = doc.getElementsByAttributeValueContaining("class", "product-name").text();
    item.setName(name);

    parsePrice(item, doc);

    if (StringUtils.isEmpty(item.getImage())) {
      parseImages(item, doc);
    }

    parseStock(item, doc);
  }

  private void parsePrice(Item item, Document doc) {
    Element pricesE = doc.getElementsByAttributeValueContaining("class", "prices").first();
    item.setPrice(
        CommonUtil.getPrice(
            pricesE.getElementsByAttributeValue("itemprop", "price").first().attr("content")));
    if (item.getPrice() == null) {
      throw new RuntimeException("Price cannot be null!");
    }
    Elements discountE = pricesE.getElementsByAttributeValueContaining("class", "strike-through");
    if (discountE.size() > 0) {
      item.setOldPrice(CommonUtil.getPrice(discountE.first().child(0).text()));
    }
  }

  private void parseImages(Item item, Document doc) {
    try {
      Element parent =
          doc.getElementsByAttributeValueContaining("class", "product_gallery").first();
      for (Element e : parent.select("img[class*=js-fullscreenslides--zoom]")) {
        String url = e.attr("data-src");
        if (url.indexOf("?") > 0) {
          url = url.substring(0, url.indexOf("?"));
        }
        item.getImageList().add(new Item.Image(url));
        if (item.getImage() == null) {
          item.setImage(url);
        }
      }
    } catch (Exception e) {
      log.error("Error while parsing images for {}", item.getCode(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(
              Constants.RUN_UID,
              String.format(
                  "Error while parsing images for %s-%s-%s",
                  brandCountry.getBrand().getShortCode().toUpperCase(),
                  brandCountry.getCountry().getShortCode().toUpperCase(),
                  item.getCode()),
              e,
              item);
    }
  }

  private void parseStock(Item item, Document doc) {
    try {
      String color =
          doc.getElementsByAttributeValueContaining("class", "selected-color").first().text();
      Element st = doc.getElementsByAttributeValueContaining("class", "attribute-values").get(1);
      for (Element stockE : st.children()) {
        Element t = stockE.child(0);
        if (!t.attr("class").contains("unselectable")) {
          item.getStockList().add(new Item.Stock(color, t.text(), 1));
        }
      }
      if (item.getStockList().size() > 0) {
        item.setAvailable(true);
      }
    } catch (Exception e) {
      log.error("Error while parsing stock for {}", item.getCode(), e);
      crawlerRepoFactory
          .getErrorSummaryRepo()
          .saveError(
              Constants.RUN_UID,
              String.format(
                  "Error while parsing stock for %s-%s-%s",
                  brandCountry.getBrand().getShortCode().toUpperCase(),
                  brandCountry.getCountry().getShortCode().toUpperCase(),
                  item.getCode()),
              e,
              item);
    }
  }

  private Item buildItem(String itemUrl) {
    Item item = new Item(brandCountry);
    item.setUrl(itemUrl);
    item.setCode(itemUrl.substring(itemUrl.lastIndexOf('/') + 1, itemUrl.indexOf(".html")));
    return item;
  }
}
