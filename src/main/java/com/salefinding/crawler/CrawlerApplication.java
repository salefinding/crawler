package com.salefinding.crawler;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.launchers.BaseCrawlerLauncher;
import com.salefinding.crawler.launchers.CrawlerLauncher;
import com.salefinding.crawler.utils.*;
import com.salefinding.enums.BrandCountryRunning;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.gen.ClBrandCountryGen;
import com.salefinding.repositories.CrawlerRepoFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
public class CrawlerApplication {

  public static void main(String[] args) {
    try {
      log.info("START program!");

      CommonUtil.initDictionary();

      if (!PuppeteerDockerHelper.get().usingOpenVpnDocker()
          && StaticValues.dockerInstanceNo > ProxyUtil.get().getNumberOfProxyList()) {
        StaticValues.dockerInstanceNo = ProxyUtil.get().getNumberOfProxyList();
      }

      List<ClBrandCountry> brandCountryList =
          CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
              .getBrandCountryRepo()
              .getAllEnabled();

      ExecutorService executor = Executors.newFixedThreadPool(StaticValues.controllerNo);

      log.info("Running NOT BLANK fetcher class brands !!!");
      List<Callable<Object>> callableList =
          brandCountryList.stream()
              .sorted(Comparator.comparing(ClBrandCountryGen::getOrder))
              .map(brandCountry -> Executors.callable(createLauncher(brandCountry)))
              .collect(Collectors.toList());
      executor.invokeAll(callableList);

      /*logger.info("START TRANSFER.");
      TransferToAPIDB.get().transfer(200);
      logger.info("END TRANSFER.");*/

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e);
    } finally {
      PuppeteerDockerHelper.get().removeAllPuppeteerContainers();

      log.info("END program!");
    }

    System.exit(0);
  }

  private static BaseCrawlerLauncher createLauncher(ClBrandCountry brandCountry) {
    brandCountry.setRunning(BrandCountryRunning.WAIT_TO_RUN.getValue());
    CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER).getBrandCountryRepo().save(brandCountry);
    if (StringUtils.isNotBlank(brandCountry.getFetcherClass())) {
      if (brandCountry.getFetcherClass().toLowerCase().contains("puppeteer")) {
        PuppeteerUtilMap.addToMap(brandCountry);
      }
    }

    BaseCrawlerLauncher launcher;
    try {
      launcher =
          (BaseCrawlerLauncher)
              Class.forName(brandCountry.getLauncherClass())
                  .getConstructor(ClBrandCountry.class)
                  .newInstance(brandCountry);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      launcher = new CrawlerLauncher(brandCountry);
    }

    log.info(
        "SCHEDULE crawling for {}-{} !",
        brandCountry.getBrand().getName(),
        brandCountry.getCountry().getName());
    return launcher;
  }
}
