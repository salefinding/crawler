package com.salefinding.crawler.crawlers;

import com.salefinding.crawler.parser.IParser;
import com.salefinding.crawler.savingData.ISaveCrawlerMethod;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategory;

import java.util.Set;

public class PagingBrandCrawlerFactory extends BaseCrawlerFactory {

  public PagingBrandCrawlerFactory(
      ISaveCrawlerMethod savingItemMethod,
      ClBrandCountry brandCountry,
      Set<ClCategory> categories) {
    super(savingItemMethod, brandCountry, categories);
  }

  @Override
  public BaseCrawler newInstance() throws Exception {
    IParser parser =
        (IParser)
            Class.forName(brandCountry.getParserClass())
                .getConstructor(ClBrandCountry.class, Set.class)
                .newInstance(brandCountry, categories);

    return (BaseCrawler)
        Class.forName(brandCountry.getCrawlerClass())
            .getConstructor(
                ISaveCrawlerMethod.class, IParser.class, ClBrandCountry.class, Set.class)
            .newInstance(savingItemMethod, parser, brandCountry, categories);
  }
}
