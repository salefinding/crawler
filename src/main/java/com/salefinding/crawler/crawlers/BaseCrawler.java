package com.salefinding.crawler.crawlers;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.parser.IParser;
import com.salefinding.crawler.savingData.ISaveCrawlerMethod;
import com.salefinding.crawler.utils.*;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.crawler.exceptions.ParseException;
import edu.uci.ics.crawler4j.frontier.Frontier;
import edu.uci.ics.crawler4j.url.WebURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

public abstract class BaseCrawler extends WebCrawler {

  protected ISaveCrawlerMethod savingDataMethod;

  protected IParser parser;

  protected ClBrandCountry brandCountry;

  protected Logger logger;

  protected Pattern filters;

  private static final Pattern FILE_ENDING_EXCLUSION_PATTERN =
      Pattern.compile(
          ".*(\\.("
              + "css|js"
              + "|bmp|gif|jpe?g|JPE?G|png|tiff?|ico|nef|raw"
              + "|mid|mp2|mp3|mp4|wav|wma|flv|mpe?g"
              + "|avi|mov|mpeg|ram|m4v|wmv|rm|smil"
              + "|pdf|doc|docx|pub|xls|xlsx|vsd|ppt|pptx"
              + "|swf"
              + "|zip|rar|gz|bz2|7z|bin"
              + "|xml|txt|java|c|cpp|exe"
              + "))$");

  protected Frontier frontier;

  protected int failedCount = 0;

  public BaseCrawler(
      ISaveCrawlerMethod savingDataMethod, IParser parser, ClBrandCountry brandCountry) {
    this.savingDataMethod = savingDataMethod;
    this.parser = parser;
    this.brandCountry = brandCountry;
    this.filters =
        Pattern.compile(getBaseStartUrl(brandCountry.getStartUrl()) + brandCountry.getFilter());
    this.logger =
        LoggerFactory.getLogger(
            "Brand-" + brandCountry.getBrandId() + "-" + brandCountry.getCountryId());
  }

  @Override
  public void init(int id, CrawlController crawlController)
      throws InstantiationException, IllegalAccessException {
    super.init(id, crawlController);
    this.logger =
        LoggerFactory.getLogger(
            "Brand-" + brandCountry.getBrandId() + "-" + brandCountry.getCountryId());
    this.frontier = crawlController.getFrontier();
  }

  @Override
  protected void onUnexpectedStatusCode(
      WebURL webUrl, int statusCode, String contentType, String description) {
    onCrawlingError(webUrl, new RuntimeException("UnexpectedStatusCode!"));
  }

  @Override
  protected void onContentFetchError(Page page) {
    onCrawlingError(page.getWebURL(), new Exception("ContentFetchError!"));
  }

  @Override
  protected void onParseError(WebURL webUrl, ParseException e) {
    onCrawlingError(webUrl, new RuntimeException("ParseException!", e));
  }

  @Override
  protected void onNotAllowedContentException(WebURL webUrl) {
    onCrawlingError(webUrl, new RuntimeException("NotAllowedContentException!"));
  }

  @Override
  protected void onUnhandledException(WebURL webUrl, Throwable e) {
    onCrawlingError(webUrl, new RuntimeException("UnhandledException!", e));
  }

  @Override
  protected void onPageBiggerThanMaxSize(String urlStr, long pageSize) {
    logger.error("Error when crawling (onPageBiggerThanMaxSize) " + urlStr);
    CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
        .getErrorSummaryRepo()
        .saveError(Constants.RUN_UID, new RuntimeException("PageBiggerThanMaxSize!"), urlStr);
  }

  /**
   * This method receives two parameters. The first parameter is the page in which we have
   * discovered this new url and the second parameter is the new url. You should implement this
   * function to specify whether the given url should be crawled or not (based on your crawling
   * logic). In this example, we are instructing the crawler to ignore urls that have css, js, git,
   * ... extensions and to only accept urls that start with "http://www.ics.uci.edu/". In this case,
   * we didn't need the referringPage parameter to make the decision.
   */
  @Override
  public boolean shouldVisit(Page referringPage, WebURL url) {
    String href = url.getURL().toLowerCase();
    boolean result = !FILE_ENDING_EXCLUSION_PATTERN.matcher(href).matches();
    result &= filters.matcher(href).matches();
    if (result) {
      result = !DuplicationUtil.isExistedItem(brandCountry.getId(), brandCountry.getBrandId(), url);
    }
    return result;
  }

  /** This function is called when a page is fetched and ready to be processed by your program. */
  @Override
  public void visit(Page page) {
    try {
      if (PauseAllProcessesUtil.waitIfLocked() == 1) {
        failedCount = 0;
      }

      Item clItem = parser.getItem(page);

      int count = savingDataMethod.saveItem(clItem);
      if (count > 0) {
        int totalCount = brandCountry.increaseLastCrawlingItemCount(count);
        logger.info("Crawled {} items.", totalCount);
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getCrawlingDataRepo()
            .updateSavedItem(brandCountry.getId(), Constants.RUN_UID, page.getWebURL().getURL());
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getCrawlingDataRepo()
            .updateVisited(brandCountry.getId(), Constants.RUN_UID, page.getWebURL().getURL());
      }

      decreaseFailCount();
    } catch (Throwable e) {
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getCrawlingDataRepo()
          .updateVisited(brandCountry.getId(), Constants.RUN_UID, page.getWebURL().getURL());
      this.onCrawlingError(page.getWebURL(), e);
    }
  }

  /**
   * Determine whether links found at the given URL should be added to the queue for crawling. By
   * default this method returns true always, but classes that extend WebCrawler can override it in
   * order to implement particular policies about which pages should be mined for outgoing links and
   * which should not.
   *
   * <p>If links from the URL are not being followed, then we are not operating as a web crawler and
   * need not check robots.txt before fetching the single URL. (see definition at
   * http://www.robotstxt.org/faq/what.html). Thus URLs that return false from this method will not
   * be subject to robots.txt filtering.
   *
   * @param url the URL of the page under consideration
   * @return true if outgoing links from this page should be added to the queue.
   */
  @Override
  protected boolean shouldFollowLinksIn(WebURL url) {
    return parser.shouldFollowLinksIn(url);
  }

  private String getBaseStartUrl(String startUrl) {
    return startUrl.indexOf("?") > 0 ? startUrl.substring(0, startUrl.indexOf("?")) : startUrl;
  }

  private void increaseFailCount() {
    logger.info("FAILED count is INCREASED to {}", ++failedCount);
    if (failedCount == RetryManageUtil.MAX_RETRY + 1) {
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(
              Constants.RUN_UID,
              String.format("FAILED count reached %d.", failedCount),
              null,
              Thread.currentThread().getName());
      PuppeteerDockerHelper.get().restartPuppeteerContainers(true, true);
      failedCount = 0;
    }
  }

  private void decreaseFailCount() {
    if (failedCount > 0) {
      if (--failedCount <= 0) {
        failedCount = 0;
      }
      logger.info("FAILED count is DECREASED to {}", failedCount);
    }
  }

  private void onCrawlingError(WebURL webUrl, Throwable throwable) {
    logger.error("Error when crawling " + webUrl.getURL(), throwable);
    if (PauseAllProcessesUtil.waitIfLocked() == 1) {
      failedCount = 0;
    }
    if (RetryManageUtil.canStillRetry(webUrl.getURL())) {
      logger.info("RE-SCHEDULE connecting to {}", webUrl.getURL());
      frontier.schedule(webUrl);
      RetryManageUtil.increaseRetryTimes(webUrl.getURL(), false);
      increaseFailCount();
      CommonUtil.sleep(3000, logger);
    } else {
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(
              Constants.RUN_UID,
              String.format("Retried %s time. Ignore it.", RetryManageUtil.MAX_RETRY),
              throwable,
              webUrl.getURL());
      // PuppeteerDockerHelper.get().restartPuppeteerContainers(true);
      failedCount = 0;
    }
  }
}
