package com.salefinding.crawler.crawlers;

import com.salefinding.crawler.savingData.ISaveCrawlerMethod;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategory;
import edu.uci.ics.crawler4j.crawler.CrawlController;

import java.util.Set;

public abstract class BaseCrawlerFactory implements CrawlController.WebCrawlerFactory<BaseCrawler> {

  protected ISaveCrawlerMethod savingItemMethod;

  protected ClBrandCountry brandCountry;

  protected Set<ClCategory> categories;

  public BaseCrawlerFactory(
      ISaveCrawlerMethod savingItemMethod,
      ClBrandCountry brandCountry,
      Set<ClCategory> categories) {
    this.savingItemMethod = savingItemMethod;
    this.brandCountry = brandCountry;
    this.categories = categories;
  }

  public abstract BaseCrawler newInstance() throws Exception;
}
