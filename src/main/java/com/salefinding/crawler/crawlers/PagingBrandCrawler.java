package com.salefinding.crawler.crawlers;

import com.salefinding.crawler.parser.IParser;
import com.salefinding.crawler.savingData.ISaveCrawlerMethod;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.DuplicationUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;

import java.util.Set;

public class PagingBrandCrawler extends BaseCrawler {

  protected Set<ClCategoryCrawler> categories;

  public PagingBrandCrawler(
      ISaveCrawlerMethod savingDataMethod,
      IParser parser,
      ClBrandCountry brandCountry,
      Set<ClCategoryCrawler> categories) {
    super(savingDataMethod, parser, brandCountry);
    this.categories = categories;
  }

  @Override
  public boolean shouldVisit(Page referringPage, WebURL url) {
    boolean result = super.shouldVisit(referringPage, url);
    if (result) {
      // Only allow urls referring from category urls
      String referringUrl = CommonUtil.removeUrlQueryParameter(referringPage.getWebURL().getURL());
      result = categories.stream().anyMatch(category -> category.getUrl().startsWith(referringUrl));
      if (result) {
        logger.info("ShouldVisit {}", url.getURL());
      }

      DuplicationUtil.addItem(brandCountry.getId(), brandCountry.getBrandId(), url);
    } else {
      logger.debug("NOT Visit {}", url.getURL());
    }

    return result;
  }
}
