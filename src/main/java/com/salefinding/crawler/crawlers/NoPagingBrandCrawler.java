package com.salefinding.crawler.crawlers;

import com.salefinding.crawler.parser.IParser;
import com.salefinding.crawler.savingData.ISaveCrawlerMethod;
import com.salefinding.crawler.utils.DuplicationUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;

public class NoPagingBrandCrawler extends BaseCrawler {

  public NoPagingBrandCrawler(
      ISaveCrawlerMethod savingDataMethod, IParser parser, ClBrandCountry brandCountry) {
    super(savingDataMethod, parser, brandCountry);
  }

  @Override
  public boolean shouldVisit(Page referringPage, WebURL url) {
    boolean result = super.shouldVisit(referringPage, url);
    if (result) {
      logger.info("ShouldVisit {}", url.getURL());

      DuplicationUtil.addItem(brandCountry.getId(), brandCountry.getBrandId(), url);
    }
    return result;
  }
}
