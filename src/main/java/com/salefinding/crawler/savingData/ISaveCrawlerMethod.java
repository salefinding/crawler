package com.salefinding.crawler.savingData;

import com.salefinding.crawler.models.Item;
import com.salefinding.models.crawler.ClCategoryCrawler;

import java.util.Collection;
import java.util.Set;

public interface ISaveCrawlerMethod {

  Set<ClCategoryCrawler> saveCategories(Collection<ClCategoryCrawler> clCategoryCrawlerSet);

  ClCategoryCrawler saveCategory(ClCategoryCrawler clCategoryCrawler);

  int saveItem(Item clItem);

  void finish();
}
