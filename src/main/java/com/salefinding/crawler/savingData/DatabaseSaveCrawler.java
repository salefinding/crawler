package com.salefinding.crawler.savingData;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.*;
import com.salefinding.repositories.CrawlerRepoFactory;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class DatabaseSaveCrawler implements ISaveCrawlerMethod {

  private final CrawlerRepoFactory crawlerRepoFactory;

  public DatabaseSaveCrawler() {
    crawlerRepoFactory = CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER);
  }

  public Set<ClCategoryCrawler> saveCategories(Collection<ClCategoryCrawler> clCategoryCrawlerSet) {
    return clCategoryCrawlerSet.stream().map(this::saveCategory).collect(Collectors.toSet());
  }

  public ClCategoryCrawler saveCategory(ClCategoryCrawler clCategoryCrawler) {
    try {
      ClCategoryCrawler dbCrawler =
          crawlerRepoFactory.getCategoryRepo().getCategoryCrawler(clCategoryCrawler);
      ClCategory clCategory;
      if (dbCrawler != null) {
        clCategory = dbCrawler.getCategory();
        clCategoryCrawler = dbCrawler;
      } else {
        clCategory =
            new ClCategory(
                clCategoryCrawler.getBrandId(),
                clCategoryCrawler.getSubBrand(),
                clCategoryCrawler.getCat1(),
                clCategoryCrawler.getCat2(),
                null,
                null,
                null);
      }

      clCategory.setAvailable(true);
      clCategory.setCrawled(true);
      crawlerRepoFactory.getCategoryRepo().saveCategory(clCategory);

      clCategoryCrawler.setCategoryId(clCategory.getId());
      clCategoryCrawler.setAvailable(true);
      clCategoryCrawler.setCrawled(true);
      crawlerRepoFactory.getCategoryRepo().saveCategoryCrawler(clCategoryCrawler);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      crawlerRepoFactory.getErrorSummaryRepo().saveError(Constants.RUN_UID, e, clCategoryCrawler);
    }

    return clCategoryCrawler;
  }

  @Override
  public synchronized int saveItem(Item item) {
    try {
      if (item == null) {
        return 0;
      }

      CommonUtil.calculateSaleRate(item);

      ClItem clItem = saveClItem(item);
      ClItemCountryDetail clItemDetail = saveClItemDetail(item, clItem);
      saveStock(item, clItemDetail);
      saveImage(item, clItemDetail);
      log.info(
          "Saved item - NAME {}. CODE {}. PRICE {}. SALE {}%. URL: {}",
          clItem.getName(),
          clItem.getCode(),
          clItemDetail.getPrice(),
          clItemDetail.getSaleRate(),
          clItemDetail.getUrl());

      return 1;
    } catch (Exception e) {
      log.error("Error when save item " + item.getUrl(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, item);
      return 0;
    }
  }

  private ClItem saveClItem(Item item) {
    if (item.getCategoryId() == null || item.getCategoryId() == 0) {
      throw new RuntimeException("Item's categoryId is 0.");
    }

    ClItem clItem = crawlerRepoFactory.getItemRepo().find(item.getBrandId(), item.getCode());
    if (clItem == null) {
      clItem = new ClItem(item.getBrandId(), item.getSubBrand(), item.getCode());
      clItem.setName(item.getName());
      clItem.setCategoryId(item.getCategoryId());
    }

    clItem.setCrawled(true);

    // only set max sale rate or available status if crawling item is available
    if (item.isAvailable()) {
      if (clItem.getMaxSaleRate() != null) {
        clItem.setMaxSaleRate(Math.max(clItem.getMaxSaleRate(), item.getSaleRate()));
      } else {
        clItem.setMaxSaleRate(item.getSaleRate());
      }

      if (!clItem.getAvailable()) {
        clItem.setAvailable(item.isAvailable());
      }
    }

    crawlerRepoFactory.getItemRepo().save(clItem);
    return clItem;
  }

  private ClItemCountryDetail saveClItemDetail(Item item, ClItem clItem) {
    ClItemCountryDetail detail =
        crawlerRepoFactory
            .getItemRepo()
            .getItemDetail(item.getBrandCountryId(), item.getCode(), item.getUrl());

    if (detail == null) {
      detail =
          new ClItemCountryDetail(
              clItem.getId(),
              item.getBrandCountryId(),
              item.getBrandId(),
              item.getSubBrand(),
              item.getCountryId());
    } else {
      detail.setCrawled(true);
    }

    detail.setItemId(clItem.getId());
    detail.setName(item.getName());
    detail.setCode(item.getCode());
    detail.setImage(item.getImage());
    if (item.getCategoryCrawlerId() != 0) {
      detail.setCategoryCrawlerId(item.getCategoryCrawlerId());
    }
    detail.setPrice(item.getPrice());
    detail.setOldPrice(item.getOldPrice());
    detail.setSpecialPrice(item.getSpecialPrice());
    detail.setAvailable(item.isAvailable());
    detail.setUrl(item.getUrl());
    detail.setParentUrl(item.getParentUrl());
    detail.setSaleRate(item.getSaleRate());

    crawlerRepoFactory.getItemRepo().save(detail);
    return detail;
  }

  private void saveImage(Item item, ClItemCountryDetail detail) {
    Long detailId = detail.getId();
    crawlerRepoFactory.getItemRepo().deleteAllImagesOfItem(detailId);
    item.getImageList()
        .forEach(
            image -> {
              ClItemImage clItemImage =
                  new ClItemImage(detailId, image.getThumbUrl(), image.getImgUrl(), null);
              try {

                crawlerRepoFactory.getItemRepo().save(clItemImage);
              } catch (Exception e) {
                log.error(e.getMessage(), e);
                crawlerRepoFactory
                    .getErrorSummaryRepo()
                    .saveError(
                        Constants.RUN_UID,
                        String.format("Error while saving image %s", clItemImage),
                        e);
              }
            });
  }

  private void saveStock(Item item, ClItemCountryDetail detail) {
    Long detailId = detail.getId();
    crawlerRepoFactory.getItemRepo().deleteAllStocksOfItem(detailId);
    item.getStockList()
        .forEach(
            stock -> {
              ClItemStock clItemStock =
                  new ClItemStock(
                      detailId,
                      stock.getColorCode(),
                      stock.getSizeCode(),
                      stock.getStock(),
                      stock.getOldPrice(),
                      stock.getPrice(),
                      stock.getSpecialPrice());
              try {
                crawlerRepoFactory.getItemRepo().save(clItemStock);
              } catch (Exception e) {
                log.error(e.getMessage(), e);
                crawlerRepoFactory
                    .getErrorSummaryRepo()
                    .saveError(
                        Constants.RUN_UID, String.format("Error while saving stock %s", stock), e);
              }
            });
  }

  @Override
  public void finish() {}
}
