package com.salefinding.crawler.parser.stock;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StockParser3 implements StockParser {

  private Logger logger = LoggerFactory.getLogger(StockParser3.class);

  public void crawlItemStocks(Item item, Object... params) {
    try {
      Document doc = (Document) params[0];

      Elements sizeSelects = doc.getElementsByAttributeValueContaining("class", "size-select");

      if (sizeSelects.size() > 0) {
        Elements options =
            sizeSelects.first().getElementsByAttributeValueContaining("data-status", "IN_STOCK");
        for (Element option : options) {
          int qty = Math.round(Float.parseFloat(option.attr("data-maxavailable")));
          if (qty > 0) {
            item.getStockList().add(new Item.Stock(null, option.text(), qty));
          }
        }
      }

      item.setAvailable(item.getStockList().size() > 0);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, item);
    }
  }
}
