package com.salefinding.crawler.parser.stock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.*;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;

public class StockParser1 implements StockParser {

  private Logger logger = LoggerFactory.getLogger(StockParser1.class);

  private ClBrandCountry brandCountry;

  private boolean usePuppeteer;

  public StockParser1(ClBrandCountry brandCountry) {
    this.brandCountry = brandCountry;
    this.usePuppeteer = PuppeteerUtilMap.usePuppeteer(brandCountry.getId());
  }

  public void crawlItemStocks(Item item, Object... params) {
    try {
      String uriBuilder = (String) params[0];

      JsonNode json =
          usePuppeteer ? getItemStockJsonUsePuppeteer(uriBuilder) : getItemStockJson(uriBuilder);
      if (json == null) {
        // TODO : we are cheating here
        item.setAvailable(true);
        return;
      }

      JsonNode options = json.get("qtys").get("options");
      Iterator<Map.Entry<String, JsonNode>> colorIterator = options.fields();
      while (colorIterator.hasNext()) {
        Map.Entry<String, JsonNode> color = colorIterator.next();
        String colorCode = color.getKey();
        Iterator<Map.Entry<String, JsonNode>> sizeIterator = color.getValue().fields();
        while (sizeIterator.hasNext()) {
          Map.Entry<String, JsonNode> size = sizeIterator.next();
          if ("qty".equalsIgnoreCase(size.getKey())) {
            continue;
          }
          String sizeCode = size.getKey();
          int qty = (int) Double.parseDouble(size.getValue().get("qty").asText());
          if (qty > 0) {
            item.getStockList().add(new Item.Stock(colorCode, sizeCode, qty));
          }
        }
      }

      item.setAvailable(item.getStockList().size() > 0);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, item);
    }
  }

  private JsonNode getItemStockJsonUsePuppeteer(String uri) {
    try {
      Document doc =
          Jsoup.parse(
              UrlHelper.getPuppeteerResponseString(
                  uri,
                  brandCountry.getWaitForElement(),
                  brandCountry.getRepeat(),
                  brandCountry.getId(),
                  PuppeteerAction.HTML,
                  false));

      String json = doc.getElementsByTag("body").first().text();
      return new ObjectMapper()
          .readTree(json.replaceAll("\\\\\"", "\"").replace("}\"", "}").replace("\"{", "{"));
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, uri);
    }

    return null;
  }

  private JsonNode getItemStockJson(String uri) {
    try {
      CommonUtil.sleep(brandCountry.getPolitenessDelay(), logger);
      String result =
          UrlHelper.getResponseString(uri, ProxyUtil.get().getModel(brandCountry.getId()), true, 0)
              .replaceAll("\\\\\"", "\"")
              .replace("}\"", "}")
              .replace("\"{", "{");

      return new ObjectMapper().readTree(result);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, uri);
    }

    return null;
  }
}
