package com.salefinding.crawler.parser.stock;

import com.salefinding.crawler.models.Item;

public interface StockParser {

  void crawlItemStocks(Item item, Object... params);
}
