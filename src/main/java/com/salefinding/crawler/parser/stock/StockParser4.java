package com.salefinding.crawler.parser.stock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.ProxyUtil;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StockParser4 implements StockParser {

  protected Logger logger = LoggerFactory.getLogger(this.getClass());

  protected ClBrandCountry brandCountry;

  public StockParser4(ClBrandCountry brandCountry) {
    this.brandCountry = brandCountry;
  }

  // {"id":"AC8466","availability_status":"IN_STOCK","variation_list":[{"sku":"AC8466_540","availability":1,"availability_status":"IN_STOCK","size":"4 UK"},{"sku":"AC8466_550","availability":1,"availability_status":"IN_STOCK","size":"4.5 UK"},{"sku":"AC8466_560","availability":2,"availability_status":"IN_STOCK","size":"5 UK"},{"sku":"AC8466_570","availability":0,"availability_status":"NOT_AVAILABLE","size":"5.5 UK"},{"sku":"AC8466_580","availability":3,"availability_status":"IN_STOCK","size":"6 UK"},{"sku":"AC8466_590","availability":1,"availability_status":"IN_STOCK","size":"6.5 UK"},{"sku":"AC8466_600","availability":1,"availability_status":"IN_STOCK","size":"7 UK"},{"sku":"AC8466_610","availability":1,"availability_status":"IN_STOCK","size":"7.5 UK"},{"sku":"AC8466_620","availability":1,"availability_status":"IN_STOCK","size":"8 UK"}]}
  // {"id":"CQ2909","availability_status":"NOT_AVAILABLE","variation_list":[{"sku":"CQ2909_540","availability":0,"availability_status":"NOT_AVAILABLE","size":"4 UK"},{"sku":"CQ2909_550","availability":0,"availability_status":"NOT_AVAILABLE","size":"4.5 UK"},{"sku":"CQ2909_560","availability":0,"availability_status":"NOT_AVAILABLE","size":"5 UK"},{"sku":"CQ2909_570","availability":0,"availability_status":"NOT_AVAILABLE","size":"5.5 UK"},{"sku":"CQ2909_580","availability":0,"availability_status":"NOT_AVAILABLE","size":"6 UK"},{"sku":"CQ2909_590","availability":0,"availability_status":"NOT_AVAILABLE","size":"6.5 UK"},{"sku":"CQ2909_600","availability":0,"availability_status":"NOT_AVAILABLE","size":"7 UK"},{"sku":"CQ2909_620","availability":0,"availability_status":"NOT_AVAILABLE","size":"8 UK"}]}
  public void crawlItemStocks(Item item, Object... params) {
    try {
      String uriBuilder = (String) params[0];

      JsonNode json = getItemStockJson(uriBuilder, 0);
      if (json == null) {
        // TODO : we are cheating here
        item.setAvailable(true);
        return;
      }

      if (json.has("variation_list")) {
        for (JsonNode variation : json.get("variation_list")) {
          String sizeCode = variation.get("size").asText();
          int qty = variation.get("availability").asInt();
          item.getStockList().add(new Item.Stock(null, sizeCode, qty));
        }
      }

      item.setAvailable(item.getStockList().size() > 0);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, item);
    }
  }

  private JsonNode getItemStockJson(String uri, int curRetry) {
    try {
      CommonUtil.sleep(brandCountry.getPolitenessDelay(), logger);

      String response;
      if (curRetry < 2) {
        response =
            UrlHelper.getResponseString(
                uri, ProxyUtil.get().getModel(brandCountry.getId()), false, 10000);
      } else {
        Document doc =
            Jsoup.parse(
                UrlHelper.getPuppeteerResponseString(
                    uri,
                    null,
                    brandCountry.getRepeat(),
                    brandCountry.getId(),
                    PuppeteerAction.HTML,
                    false));
        response = doc.getElementsByTag("pre").first().text();
      }

      return new ObjectMapper().readTree(response);
    } catch (Exception e) {
      if (curRetry < 4) {
        CommonUtil.sleep(3000, logger);
        getItemStockJson(uri, curRetry + 1);
      } else {
        logger.error(e.getMessage(), e);
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(Constants.RUN_UID, e, uri);
      }
    }

    return null;
  }
}
