package com.salefinding.crawler.parser.stock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StockParser2 implements StockParser {

  private Logger logger = LoggerFactory.getLogger(StockParser2.class);

  public void crawlItemStocks(Item item, Object... params) {
    try {
      Document doc = (Document) params[0];

      for (Element element : doc.getElementsByAttributeValue("type", "text/javascript")) {
        if (element.childNodes().size() > 0
            && element
                .childNodes()
                .get(0)
                .outerHtml()
                .contains("var spConfig = new Product.Config")) {
          String text = element.childNodes().get(0).outerHtml();
          String jsonString =
              text.substring(text.indexOf("{\"attributes\""), text.indexOf("Tax\"}}") + 6);
          // String jsonString = text.replace("var spConfig = new Product.Config(",
          // "").replace(");", "");
          if (parseItemStock(item, jsonString)) {
            break;
          }
        }
      }

      item.setAvailable(item.getStockList().size() > 0);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, item);
    }
  }

  private boolean parseItemStock(Item item, String jsonString) {
    try {
      JsonNode json = new ObjectMapper().readTree(jsonString);

      Map<String, Set<String>> colorCode = new HashMap<>();

      for (JsonNode attribute : json.get("attributes")) {
        String code = attribute.get("code").asText();

        for (JsonNode option : attribute.get("options")) {
          String label = option.get("label").asText();

          JsonNode node = option.get("products");
          Set<String> products = new HashSet<>();
          for (int i = 0; i < node.size(); i++) {
            products.add(node.get(i).asText());
          }

          if (code.equals("color")) {
            colorCode.put(label, products);
          } else if (code.equals("size")) {
            for (Map.Entry<String, Set<String>> entry : colorCode.entrySet()) {
              for (String str : entry.getValue()) {
                if (products.contains(str)) {
                  item.getStockList().add(new Item.Stock(entry.getKey(), label, 2));
                  break;
                }
              }
            }
          }
        }
      }
      return true;
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
      return false;
    }
  }
}
