package com.salefinding.crawler.parser;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.parser.stock.StockParser3;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import edu.uci.ics.crawler4j.crawler.Page;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AdidasParser extends BaseParser {

  private Logger logger = LoggerFactory.getLogger(AdidasParser.class);

  public AdidasParser(ClBrandCountry brandCountry, Set<ClCategoryCrawler> categories) {
    super(brandCountry, categories);
    this.stockParser = new StockParser3();
  }

  protected Item parseItem(Page page) {
    if (!page.getWebURL().getURL().endsWith(".html")) {
      logger.info("{} is NOT an item page.", page.getWebURL().getURL());
      return null;
    }

    Item item = new Item(brandCountry);
    item.setUrl(page.getWebURL().getURL());
    item.setParentUrl(page.getWebURL().getParentUrl());

    Document doc = getDocFromURL(page);

    Element productView = doc.getElementsByAttributeValueContaining("class", "listview").first();
    if (productView == null) {
      logger.info("{} SHOULD be an item page.", item.getUrl());
      return null;
    }

    logger.info("Parsing item {}.", item.getUrl());

    for (Element element : productView.getElementsByAttributeValue("itemprop", "name")) {
      if (element.tag().getName().equalsIgnoreCase("h1")) {
        item.setName(element.text());
        break;
      }
    }

    parseCategory(page, item, productView);

    item.setImage(productView.getElementById("main-image").select("img").first().attr("data-zoom"));

    // clItem code
    /*String code = item.getImage()
    .substring(item.getImage().lastIndexOf('/') + 1, item.getImage().indexOf('_'));*/
    String code =
        item.getUrl()
            .substring(item.getUrl().lastIndexOf('/') + 1, item.getUrl().lastIndexOf(".html"));
    item.setCode(code);

    Elements elements = productView.getElementsByAttributeValue("itemprop", "price");
    if (elements.size() > 0) {
      item.setPrice(getPrice(elements.first().text()));
    }

    elements = productView.getElementsByAttributeValue("class", "baseprice");
    if (elements.size() > 0) {
      item.setOldPrice(getPrice(elements.first().text()));
    }

    // get item stocks
    stockParser.crawlItemStocks(item, doc);

    // get item images
    parseImages(item, productView);

    return item;
  }

  private void parseCategory(Page page, Item item, Element productView) {
    super.parseCategory(page, item);

    if (item.getCategoryCrawlerId() == 0) {
      List<String> catStrList = new ArrayList<>();
      Elements breadcrumbs =
          productView.getElementsByAttributeValueContaining("itemtype", "Breadcrumb");
      for (Element breadcrumb : breadcrumbs) {
        String bcStr = breadcrumb.getElementsByTag("li").first().attr("data-context");
        if (!"home".equalsIgnoreCase(bcStr)) {
          catStrList.add(bcStr);
        }
      }

      if (catStrList.size() > 1) {
        logger.info(
            "Breadcrumbs of item {}: {} - {}", item.getUrl(), catStrList.get(0), catStrList.get(1));
        for (ClCategoryCrawler category : categories) {
          if (category.getCat1().equalsIgnoreCase(catStrList.get(0))
              && category.getCat2().equalsIgnoreCase(catStrList.get(1))) {
            logger.info(
                "Category of item {}: catCrawler: {} - cat: {}",
                item.getUrl(),
                category.getId(),
                category.getCategoryId());
            item.setCategoryCrawlerId(category.getId());
            item.setCategoryId(category.getCategoryId());
            break;
          }
        }
      }
    }
  }

  private void parseImages(Item item, Element productView) {
    Elements elements =
        productView.getElementsByAttributeValueContaining("class", "pdp-image-carousel-item");
    for (Element element : elements) {
      try {
        String thumbUrl = element.getElementsByTag("img").first().attr("src");
        String imgUrl = element.getElementsByTag("img").first().attr("data-zoom");

        item.getImageList().add(new Item.Image(thumbUrl, imgUrl));
      } catch (Exception ex) {
        logger.error(ex.getMessage(), ex);
      }
    }
  }
}
