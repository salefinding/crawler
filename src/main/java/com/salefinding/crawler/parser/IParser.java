package com.salefinding.crawler.parser;

import com.salefinding.crawler.models.Item;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.exceptions.ParseException;
import edu.uci.ics.crawler4j.url.WebURL;

public interface IParser {

  Item getItem(Page page) throws ParseException;

  boolean shouldFollowLinksIn(WebURL url);
}
