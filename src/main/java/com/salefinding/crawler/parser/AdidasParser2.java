package com.salefinding.crawler.parser;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.parser.stock.StockParser4;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

public class AdidasParser2 extends BaseParser {

  private Logger logger = LoggerFactory.getLogger(AdidasParser2.class);

  public AdidasParser2(ClBrandCountry brandCountry, Set<ClCategoryCrawler> categories) {
    super(brandCountry, categories);
    this.stockParser = new StockParser4(brandCountry);
  }

  protected Item parseItem(Page page) {
    if (!page.getWebURL().getURL().endsWith(".html")) {
      logger.info("{} is NOT an item page.", page.getWebURL().getURL());
      return null;
    }

    Item item = new Item(brandCountry);
    item.setUrl(page.getWebURL().getURL());
    item.setParentUrl(page.getWebURL().getParentUrl());

    Document doc = getDocFromURL(page);

    Element productView =
        doc.getElementsByAttributeValueContaining("data-auto-id", "hero-stack").first();
    if (productView == null) {
      if (isProductPage(item.getUrl())) {
        throw new RuntimeException(item.getUrl() + " SHOULD be an item page");
      }
      logger.info("{} CAN be an item page.", item.getUrl());
      // CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER).getErrorSummaryRepo().saveError(item.getUrl() + " CAN be an item page.", null, item);
      return null;
    }

    logger.info("Parsing item {}.", item.getUrl());

    parseCategory(page, item);

    item.setName(
        productView.getElementsByAttributeValue("itemprop", "name").first().attr("content"));

    item.setImage(
        productView
            .getElementsByAttributeValueContaining("class", "images_container")
            .first()
            .getElementsByTag("img")
            .first()
            .attr("src"));
    if (item.getImage().contains("?")) {
      item.setImage(item.getImage().substring(0, item.getImage().indexOf("?")));
    }

    item.setCode(
        productView
            .getElementsByAttributeValueContaining("itemprop", "sku")
            .first()
            .attr("content"));

    Element element =
        productView.getElementsByAttributeValueContaining("class", "gl-price").first();
    if (element.childNodeSize() > 1) {
      item.setPrice(getPrice(element.child(0).text()));
      item.setOldPrice(getPrice(element.child(1).text()));
    } else {
      item.setPrice(getPrice(element.child(0).text()));
    }

    // get item stocks
    String stockUri =
        String.format("%s/%s/availability", brandCountry.getCheckStockApiUri(), item.getCode());
    stockParser.crawlItemStocks(item, stockUri);

    // get item images
    parseImages(item, productView);

    return item;
  }

  private void parseCategory(Page page, Item item, Document doc) {
    try {
      final String[] url = {null};
      Elements breadcrumbs = doc.getElementsByAttributeValueContaining("class", "breadcrumb_item");
      breadcrumbs.stream()
          .filter(breadcrumb -> breadcrumb.attr("property").contains("itemListElement"))
          .forEach(breadcrumb -> url[0] = breadcrumb.getElementsByTag("a").first().attr("href"));

      if (url[0] != null) {
        Optional<ClCategoryCrawler> category =
            categories.stream().filter(cat -> cat.getUrl().contains(url[0])).findFirst();
        if (category.isPresent()) {
          item.setCategoryCrawlerId(category.get().getId());
          item.setCategoryId(category.get().getCategoryId());
          return;
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, "Error when parsing by AdidasParser2", e, item);
    }

    super.parseCategory(page, item);
  }

  private void parseImages(Item item, Element productView) {
    Element thumbContainer =
        productView.getElementsByAttributeValueContaining("class", "thumbnails_container").first();
    Elements elements = thumbContainer.getElementsByAttributeValueContaining("class", "thumbnail");
    for (Element element : elements) {
      try {
        Element thumbElement = element.getElementsByTag("img").first();
        if (thumbElement != null) {
          String thumbUrl = thumbElement.attr("src");
          String imgUrl;
          if (thumbUrl.contains("?")) {
            imgUrl = thumbUrl.substring(0, thumbUrl.indexOf("?"));
          } else {
            imgUrl = thumbUrl;
          }

          if (!imgUrl.contains("video")) {
            item.getImageList().add(new Item.Image(thumbUrl, imgUrl));
          }
        }
      } catch (Exception ex) {
        logger.error(ex.getMessage(), ex);
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(Constants.RUN_UID, ex, item);
      }
    }
  }

  private boolean isProductPage(String url) {
    String code = url.substring(url.lastIndexOf('/') + 1, url.indexOf(".html"));
    return Pattern.compile("([0-9]|[A-Z])+").matcher(code).matches();
  }
}
