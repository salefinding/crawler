package com.salefinding.crawler.parser;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.parser.stock.StockParser2;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class CharlesKeithParser extends BaseParser {

  private Logger logger = LoggerFactory.getLogger(CharlesKeithParser.class);

  public CharlesKeithParser(ClBrandCountry brandCountry, Set<ClCategoryCrawler> categories) {
    super(brandCountry, categories);
    stockParser = new StockParser2();
  }

  protected Item parseItem(Page page) {
    Item item = new Item(brandCountry);
    item.setUrl(page.getWebURL().getURL());
    item.setParentUrl(page.getWebURL().getParentUrl());

    Document doc = getDocFromURL(page);

    Element productView = doc.getElementsByAttributeValue("class", "product-essential").first();
    if (productView == null) {
      if (item.getUrl().endsWith(".html")) {
        throw new RuntimeException(item.getUrl() + " SHOULD be an item page");
      }
      logger.info("{} is NOT an item page.", item.getUrl());
      return null;
    }

    logger.info("Parsing item {}.", item.getUrl());

    item.setName(
        productView.getElementsByAttributeValue("class", "product-name").first().child(0).text());

    item.setCode(productView.getElementsByAttributeValue("class", "sku").first().text());

    parseCategory(page, item);

    item.setImage(
        productView
            .getElementsByAttributeValueContaining("class", "product-image-thumbs")
            .first()
            .getElementsByAttributeValueContaining("class", "thumb-link")
            .first()
            .getElementsByTag("img")
            .first()
            .attr("src"));

    Elements elements = productView.getElementsByAttributeValue("class", "regular-price");
    if (elements.size() == 0) {
      elements = productView.getElementsByAttributeValue("class", "special-price");
    }
    if (elements.size() > 0) {
      Element price = elements.first().getElementsByAttributeValue("class", "price").first();
      item.setPrice(getPrice(price.text()));
    }

    elements = productView.getElementsByAttributeValue("class", "old-price");
    if (elements.size() > 0) {
      Element element = elements.first().getElementsByAttributeValue("class", "price").first();
      item.setOldPrice(getPrice(element.text()));
    }

    stockParser.crawlItemStocks(item, doc);

    parseImages(item, productView);

    return item;
  }

  private void parseImages(Item item, Element productView) {
    Element prodThumbImgs =
        productView.getElementsByAttributeValueContaining("class", "product-image-thumbs").first();
    Elements elements = prodThumbImgs.getElementsByAttributeValueContaining("class", "thumb-link");
    for (Element element : elements) {
      try {
        String thumbUrl = element.getElementsByTag("img").first().attr("src");
        String imgUrl = element.attr("href");

        item.getImageList().add(new Item.Image(thumbUrl, imgUrl));
      } catch (Exception ex) {
        logger.error(ex.getMessage(), ex);
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(Constants.RUN_UID, ex, item);
      }
    }
  }
}
