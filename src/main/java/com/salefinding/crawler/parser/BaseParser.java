package com.salefinding.crawler.parser;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.parser.stock.StockParser;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.exceptions.ParseException;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

public abstract class BaseParser implements IParser {

  private Logger logger = LoggerFactory.getLogger(BaseParser.class);

  protected ClBrandCountry brandCountry;

  protected Set<ClCategoryCrawler> categories;

  protected StockParser stockParser;

  protected BaseParser(ClBrandCountry brandCountry, Set<ClCategoryCrawler> categories) {
    this.brandCountry = brandCountry;
    this.categories = categories;
  }

  protected void parseCategory(Page page, Item item) {
    if (page.getWebURL() != null) {
      for (ClCategoryCrawler category : categories) {
        String cat =
            category.getUrl().replaceAll("(\\?)(.*)", "").replaceAll("(http)(s*)(://)", "");
        if ((page.getWebURL().getParentUrl() != null
                && page.getWebURL().getParentUrl().contains(cat))
            || (page.getWebURL().getURL() != null && page.getWebURL().getURL().contains(cat))) {

          item.setCategoryId(category.getCategoryId());
          item.setCategoryCrawlerId(category.getId());
          return;
        }
      }
      logger.error(
          "Cannot find category for item.\nItem url: {}\nParent item url: {}",
          page.getWebURL().getURL(),
          page.getWebURL().getParentUrl());
    } else {
      logger.error("Cannot find category because WEB-URL is NULL.");
    }

    ClCategoryCrawler category = categories.iterator().next();
    item.setCategoryId(category.getCategoryId());
    item.setCategoryCrawlerId(category.getId());
  }

  protected BigDecimal getPrice(String priceString) {
    String cPrice = CommonUtil.removeNonNumericChar(priceString);
    if (StringUtils.isNotBlank(cPrice)) {
      return new BigDecimal(cPrice);
    }
    return null;
  }

  protected Document getDocFromURL(Page page) {
    return Jsoup.parse(((HtmlParseData) page.getParseData()).getHtml());
  }

  public Item getItem(Page page) throws ParseException {
    Item item;
    try {
      if (!(page.getParseData() instanceof HtmlParseData)) {
        throw new RuntimeException(
            String.format("%s content is NOT a html", page.getWebURL().getURL()));
      }
      item = parseItem(page);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new ParseException(e);
    }
    if (item != null) {
      CommonUtil.calculateSaleRate(item);
    }

    return item;
  }

  protected abstract Item parseItem(Page page);

  @Override
  public boolean shouldFollowLinksIn(WebURL url) {
    String urlStr = getUrlWithoutParameters(url.getURL());
    String parentUrlStr =
        getUrlWithoutParameters(url.getParentUrl() != null ? url.getParentUrl() : "null");

    boolean isBelongedToACategory =
        categories.stream()
            .anyMatch(
                category ->
                    category.getUrl().contains(urlStr) || category.getUrl().contains(parentUrlStr));
    if (!isBelongedToACategory) {
      logger.error("{} does NOT belong to any category link.", url);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getCrawlingDataRepo()
          .saveOrUpdate(brandCountry.getId(), Constants.RUN_UID, url.getURL(), false, false, false);
      return false;
    }

    return true;
  }

  private String getUrlWithoutParameters(String url) {
    try {
      URI uri = new URI(url);
      String temp =
          new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), null, uri.getFragment())
              .toString();
      if (temp.endsWith("/")) {
        temp = temp.substring(0, temp.length() - 1);
      }
      return temp;
    } catch (URISyntaxException e) {
      return url;
    }
  }
}
