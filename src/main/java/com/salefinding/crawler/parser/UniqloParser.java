package com.salefinding.crawler.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.parser.stock.StockParser1;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Set;

public class UniqloParser extends BaseParser {

  private Logger logger = LoggerFactory.getLogger(UniqloParser.class);

  public UniqloParser(ClBrandCountry brandCountry, Set<ClCategoryCrawler> categories) {
    super(brandCountry, categories);
    this.stockParser = new StockParser1(brandCountry);
  }

  protected Item parseItem(Page page) {
    Item item = new Item(brandCountry);
    item.setUrl(page.getWebURL().getURL());
    item.setParentUrl(page.getWebURL().getParentUrl());

    Document doc = getDocFromURL(page);

    Element mainImageSection = doc.getElementsByAttributeValue("class", "product-main-image-section").first();
    if (mainImageSection == null) {
      logger.info("{} is NOT an item page.", item.getUrl());
      return null;
    }

    logger.info("Parsing item {}.", item.getUrl());

    int count = 0;
    while (doc.getElementById("goodsNmArea") == null && count++ < 3) {
      doc = getDocFromURL(page);
    }
    Element name = doc.getElementById("goodsNmArea");
    item.setName(name.children().get(0).text());

    for (Element element : doc.getElementsByAttributeValue("class", "number")) {
      String str = element.text();
      if (str.contains("ITEM CODE:")) {
        item.setCode(str.replace("ITEM CODE:", "").trim());
      }
    }

    parseCategory(doc, page, item);

    Element image = doc.getElementById("prodImgZOOM");
    item.setImage(image.select("img").first().attr("src"));

    Element price = doc.getElementById("product-price-7");
    item.setPrice(getPrice(price.text()));

    Element oldPriceElement = doc.getElementById("old-price-7");
    BigDecimal oldPrice = getPrice(oldPriceElement.text());
    if (oldPrice != null) {
      item.setOldPrice(oldPrice);
    }

    // get item stocks
    String productId = doc.getElementById("product_id").attr("value");
    String stockUri =
        brandCountry.getCheckStockApiUri()
            + "?store=2"
            + "&pid="
            + productId
            + "&model="
            + item.getCode();
    stockParser.crawlItemStocks(item, stockUri);

    // get item images
    parseImages(item, doc);
    if (StringUtils.isBlank(item.getImage()) && item.getImageList().size() > 0) {
      item.setImage(item.getImageList().iterator().next().getImgUrl());
    }

    return item;
  }

  protected void parseCategory(Document doc, Page page, Item item) {
    try {
      Element categoryElement = doc.getElementsByAttributeValue("class", "category").first();

      if (categoryElement.getElementsByTag("a").size() > 0) {
        String url = categoryElement.getElementsByTag("a").first().attr("href");

        if (StringUtils.isNotBlank(url)) {
          // Get a real URL of category url
          CommonUtil.sleep(brandCountry.getPolitenessDelay(), logger);

          url =
              UrlHelper.getRealUrl(
                  url,
                  brandCountry.getWaitForElement(),
                  brandCountry.getRepeat(),
                  brandCountry.getId(),
                  true);

          for (ClCategoryCrawler category : categories) {
            if (url.equalsIgnoreCase(category.getUrl())
                || url.contains(category.getUrl().replace(".html", ""))) {
              item.setCategoryId(category.getCategoryId());
              item.setCategoryCrawlerId(category.getId());
              return;
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e, item);
    }

    super.parseCategory(page, item);
  }

  private void parseImages(Item item, Document doc) {
    for (Element element : doc.getElementsByAttributeValue("type", "text/javascript")) {
      if (element.childNodes().size() > 0
          && element.childNodes().get(0).outerHtml().contains("aImages = {")) {
        String text = element.childNodes().get(0).outerHtml();
        String jsonString =
            text.substring(
                text.indexOf("aImages = {") + 10,
                text.indexOf("}}", text.indexOf("aImages = {" + 10)) + 2);

        try {
          JsonNode json = new ObjectMapper().readTree(jsonString);
          for (JsonNode imageNode : json) {
            String thumbUrl = null, imgUrl;
            if (imageNode.has("thumbnail")) {
              thumbUrl = imageNode.get("thumbnail").asText();
            }
            if (imageNode.has("zoom")) {
              imgUrl = imageNode.get("zoom").asText();
            } else {
              imgUrl = imageNode.get("large").asText();
            }

            item.getImageList().add(new Item.Image(thumbUrl, imgUrl));
          }
        } catch (IOException e) {
          logger.error(jsonString);
          logger.error(e.getMessage(), e);
        }
      }
    }
  }
}
