package com.salefinding.crawler.parser;

import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.crawler.Page;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Set;

public class CharlesKeithParser2 extends BaseParser {

  private Logger logger = LoggerFactory.getLogger(CharlesKeithParser2.class);

  public CharlesKeithParser2(ClBrandCountry brandCountry, Set<ClCategoryCrawler> categories) {
    super(brandCountry, categories);
  }

  protected Item parseItem(Page page) {
    Item item = new Item(brandCountry);
    item.setUrl(page.getWebURL().getURL());
    item.setParentUrl(page.getWebURL().getParentUrl());

    Document doc = getDocFromURL(page);

    Element productView = doc.getElementsByAttributeValue("class", "row pdp").first();
    if (productView == null) {
      try {
        URI uri = URI.create(item.getUrl());
        int secondSlash = uri.getPath().indexOf('/', uri.getPath().indexOf('/') + 1);
        int thirdSlash = uri.getPath().indexOf('/', secondSlash + 1);
        String url = uri.getPath().substring(secondSlash + 1, thirdSlash);
        if (uri.getPath().endsWith(".html") && url.charAt(3) == '-') {
          throw new RuntimeException(item.getUrl() + " SHOULD be an item page");
        }
        logger.info("{} is NOT an item page.", item.getUrl());
        return null;
      } catch (Exception e) {
        logger.info("{} is NOT an item page.", item.getUrl());
        return null;
      }
    }

    logger.info("Parsing item {}.", item.getUrl());

    item.setName(
        productView.getElementsByAttributeValueContaining("class", "product-name").first().text());

    item.setCode(
        productView.getElementsByAttributeValueContaining("class", "product-id").first().text());

    parseCategory(page, item);

    item.setImage(
        UriBuilder.fromUri(
                productView
                    .getElementsByAttributeValueContaining("class", "carousel-item")
                    .first()
                    .getElementsByTag("img")
                    .first()
                    .attr("src"))
            .replaceQuery(null)
            .build()
            .toString());

    Element priceElement =
        productView.getElementsByAttributeValueContaining("class", "pdp-prices").first();
    if (priceElement != null) {
      Elements oldPriceElements =
          priceElement.getElementsByAttributeValueContaining("class", "strike-through");
      if (oldPriceElements.size() > 0) {
        item.setOldPrice(getPrice(oldPriceElements.first().child(0).text()));
      }

      Elements salePriceElements =
          priceElement.getElementsByAttributeValueContaining("class", "sale");
      if (salePriceElements.size() > 0) {
        item.setPrice(getPrice(salePriceElements.first().text()));
      }
    }

    parseStock(item, productView);
    parseImages(item, productView);

    return item;
  }

  private void parseImages(Item item, Element productView) {
    Elements elements = productView.getElementsByAttributeValueContaining("class", "carousel-item");
    for (Element element : elements) {
      try {
        Element img = element.getElementsByTag("img").first();
        String thumbUrl = img.attr("src");
        String imgUrl = thumbUrl.substring(0, thumbUrl.indexOf('?'));

        item.getImageList().add(new Item.Image(thumbUrl, imgUrl));
      } catch (Exception ex) {
        logger.error(ex.getMessage(), ex);
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(Constants.RUN_UID, ex, item);
      }
    }
  }

  private void parseStock(Item item, Element productView) {
    // get current color
    String curColorCode = null;
    Elements colorElements =
        productView
            .getElementsByAttributeValueContaining("class", "pdp-attr--color")
            .first()
            .getElementsByAttributeValueContaining("class", "color-value");

    for (Element colorElement : colorElements) {
      if (colorElement.attr("class").contains("selected")) {
        curColorCode = colorElement.attr("data-attr-value");
        break;
      }
    }

    Elements sizeElements =
        productView
            .getElementsByAttributeValueContaining("class", "pdp-attr--size")
            .first()
            .getElementsByAttributeValueContaining("class", "size-value");
    for (Element sizeElement : sizeElements) {
      if (sizeElement.attr("class").contains("selectable")) {
        item.getStockList().add(new Item.Stock(curColorCode, sizeElement.text(), 1));
      }
    }

    item.setAvailable(item.getStockList().size() > 0);
  }
}
