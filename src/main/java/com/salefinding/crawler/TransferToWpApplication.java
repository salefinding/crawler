package com.salefinding.crawler;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.transfer.wp.TransferToWP;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.PropUtil;
import com.salefinding.crawler.utils.ProxyUtil;
import com.salefinding.crawler.utils.StaticValues;
import com.salefinding.models.crawler.gen.ClBrandCountryGen;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class TransferToWpApplication {

  private static Logger logger = LoggerFactory.getLogger(TransferToWpApplication.class);

  public static void main(String[] args) {
    try {
      logger.info("START TRANSFER TO WP.");

      /*PuppeteerDockerHelper.get().startPuppeteerContainers(StaticValues.dockerInstanceNo);
      ProxyUtil.get().loadModelList();*/

      List<Long> brandIdList = new ArrayList<>();

      String brandIds = PropUtil.getString(Constants.TRANSFER_BRAND_ID, "");
      if (StringUtils.isNotBlank(brandIds)) {
        for (String b : brandIds.split(",")) {
          brandIdList.add(Long.parseLong(b));
        }
      } else {
        brandIdList.addAll(
            CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                .getBrandCountryRepo()
                .getAllEnabled()
                .stream()
                .map(ClBrandCountryGen::getBrandId)
                .distinct()
                .collect(Collectors.toList()));
      }

      ExecutorService executor = Executors.newFixedThreadPool(StaticValues.transferThreadNo);
      List<Callable<Object>> callableList =
          brandIdList.stream()
              .map(
                  brandId ->
                      Executors.callable(
                          () -> {
                            logger.info("Transferring BrandId {}.", brandId);
                            TransferToWP.getInstance(brandId).transfer(3);
                          }))
              .collect(Collectors.toList());
      executor.invokeAll(callableList);

      logger.info("END TRANSFER TO WP.");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e);
    } finally {
      PuppeteerDockerHelper.get().removeAllPuppeteerContainers();
      logger.info("END program!");
    }

    System.exit(0);
  }
}
