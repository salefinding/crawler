package com.salefinding.crawler.utils;

import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.models.PuppeteerModel;

public class PuppeteerUtil extends NetworkModelUtil<PuppeteerModel> {

  private static PuppeteerUtil instance = new PuppeteerUtil();

  public static PuppeteerUtil get() {
    if (instance == null) {
      synchronized (PuppeteerUtil.class) {
        if (instance == null) {
          instance = new PuppeteerUtil();
        }
      }
    }
    return instance;
  }

  protected PuppeteerUtil() {
    this(PropUtil.getString(Constants.GCLOUD_GROUP_NAME));
  }

  protected PuppeteerUtil(String groupName) {
    super(PuppeteerModel.class, groupName);
    // scheduleReloadModels();
  }

  protected PuppeteerUtil(Class clazz, String groupName) {
    super(clazz, groupName);
  }

  @Override
  protected PuppeteerModel createModel(String ip) {
    return new PuppeteerModel(ip, ip, 8080);
  }

  public boolean isValid(PuppeteerModel model) {
    return UrlHelper.isConnectible(
        model.getPuppeteerUrl(null, null, null, PuppeteerAction.HEALTHCHECK), null, 10000);
  }
}
