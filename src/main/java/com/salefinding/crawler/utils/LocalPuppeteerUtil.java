package com.salefinding.crawler.utils;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.models.LocalPuppeteerModel;

public class LocalPuppeteerUtil extends PuppeteerUtil {

  private static LocalPuppeteerUtil instance = new LocalPuppeteerUtil();

  public static LocalPuppeteerUtil get() {
    if (instance == null) {
      synchronized (LocalPuppeteerUtil.class) {
        if (instance == null) {
          instance = new LocalPuppeteerUtil();
        }
      }
    }
    return instance;
  }

  protected LocalPuppeteerUtil() {
    super(LocalPuppeteerModel.class, null);
  }

  @Override
  protected void addDefaultModelList() {
    int proxyIndex = 0;
    for (int port : PuppeteerDockerHelper.get().getPupIdMap().keySet()) {
      LocalPuppeteerModel model =
          new LocalPuppeteerModel(
              Constants.PUP_PREFIX + (port - Constants.STARTING_PORT),
              Constants.localPuppeteerIp,
              port);
      model.setIndex(proxyIndex++);

      validateAndAddModel(model);
    }
  }
}
