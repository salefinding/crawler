package com.salefinding.crawler.utils;

import com.salefinding.crawler.models.ProxyModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;

public class ProxyUtil extends NetworkModelUtil<ProxyModel> {

  private static final String PROXY_GROUP_NAME = "proxy-group";

  private static ProxyUtil instance = new ProxyUtil();

  private int rotatedIndex = 0;

  public static ProxyUtil get() {
    if (instance == null) {
      synchronized (ProxyUtil.class) {
        if (instance == null) {
          instance = new ProxyUtil();
        }
      }
    }
    return instance;
  }

  private ProxyUtil() {
    super(ProxyModel.class, PROXY_GROUP_NAME);
    // scheduleReloadModels();
  }

  @Override
  protected void addDefaultModelList() {
    String proxyProperty = PropUtil.getString(Constants.PROXY_KEY);
    if (StringUtils.isNotBlank(proxyProperty)) {
      String[] array = proxyProperty.split(",");
      for (String str : array) {
        str = StringUtils.trim(str);
        if ("local".equalsIgnoreCase(str)) {
          addModel(new ProxyModel());
        } else {
          ProxyModel model = new ProxyModel();

          String[] proxyParts = str.split(":");
          if (proxyParts.length > 0) {
            model.setHost(proxyParts[0]);
            model.setPort(3128);
          }
          if (proxyParts.length > 1) {
            model.setPort(Integer.parseInt(proxyParts[1]));
          }
          if (proxyParts.length > 2) {
            model.setUsername(proxyParts[2]);
          }
          if (proxyParts.length > 3) {
            model.setPassword(proxyParts[3]);
          }
          if (proxyParts.length > 4) {
            model.setProtocol(proxyParts[4]);
          }
          model.setPingTime((int) CommonUtil.getPingTime(model.getHost(), 5, 0));

          validateAndAddModel(model);
        }
      }
      modelList.sort(Comparator.comparingDouble(ProxyModel::getPingTime));
    }
  }

  @Override
  protected ProxyModel createModel(String ip) {
    return new ProxyModel(ip, "http", ip, 3128);
  }

  @Override
  public boolean isValid(ProxyModel model) {
    return model.getPingTime() > 0
        && UrlHelper.isConnectible("https://www.google.com", model, 3000);
  }

  public ProxyModel getByIndex(int index) {
    if (modelList.size() == 0) {
      return null;
    }
    return modelList.get(index % modelList.size());
  }

  public synchronized ProxyModel getRotatedModel() {
    if (modelList.size() == 0) {
      return null;
    }
    return modelList.get(rotatedIndex++ % modelList.size());
  }

  public int getNumberOfProxyList() {
    return modelList.size();
  }
}
