package com.salefinding.crawler.utils;

public class StaticValues {

  public static int dockerInstanceNo =
      PropUtil.getInt(Constants.DOCKER_PUPPETEER_CONTAINER_SIZE, 20);

  public static int controllerNo = PropUtil.getInt(Constants.NUMBER_OF_CONTROLLERS_KEY, 2);

  public static int transferThreadNo = PropUtil.getInt(Constants.NUMBER_OF_TRANSFER_THREAD_KEY, 2);
}
