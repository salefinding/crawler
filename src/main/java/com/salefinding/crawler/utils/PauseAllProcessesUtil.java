package com.salefinding.crawler.utils;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PauseAllProcessesUtil {

  private static final Logger logger = LoggerFactory.getLogger(PauseAllProcessesUtil.class);

  private static int lock = 0;

  public static synchronized void lock() {
    logger.info("LOCKING PARSING PROCESSES");
    lock = 1;
  }

  public static synchronized void unLock() {
    logger.info("UNLOCKING PARSING PROCESSES");
    lock = 0;
  }

  public static int waitIfLocked() {
    int result = 0;
    while (lock > 0) {
      result = PuppeteerDockerHelper.get().addThreadToWaitingList(Thread.currentThread().getName());
      logger.info("Parsing processes are LOCKED. Wait 10s.");
      CommonUtil.sleep(10000);
    }
    return result;
  }
}
