package com.salefinding.crawler.utils;

import com.salefinding.models.crawler.ClCrawlingData;
import com.salefinding.repositories.CrawlerRepoFactory;
import edu.uci.ics.crawler4j.url.WebURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class DuplicationUtil {

  private static Logger logger = LoggerFactory.getLogger(DuplicationUtil.class);

  private static Map<Long, Set<String>> brandCountryItemMap = new ConcurrentHashMap<>();

  public static synchronized void addItem(Long brandCountryId, Long brandId, WebURL url) {
    String itemFullCode = getItemFullCode(brandId, url);
    if (itemFullCode != null) {
      if (!brandCountryItemMap.containsKey(brandCountryId)) {
        Set<String> itemSet = new HashSet<>();
        itemSet.add(itemFullCode);

        brandCountryItemMap.put(brandCountryId, itemSet);
      } else {
        brandCountryItemMap.get(brandCountryId).add(itemFullCode);
      }

      try {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getCrawlingDataRepo()
            .save(
                new ClCrawlingData(
                    brandCountryId, Constants.RUN_UID, url.getURL(), true, false, false));
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
      }

      logger.info("Add {} to brand-country-id {}.", itemFullCode, brandCountryId);
    }
  }

  public static boolean isExistedItem(Long brandCountryId, Long brandId, WebURL webURL) {
    String itemFullCode = getItemFullCode(brandId, webURL);
    if (itemFullCode == null) {
      return false;
    }
    boolean result =
        brandCountryItemMap.containsKey(brandCountryId)
            && brandCountryItemMap.get(brandCountryId).contains(getItemFullCode(brandId, webURL));
    if (result) {
      logger.info("{} is EXISTED.", itemFullCode);
    }

    return result;
  }

  public static synchronized void addItem(Long brandCountryId, String key) {
    if (key != null) {
      if (!brandCountryItemMap.containsKey(brandCountryId)) {
        Set<String> itemSet = new HashSet<>();
        itemSet.add(key);

        brandCountryItemMap.put(brandCountryId, itemSet);
      } else {
        brandCountryItemMap.get(brandCountryId).add(key);
      }

      logger.info("Add {} to brand-country-id {}.", key, brandCountryId);
    }
  }

  public static boolean isExistedItem(Long brandCountryId, String key) {
    boolean result =
        brandCountryItemMap.containsKey(brandCountryId)
            && brandCountryItemMap.get(brandCountryId).contains(key);
    if (result) {
      logger.info("{} is EXISTED.", key);
    }

    return result;
  }

  private static String getItemFullCode(Long brandId, WebURL webURL) {
    String url = webURL.getURL().replaceAll("/+$", "");
    if (!url.contains(".html")) {
      return null;
    }

    if (brandId == 1 || brandId == 2 || brandId == 4) {
      return url;
    } else {
      return url.substring(url.lastIndexOf('/') + 1, url.indexOf(".html"));
    }
  }
}
