package com.salefinding.crawler.utils;

import com.salefinding.models.crawler.ClBrandCountry;

import java.util.HashMap;
import java.util.Map;

public class PuppeteerUtilMap {

  private static Map<Long, PuppeteerUtil> map = new HashMap<>();

  public static PuppeteerUtil getUtil(Long brandCountryId) {
    return map.get(brandCountryId);
  }

  public static boolean usePuppeteer(Long brandCountryId) {
    return map.containsKey(brandCountryId);
  }

  public static void addToMap(ClBrandCountry brandCountry) {
    PuppeteerUtil util;
    if (brandCountry.getFetcherClass().toLowerCase().contains("local")) {
      util = LocalPuppeteerUtil.get();
    } else {
      util = PuppeteerUtil.get();
    }
    map.put(brandCountry.getId(), util);
  }
}
