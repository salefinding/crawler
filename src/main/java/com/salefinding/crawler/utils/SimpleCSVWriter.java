package com.salefinding.crawler.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SimpleCSVWriter {

  private static String produceCsvData(Object[] data)
      throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    if (data.length == 0) {
      return "";
    }

    Class classType = data[0].getClass();
    StringBuilder builder = new StringBuilder();

    Method[] methods = classType.getDeclaredMethods();

    for (Method m : methods) {
      if (m.getParameterTypes().length == 0) {
        if (m.getName().startsWith("get")) {
          builder.append(m.getName().substring(3)).append(',');
        } else if (m.getName().startsWith("is")) {
          builder.append(m.getName().substring(2)).append(',');
        }
      }
    }

    builder.deleteCharAt(builder.length() - 1);
    builder.append('\n');
    for (Object d : data) {
      for (Method m : methods) {
        if (m.getParameterTypes().length == 0) {
          if ((m.getName().startsWith("get") || m.getName().startsWith("is"))
              && m.invoke(d) != null) {
            System.out.println(m.invoke(d).toString());
            builder.append(m.invoke(d).toString()).append(',');
          }
        }
      }
      builder.append('\n');
    }
    builder.deleteCharAt(builder.length() - 1);
    return builder.toString();
  }

  public static boolean generateCSV(File csvFileName, Object[] data) {
    BufferedWriter fw = null;
    try {
      fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFileName), "UTF-8"));
      if (!csvFileName.exists()) {
        if (!csvFileName.createNewFile()) {
          throw new Exception();
        }
      }
      fw.write(produceCsvData(data));
      fw.flush();
    } catch (Exception e) {
      System.out.println("Error while generating csv from data. Error message : " + e.getMessage());
      e.printStackTrace();
      return false;
    } finally {
      if (fw != null) {
        try {
          fw.close();
        } catch (Exception e) {
          // ignore
        }
      }
    }
    return true;
  }
}
