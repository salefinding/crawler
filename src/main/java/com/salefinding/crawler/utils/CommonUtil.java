package com.salefinding.crawler.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.models.Item;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.models.PuppeteerModel;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.gen.ClKeyValueGen;
import com.salefinding.repositories.CrawlerRepoFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Slf4j
public class CommonUtil {

  private static Map<String, String> dictionary = new HashMap<>();

  public static void initDictionary() {
    dictionary =
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getCommonRepository()
            .getAllKeyValues()
            .stream()
            .collect(
                Collectors.toMap(
                    clKeyValue -> clKeyValue.getKey().toUpperCase(), ClKeyValueGen::getValue));
  }

  public static void sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public static void sleep(long millis, Logger log) {
    try {
      log.info("SLEEP {} ms", millis);
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public static String removeNonNumericChar(String str) {
    return str.replaceAll("[^\\d.]", "");
  }

  public static String removeUrlQueryParameter(String url) {
    if (url.indexOf('?') > 0) {
      return url.substring(0, url.indexOf('?'));
    }
    return url;
  }

  public static double getPingTime(String host, int count, int retry) {
    // The command to execute
    String pingCmd = "ping -c " + count + " -w 5 " + host;
    log.info("Pinging to {} ...", host);

    // get the runtime to execute the command
    Runtime runtime = Runtime.getRuntime();
    try {
      Process process = runtime.exec(pingCmd);

      // Gets the input stream to read the output of the command
      BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));

      // reads the outputs
      String inputLine = in.readLine();
      double totalTime = 0;
      int k = 0;
      while ((inputLine != null)) {
        if (k < count && inputLine.length() > 0 && inputLine.contains("time")) {
          String time = inputLine.substring(inputLine.indexOf("time") + 5);
          time = time.replace("ms", "").trim();
          totalTime += Double.parseDouble(time);
          k++;
        }
        inputLine = in.readLine();
      }

      double pingTime = totalTime / k;
      log.info(String.format("Ping time to %s is %.2f", host, pingTime));

      if (retry < 3 && (pingTime <= 0 || pingTime > 100)) {
        log.info(
            "Restart getPingTime as pingTime {} is INVALID. Retry times = {}", pingTime, retry + 1);
        return getPingTime(host, count, retry + 1);
      }
      return pingTime;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      if (retry < 3) {
        log.info("Restart getPingTime as exception. Retry times = {}", retry + 1);
        return getPingTime(host, count, retry + 1);
      }
      return -1;
    }
  }

  public static void registerBatch(ClBrandCountry brandCountry) {
    try {
      NetworkModelUtil util;
      if (brandCountry.getFetcherClass().toLowerCase().contains("localpuppeteer")) {
        util = LocalPuppeteerUtil.get();
      } else if (brandCountry.getFetcherClass().toLowerCase().contains("puppeteer")) {
        util = PuppeteerUtil.get();
      } else {
        util = LocalPuppeteerUtil.get();
      }

      long batch;
      do {
        batch =
            ProxyUtil.get().registerBatch(brandCountry.getId(), brandCountry.getNumberOfThread());
        if (batch == -1) {
          PuppeteerDockerHelper.get().restartPuppeteerContainers(false, false);
          CommonUtil.sleep(30000, log);
          continue;
        }

        batch = util.registerBatch(brandCountry.getId(), brandCountry.getNumberOfThread());
        if (batch == -1) {
          PuppeteerDockerHelper.get().restartPuppeteerContainers(false, false);
          CommonUtil.sleep(30000, log);
        }
      } while (batch == -1);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

  public static void unregisterBatch(ClBrandCountry brandCountry) {
    log.info("Remove batch for {}", brandCountry.getId());
    if (brandCountry.getFetcherClass().toLowerCase().contains("localpuppeteer")) {
      LocalPuppeteerUtil.get().unregisterBatch(brandCountry.getId());
    } else if (brandCountry.getFetcherClass().toLowerCase().contains("puppeteer")) {
      PuppeteerUtil.get().unregisterBatch(brandCountry.getId());
    } else {
      LocalPuppeteerUtil.get().unregisterBatch(brandCountry.getId());
    }
    ProxyUtil.get().unregisterBatch(brandCountry.getId());
  }

  public static BigDecimal getPrice(String priceString) {
    String cPrice = CommonUtil.removeNonNumericChar(priceString);
    if (StringUtils.isNotBlank(cPrice)) {
      return new BigDecimal(cPrice);
    }
    return null;
  }

  public static void calculateSaleRate(Item item) {
    if (item.getPrice() == null) {
      item.setSaleRate(0);
      return;
    }

    int result = 0;

    if (item.getOldPrice() != null) {
      result =
          100 - Math.round(100 * item.getPrice().floatValue() / item.getOldPrice().floatValue());
    }

    if (item.getSpecialPrice() != null && item.getOldPrice() != null) {
      result =
          100
              - Math.round(
                  100 * item.getSpecialPrice().floatValue() / item.getOldPrice().floatValue());
    }

    item.setSaleRate(result);
  }

  public static void awaitTerminationAfterShutdown(ExecutorService threadPool) {
    threadPool.shutdown();
    try {
      if (!threadPool.awaitTermination(10, TimeUnit.HOURS)) {
        threadPool.shutdownNow();
      }
    } catch (InterruptedException ex) {
      threadPool.shutdownNow();
      Thread.currentThread().interrupt();
    }
  }

  public static String getResponse(
      ClBrandCountry brandCountry,
      String uri,
      int curRetry,
      String mustHave,
      boolean usePuppeteer) {
    String response = "";
    try {
      CommonUtil.sleep(brandCountry.getPolitenessDelay());

      if (!usePuppeteer && curRetry < 5) {
        response =
            UrlHelper.getResponseString(
                uri, ProxyUtil.get().getModel(brandCountry.getId()), false, 10000);
      } else {
        response =
            UrlHelper.getPuppeteerResponseString(
                uri,
                brandCountry.getWaitForElement(),
                brandCountry.getRepeat(),
                brandCountry.getId(),
                PuppeteerAction.HTML,
                false);
      }

      if (StringUtils.isBlank(response) || !response.contains(mustHave)) {
        log.error("Retry as Invalid response. {}", response);
        throw new RuntimeException("Response is INVALID.");
      }

      return response;
    } catch (Exception e) {
      if (curRetry <= 5) {
        switch (curRetry) {
          case 1:
            restartContainer(brandCountry, false);
            break;
          case 3:
            restartContainer(brandCountry, true);
            break;
        }
        log.info("Wait 3s and then retry (curRetry = {}) getting {}", curRetry + 1, uri);
        CommonUtil.sleep(3000);
        return getResponse(brandCountry, uri, curRetry + 1, mustHave, usePuppeteer);
      } else {
        log.error(e.getMessage(), e);
        restartContainer(brandCountry, true);
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(Constants.RUN_UID, e, uri, response);
        return null;
      }
    }
  }

  public static void restartContainer(ClBrandCountry brandCountry, boolean changeProxySerName) {
    // restart proxy model
    PuppeteerDockerHelper.get()
        .restartContainer(ProxyUtil.get().getModel(brandCountry.getId()), changeProxySerName);
    // restart pup model
    PuppeteerModel model =
        PuppeteerUtilMap.getUtil(brandCountry.getId()).getModel(brandCountry.getId());
    PuppeteerDockerHelper.get().restartContainer(model, false);
  }

  public static void runInThreadPool(
      List<Runnable> runnableList, String nameFormat, int noOfThread) {
    ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat(nameFormat).build();

    ExecutorService executor = Executors.newFixedThreadPool(noOfThread, namedThreadFactory);

    for (Runnable runnable : runnableList) {
      executor.submit(runnable);
    }

    awaitTerminationAfterShutdown(executor);
  }

  public static <T> List<T> runAndGetInThreadPool(
      List<Callable<T>> callables, String nameFormat, int noOfThread) {
    ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat(nameFormat).build();

    ExecutorService executor = Executors.newFixedThreadPool(noOfThread, namedThreadFactory);

    try {
      List<Future<T>> futures = executor.invokeAll(callables);
      executor.shutdown();

      return futures.stream()
          .map(
              tFuture -> {
                try {
                  return tFuture.get();
                } catch (InterruptedException | ExecutionException e) {
                  log.error(e.getMessage(), e);
                  return null;
                }
              })
          .collect(Collectors.toList());
    } catch (InterruptedException ex) {
      executor.shutdownNow();
      return new ArrayList<>();
    }
  }

  public static String getDictionaryValue(String key) {
    if (key != null && dictionary.containsKey(key.toUpperCase())) {
      return dictionary.get(key);
    }
    return key;
  }
}
