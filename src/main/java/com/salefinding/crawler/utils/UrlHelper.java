package com.salefinding.crawler.utils;

import com.salefinding.crawler.models.ProxyModel;
import com.salefinding.crawler.models.PuppeteerAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Map;

public class UrlHelper {

  private static Logger logger = LoggerFactory.getLogger(UrlHelper.class);

  public static String getRealUrl(
      String url, String waitForElement, Boolean repeat, Long brandCountryId, boolean shouldRetry) {
    return getRealUrl(
        url,
        waitForElement,
        repeat,
        brandCountryId,
        PuppeteerUtilMap.usePuppeteer(brandCountryId),
        shouldRetry);
  }

  private static String getRealUrl(
      String url,
      String waitForElement,
      Boolean repeat,
      Long brandCountryId,
      boolean usePuppeteer,
      boolean shouldRetry) {
    try {
      String realUrl = url;
      if (usePuppeteer) {
        realUrl =
            getPuppeteerResponseString(
                url, waitForElement, repeat, brandCountryId, PuppeteerAction.URL, shouldRetry);
      } else {
        ProxyModel proxy =
            ProxyUtil.get().getModelListSize() > 0
                ? ProxyUtil.get().getModel(brandCountryId)
                : null;
        HttpClientManager.HttpResponse response =
            HttpClientManager.getInstance()
                .sendHttpRequest(
                    proxy, new HttpClientManager.RequestEvent(url, true, null), shouldRetry, 0);
        if (response != null) {
          realUrl = response.getUrl();
        }
      }

      if (isValidURL(realUrl)) {
        return realUrl;
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

    return url;
  }

  private static boolean isValidURL(String urlString) {
    try {
      URL url = new URL(urlString);
      url.toURI();
      return true;
    } catch (Exception exception) {
      logger.error("{} is NOT a valid URL.", urlString);
      return false;
    }
  }

  public static boolean isConnectible(String url, ProxyModel proxy, long timeOut) {
    try {
      HttpClientManager.HttpResponse response =
          HttpClientManager.getInstance()
              .sendHttpRequest(
                  proxy, new HttpClientManager.RequestEvent(url, true, null), false, timeOut);
      if (response != null) {
        return response.isSuccessful();
      }
    } catch (Throwable e) {
      logger.error("Cannot connect to {} via {}", url, proxy);
    }

    return false;
  }

  public static String getResponseString(
      String url, ProxyModel proxy, boolean shouldRetry, long timeOut) throws Exception {
    return getResponseString(url, null, null, proxy, shouldRetry, timeOut);
  }

  public static String getResponseString(
      String url,
      Map<String, String> params,
      Map<String, String> headers,
      ProxyModel proxy,
      boolean shouldRetry,
      long timeOut)
      throws Exception {
    HttpClientManager.HttpResponse response =
        HttpClientManager.getInstance()
            .sendHttpRequest(
                proxy,
                new HttpClientManager.RequestEvent(url, true, params, headers),
                shouldRetry,
                timeOut);
    if (response != null) {
      return response.getBody();
    }
    return "";
  }

  public static String getPuppeteerResponseString(
      String url,
      String waitForElement,
      Boolean repeat,
      Long brandCountryId,
      PuppeteerAction action,
      boolean shouldRetry)
      throws Exception {
    HttpClientManager.HttpResponse response =
        HttpClientManager.getInstance()
            .sendHttpRequest(
                new HttpClientManager.PuppeteerRequestEvent(
                    url, waitForElement, repeat, true, null),
                shouldRetry,
                0,
                PuppeteerUtilMap.getUtil(brandCountryId),
                brandCountryId,
                action);
    if (response != null) {
      return response.getBody();
    }

    return "";
  }
}
