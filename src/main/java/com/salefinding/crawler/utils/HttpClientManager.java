package com.salefinding.crawler.utils;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.models.ProxyModel;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.repositories.CrawlerRepoFactory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class HttpClientManager {

  private static Logger logger = LoggerFactory.getLogger(HttpClientManager.class);

  private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

  private static HttpClientManager httpClientManager = new HttpClientManager();

  private long defaultTimeOut =
      PropUtil.getLong(Constants.HTTP_TIMEOUT, Constants.CONNECTION_TIMEOUT_MS);

  public static HttpClientManager getInstance() {
    if (httpClientManager == null) {
      synchronized (HttpClientManager.class) {
        if (httpClientManager == null) {
          httpClientManager = new HttpClientManager();
        }
      }
    }
    return httpClientManager;
  }

  public HttpResponse sendHttpRequest(
      RequestEvent reqEvent,
      boolean shouldRetry,
      long timeOut,
      PuppeteerUtil util,
      Long brandCountryId,
      PuppeteerAction action)
      throws Exception {
    return sendHttpRequest(
        reqEvent, shouldRetry, timeOut, util, brandCountryId, action, null, null);
  }

  public HttpResponse sendHttpRequest(
      ProxyModel proxyModel, RequestEvent reqEvent, boolean shouldRetry, long timeOut)
      throws Exception {
    return sendHttpRequest(reqEvent, shouldRetry, timeOut, null, null, null, null, proxyModel);
  }

  public HttpResponse sendHttpRequest(
      RequestEvent reqEvent, boolean shouldRetry, long timeOut, Map.Entry<String, String> authen)
      throws Exception {
    return sendHttpRequest(reqEvent, shouldRetry, timeOut, null, null, null, authen, null);
  }

  public HttpResponse sendHttpRequest(
      RequestEvent reqEvent,
      boolean shouldRetry,
      long timeOut,
      PuppeteerUtil util,
      Long brandCountryId,
      PuppeteerAction action,
      Map.Entry<String, String> authen,
      ProxyModel proxyModel)
      throws Exception {
    return buildResponse(
        sendHttpRequestReturnBase(
            reqEvent, shouldRetry, timeOut, util, brandCountryId, action, authen, proxyModel));
  }

  public Response sendHttpRequestReturnBase(
      RequestEvent reqEvent,
      boolean shouldRetry,
      long timeOut,
      PuppeteerUtil util,
      Long brandCountryId,
      PuppeteerAction action,
      Map.Entry<String, String> authen,
      ProxyModel proxyModel)
      throws Exception {
    RequestEvent newEvent = new RequestEvent(reqEvent);

    // Create a trust manager that does not validate certificate chains
    final TrustManager[] trustAllCerts =
        new TrustManager[] {
          new X509TrustManager() {
            @Override
            public void checkClientTrusted(
                java.security.cert.X509Certificate[] chain, String authType) {}

            @Override
            public void checkServerTrusted(
                java.security.cert.X509Certificate[] chain, String authType) {}

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return new X509Certificate[0];
            }
          }
        };

    // Install the all-trusting trust manager
    final SSLContext sslContext = SSLContext.getInstance("SSL");
    sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
    // Create an ssl socket factory with our all-trusting manager
    final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

    OkHttpClient.Builder builder =
        new OkHttpClient.Builder()
            .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
            .protocols(Arrays.asList(Protocol.HTTP_2, Protocol.HTTP_1_1));

    if (timeOut > 0) {
      setTimeOut(builder, timeOut);
    } else {
      setTimeOut(builder, defaultTimeOut);
    }

    if (util != null) {
      PuppeteerRequestEvent puppeteerEvent = (PuppeteerRequestEvent) reqEvent;
      newEvent.setUrl(
          util.getModel(brandCountryId)
              .getPuppeteerUrl(
                  reqEvent.getUrl(),
                  puppeteerEvent.getWaitForElement(),
                  puppeteerEvent.getRepeat(),
                  action));
    }

    if (proxyModel == null) {
      builder.proxy(Proxy.NO_PROXY);
    } else {
      setProxy(builder, proxyModel);
    }

    if (authen != null) {
      builder.authenticator(
          (route, response) -> {
            String credential = Credentials.basic(authen.getKey(), authen.getValue());
            return response
                .request()
                .newBuilder()
                .header(HttpHeaders.AUTHORIZATION, credential)
                .build();
          });
    }

    OkHttpClient client = builder.build();

    while (true) {
      try {
        PauseAllProcessesUtil.waitIfLocked();

        logger.info(
            "Send {} request to {} via new url {}",
            newEvent.isGet ? "GET" : "POST",
            reqEvent.getUrl(),
            newEvent.getUrl());
        return getResponse(client, newEvent);
      } catch (Exception e) {
        if (shouldRetry) {
          if (RetryManageUtil.canStillRetry(reqEvent.getUrl())) {
            logger.error(
                String.format(
                    "\nReq Endpoint: %s\nParams: %s\nException: %s",
                    newEvent.getUrl(), newEvent.getParams(), e.getMessage()));
            logger.info("RETRYING connect to {}", reqEvent.getUrl());
            RetryManageUtil.increaseRetryTimes(reqEvent.getUrl(), false);
            CommonUtil.sleep(10000, logger);
          } else {
            logger.error(
                String.format(
                    "\nReq Endpoint: %s\nParams: %s", newEvent.getUrl(), newEvent.getParams()),
                e);
            CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
                .getErrorSummaryRepo()
                .saveError(
                    Constants.RUN_UID,
                    "SendHttpRequest retried max times. Restart containers.",
                    e,
                    reqEvent,
                    proxyModel);
            PuppeteerDockerHelper.get().restartPuppeteerContainers(true, true);
            RetryManageUtil.resetRetryTimes(reqEvent.getUrl());
          }
        } else {
          throw e;
        }
      }
    }
  }

  private Response getResponse(OkHttpClient client, RequestEvent reqEvent) throws IOException {
    Request request = buildRequest(reqEvent);
    Response response = client.newCall(request).execute();
    RetryManageUtil.resetRetryTimes(reqEvent.getUrl());
    if (!response.isSuccessful()) {
      String message = response.message();
      response.close();
      throw new RuntimeException(message);
    }
    return response;
  }

  private Request buildRequest(RequestEvent reqEvent) {
    String url = reqEvent.getUrl();
    if (url.startsWith("//")) {
      if ("https".equalsIgnoreCase(url.substring(0, 5))) {
        url = "https:" + url;
      } else {
        url = "http:" + url;
      }
    }

    Map<String, String> headers = new HashMap<>();
    headers.put(
        "User-Agent",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36");
    headers.put(
        "accept",
        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
    if (reqEvent.getHeaders() != null) {
      headers.putAll(reqEvent.getHeaders());
    }

    Headers.Builder headersBuilder = new Headers.Builder();
    for (Map.Entry<String, String> e : headers.entrySet()) {
      headersBuilder.set(e.getKey(), e.getValue());
    }

    Request.Builder requestBuilder = new Request.Builder().url(url).headers(headersBuilder.build());

    Request request;
    if (reqEvent.isGet) {
      request = requestBuilder.get().build();
    } else {
      RequestBody body = RequestBody.create(JSON, reqEvent.getBody());
      requestBuilder
          .header(HttpHeaders.CONTENT_TYPE, JSON.toString())
          .header(HttpHeaders.ACCEPT, "application/json");

      switch (reqEvent.getHttpMethod()) {
        case "POST":
          requestBuilder.post(body);
          break;
        case "PUT":
          requestBuilder.put(body);
          break;
      }

      request = requestBuilder.build();
    }

    return request;
  }

  private void setProxy(OkHttpClient.Builder builder, @Nonnull ProxyModel proxyModel) {
    Proxy.Type type;
    if (StringUtils.isBlank(proxyModel.getProtocol())
        || proxyModel.getProtocol().toUpperCase().contains(Proxy.Type.HTTP.name())) {
      type = Proxy.Type.HTTP;
    } else {
      type = Proxy.Type.SOCKS;
    }
    builder.proxy(
        new Proxy(type, new InetSocketAddress(proxyModel.getHost(), proxyModel.getPort())));
    if (StringUtils.isNotBlank(proxyModel.getUsername())) {
      builder.proxyAuthenticator(
          (route, response) -> {
            String credential =
                Credentials.basic(proxyModel.getUsername(), proxyModel.getPassword());
            return response
                .request()
                .newBuilder()
                .header(HttpHeaders.PROXY_AUTHORIZATION, credential)
                .build();
          });
    }
  }

  private void setTimeOut(OkHttpClient.Builder builder, long timeOut) {
    builder.connectTimeout(timeOut, TimeUnit.MILLISECONDS);
    builder.writeTimeout(timeOut, TimeUnit.MILLISECONDS);
    builder.readTimeout(timeOut, TimeUnit.MILLISECONDS);
  }

  private HttpResponse buildResponse(Response response) {
    HttpResponse result = null;
    try {
      result =
          new HttpResponse(
              response.request().url().uri().toString(),
              response.body() != null ? response.body().string() : "",
              response.isSuccessful());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    } finally {
      if (response != null) {
        response.close();
      }
    }

    return result;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class RequestEvent {

    protected String url;

    protected boolean isGet;

    protected String httpMethod;

    protected Map<String, String> params;

    protected Map<String, String> headers;

    protected String body;

    public RequestEvent(String url, boolean isGet, Map<String, String> params) {
      this.params = params;
      this.isGet = isGet;
      if (params != null) {
        HttpUrl.Builder httpBuilder = Objects.requireNonNull(HttpUrl.parse(url)).newBuilder();
        for (Map.Entry<String, String> param : params.entrySet()) {
          httpBuilder.addQueryParameter(param.getKey(), param.getValue());
        }
        this.url = httpBuilder.toString();
      } else {
        this.url = url;
      }
    }

    public RequestEvent(
        String url, boolean isGet, Map<String, String> params, Map<String, String> headers) {
      this(url, isGet, params);
      this.headers = headers;
    }

    public RequestEvent(String url, String httpMethod, Map<String, String> params) {
      this(url, "GET".equals(httpMethod), params);
      this.httpMethod = httpMethod;
    }

    RequestEvent(RequestEvent event) {
      this(event.getUrl(), event.isGet(), event.getParams());
      this.body = event.getBody();
      this.httpMethod = event.getHttpMethod();
    }
  }

  @EqualsAndHashCode(callSuper = true)
  @Data
  public static class PuppeteerRequestEvent extends RequestEvent {

    protected String waitForElement;

    protected Boolean repeat;

    PuppeteerRequestEvent(
        String url,
        String waitForElement,
        Boolean repeat,
        boolean isGet,
        Map<String, String> paramsMap) {
      super(url, isGet, paramsMap);
      this.waitForElement = waitForElement;
      this.repeat = repeat;
    }
  }

  @Data
  @AllArgsConstructor
  public static class HttpResponse {

    private String url;

    private String body;

    private boolean isSuccessful;

    public HttpResponse(boolean isSuccessful) {
      this.isSuccessful = isSuccessful;
    }
  }
}
