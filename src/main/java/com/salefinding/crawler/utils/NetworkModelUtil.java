package com.salefinding.crawler.utils;

import com.salefinding.crawler.helper.GCloudComputeHelper;
import com.salefinding.crawler.models.NetworkModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public abstract class NetworkModelUtil<T extends NetworkModel> {

  protected final Logger logger;

  protected final List<T> modelList;

  /** Map of brandCountryId and model list The model list is divided into ControllerNo batches. */
  protected final Map<Long, List<T>> brandCountryModels;

  protected final Queue<T> availableModels;

  protected int rotationCurIndex = 0;

  protected final String groupName;

  protected Class<T> clazz;

  protected NetworkModelUtil(Class<T> clazz, String groupName) {
    this.logger = LoggerFactory.getLogger(this.getClass());
    this.groupName = groupName;
    this.clazz = clazz;

    modelList = new CopyOnWriteArrayList<>();
    brandCountryModels = new ConcurrentHashMap<>();
    availableModels = new ConcurrentLinkedQueue<>();
  }

  public synchronized void loadModelList() {
    logger.info("LOAD {} list.", clazz.getSimpleName());

    if (modelList.size() > 0) {
      modelList.removeIf(
          t -> {
            boolean valid = isValid(t);
            if (!valid) {
              logger.info("Model {} --- INVALID", t);
              availableModels.remove(t);
              return true;
            }
            return false;
          });
    }

    addDefaultModelList();

    if (groupName != null) {
      List<String> ipList = GCloudComputeHelper.getInstance().getIPListOfInstances(groupName);
      for (String ip : ipList) {
        T model = createModel(ip);
        validateAndAddModel(model);
      }
    }
  }

  public void validateAndAddModel(T model) {
    if (!modelList.contains(model)) {
      boolean valid = isValid(model);
      logger.info("Validate {} - {}", model, valid ? "VALID" : "INVALID");
      if (valid) {
        addModel(model);
      }
    }
  }

  public void addModel(T model) {
    logger.info("Adding model {}", model);
    modelList.add(model);
    availableModels.add(model);
  }

  protected void addDefaultModelList() {
    // Default is adding nothing.
  }

  public synchronized T getModel(Long brandCountryId) {
    T model;
    if (Constants.USE_ROTATE_NETWORK_MODEL_SELECTION) {
      if (rotationCurIndex >= modelList.size()) {
        rotationCurIndex = 0;
      }
      model = modelList.get(rotationCurIndex++);
    } else {
      // use fixed model for every thread
      String threadName = Thread.currentThread().getName();
      int threadIndex = Integer.parseInt(threadName.substring(threadName.lastIndexOf('-') + 1));
      model =
          brandCountryModels
              .get(brandCountryId)
              .get(threadIndex % brandCountryModels.get(brandCountryId).size());
    }
    logger.info("Model will be used {}:{}", model.getHost(), model.getPort());
    return model;
  }

  public synchronized long registerBatch(Long brandCountryId, int threadNo) {
    logger.error(
        "Try register {} for brandCountry {} with thread-no {}",
        clazz.getName(),
        brandCountryId,
        threadNo);
    if (brandCountryModels.size() >= 5 && !brandCountryModels.containsKey(brandCountryId)) {
      logger.error(
          "CANNOT register for brandCountry {} with thread-no {}", brandCountryId, threadNo);
      return -1;
    }

    if (!brandCountryModels.containsKey(brandCountryId)) {
      List<T> models = getAvailableModels(threadNo);
      if (models != null) {
        brandCountryModels.put(brandCountryId, models);
      } else {
        logger.error("Cannot register {} for brandCountry {}", clazz.getName(), brandCountryId);
        return -1;
      }
    }

    logger.info(
        "NetworkModel list for brandCountry {} : {}",
        brandCountryId,
        brandCountryModels.get(brandCountryId));

    return brandCountryId;
  }

  public synchronized void unregisterBatch(Long brandCountryId) {
    if (brandCountryModels.containsKey(brandCountryId)) {
      logger.info("Remove {} for {}", clazz.getName(), brandCountryId);
      brandCountryModels.get(brandCountryId).forEach(model -> availableModels.add(model));
      brandCountryModels.remove(brandCountryId);
    }
  }

  private List<T> getAvailableModels(int threadNo) {
    List<T> models = new ArrayList<>();
    if (availableModels.size() < threadNo) {
      return null;
    }

    for (int i = 0; i < threadNo; i++) {
      models.add(availableModels.poll());
    }

    return models;
  }

  protected T createModel(String ip) {
    try {
      T model = clazz.getDeclaredConstructor().newInstance();
      model.setHost(ip);
      return model;
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      return null;
    }
  }

  public abstract boolean isValid(T model);

  public int getModelListSize() {
    return modelList.size();
  }

  public void resetModelList() {
    logger.info("Reset ModelList for {}", clazz.getSimpleName());
    modelList.clear();
    availableModels.clear();
    brandCountryModels.clear();
  }

  public void reload() {
    logger.info("RELOAD {} list.", clazz.getSimpleName());
    Map<Long, Integer> registeredBrandCountries =
        brandCountryModels.keySet().stream()
            .collect(Collectors.toMap(o -> o, o -> brandCountryModels.get(o).size()));

    loadModelList();

    brandCountryModels.clear();
    for (Map.Entry<Long, Integer> entry : registeredBrandCountries.entrySet()) {
      registerBatch(entry.getKey(), entry.getValue());
    }
  }
}
