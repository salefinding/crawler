package com.salefinding.crawler.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static com.salefinding.crawler.utils.Constants.PROP_FILE_NAME;

public class PropUtil {

  private static final Logger logger = LoggerFactory.getLogger(PropUtil.class);

  private static Properties prop = null;

  private static synchronized void init() {
    try {
      prop = new Properties();
      prop.load(new FileInputStream(PROP_FILE_NAME));
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
    }
  }

  public static String getString(String key) {
    return getString(key, null);
  }

  public static String getString(String key, String defaultValue) {
    if (prop == null) {
      init();
    }
    return prop.getProperty(key, defaultValue);
  }

  public static int getInt(String key, int defaultValue) {
    return Integer.parseInt(getString(key, String.valueOf(defaultValue)));
  }

  public static boolean getBoolean(String key, boolean defaultValue) {
    return Boolean.parseBoolean(getString(key, String.valueOf(defaultValue)));
  }

  public static long getLong(String key, long defaultValue) {
    return Long.parseLong(getString(key, String.valueOf(defaultValue)));
  }

  public static double getDouble(String key, double defaultValue) {
    return Double.parseDouble(getString(key, String.valueOf(defaultValue)));
  }
}
