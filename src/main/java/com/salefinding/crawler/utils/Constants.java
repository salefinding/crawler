package com.salefinding.crawler.utils;

import java.math.BigDecimal;
import java.util.UUID;

public interface Constants {

  String DB_CRAWLER = "crawler";

  String DB_API = "api";

  String NUMBER_OF_CONTROLLERS_KEY = "number_of_controllers";

  String NUMBER_OF_TRANSFER_THREAD_KEY = "number_of_transfer_threads";

  String RESUMABLE_CRAWLING = "resumable";

  String PROXY_KEY = "proxy";

  String SURF_SHARK_KEY = "surfshark";

  String LOCAL_PUPPETEER_KEY = "local_puppeteer";

  String LOCAL_PUPPETEER_IP = "local_puppeteer_ip";

  String GCLOUD_KEY = "gcloud.key";

  String GCLOUD_PROJECT = "gcloud.project";

  String GCLOUD_ZONE_NAME = "gcloud.zone.name";

  String GCLOUD_GROUP_NAME = "gcloud.group.name";

  String GCLOUD_PUPPETEER_GROUP_SIZE = "gcloud.puppeteer-group-size";

  String RELOAD_NETWORK_INSTANCES_MINUTE = "reload-network-instances-minute";

  String RELOAD_PUPPETEER_INSTANCES_MINUTE = "reload-puppeteer-instances-minute";

  String DOCKER_PUPPETEER_CONTAINER_SIZE = "docker.puppeteer-container-size";

  String DOCKER_PUPPETEER_IMAGE = "docker.puppeteer-image";

  String DOCKER_USE_OPENVPN = "docker.use-openvpn";

  String DOCKER_URI = "docker.uri";

  String PROP_FILE_NAME = "app.properties";

  String HTTP_TIMEOUT = "http.timeout";

  String MAX_RETRY = "max.retry";

  String ENVIRONMENT = "environment";

  String NETWORK_MODEL_SELECTION = "network-model-selection";

  String WP_API_URL = "wp-api-url";

  String WP_API_KEY = "wp-api-key";

  String WP_API_SECRET = "wp-api-secret";

  String CLOUDINARY_API_KEY = "cloudinary-api-key";

  String CLOUDINARY_API_SECRET = "cloudinary-api-secret";

  String TRANSFER_BRAND_ID = "transfer-brand-id";

  BigDecimal INTEREST_RATE = BigDecimal.valueOf(PropUtil.getDouble("interest-rate", 1.0));

  BigDecimal MAX_INTEREST = BigDecimal.valueOf(PropUtil.getLong("max-interest", 1000000L));

  BigDecimal MIN_INTEREST = BigDecimal.valueOf(PropUtil.getLong("min-interest", 0L));

  String RUN_UID = UUID.randomUUID().toString().replaceAll("-", "");

  int CONNECTION_TIMEOUT_MS = 60 * 1000;

  boolean IS_DEV = "dev".equalsIgnoreCase(PropUtil.getString(Constants.ENVIRONMENT, "prod"));

  boolean USE_ROTATE_NETWORK_MODEL_SELECTION =
      "rotate".equalsIgnoreCase(PropUtil.getString(Constants.NETWORK_MODEL_SELECTION, "rotate"));

  String localPuppeteerIp = PropUtil.getString(Constants.LOCAL_PUPPETEER_IP);

  int STARTING_PORT = IS_DEV ? 5000 : 4000;

  int STARTING_PROXY_PORT = IS_DEV ? 7000 : 6000;

  String PUP_PREFIX = Constants.IS_DEV ? "dpup" : "pup";

  String PROXY_PREFIX = Constants.IS_DEV ? "dproxy" : "proxy";

  String WEB_PREFIX = Constants.IS_DEV ? "dweb" : "web";

  enum Country {
    SINGAPORE("SG", "Singapore"),
    MAYLAYSIA("ML", "Malaysia"),
    VIETNAM("VN", "Vietnam");

    String shortCode;

    String name;

    Country(String shortCode, String name) {
      this.shortCode = shortCode;
      this.name = name;
    }

    public String getShortCode() {
      return shortCode;
    }

    public String getName() {
      return name;
    }
  }
}
