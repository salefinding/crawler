package com.salefinding.crawler.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RetryManageUtil {

  private static final Logger logger = LoggerFactory.getLogger(RetryManageUtil.class);

  private static final Map<String, Integer> retryMap = new ConcurrentHashMap<>();

  public static final Integer MAX_RETRY = PropUtil.getInt(Constants.MAX_RETRY, 5);

  public static boolean canStillRetry(String url) {
    return retryMap.get(url) == null || retryMap.get(url) <= MAX_RETRY;
  }

  public static void increaseRetryTimes(String url, boolean shouldWait) {
    if (retryMap.containsKey(url)) {
      retryMap.put(url, retryMap.get(url) + 1);
    } else {
      retryMap.put(url, 1);
    }
    if (shouldWait) {
      long sleepSeconds = Math.round(Math.pow(2, 1.4 * retryMap.get(url)) * 2); // 2^(1.4*retry)*2
      logger.info("Sleep {}s before retrying {}", sleepSeconds, url);
      CommonUtil.sleep(sleepSeconds * 1000);
    }
  }

  public static void resetRetryTimes(String url) {
    //logger.info("Temporarily disable resetRetryTimes!!!");
    if (retryMap.containsKey(url)) {
      logger.info("RESET retry times for {}", url);
      retryMap.remove(url);
    }
  }

  public static void resetAll() {
    logger.info("RESET all retry times");
    retryMap.clear();
  }
}
