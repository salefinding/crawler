package com.salefinding.crawler.category;

import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class UniqloCategoryParser2 extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(UniqloCategoryParser2.class);

  public UniqloCategoryParser2(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    return new HashSet<>();
  }
}
