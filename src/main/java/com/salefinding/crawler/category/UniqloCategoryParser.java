package com.salefinding.crawler.category;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class UniqloCategoryParser extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(UniqloCategoryParser.class);

  public UniqloCategoryParser(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();

    try {
      Document doc = getDoc();
      Element nav = doc.getElementById("navHeader");

      Elements mmItems = nav.getElementsByAttributeValueContaining("class", "mm-item");
      for (Element mmItem : mmItems) {
        if (mmItem.getElementsByAttributeValueContaining("class", "show_bk").size() < 1) {
          continue;
        }
        String gender =
            mmItem
                .getElementsByAttributeValueContaining("class", "show_bk")
                .first()
                .text()
                .toUpperCase();

        Elements secMenus = mmItem.getElementsByAttributeValueContaining("class", "secmenu");
        for (int i = 0; i < secMenus.size(); i++) {
          Element secMenu = secMenus.get(i);
          String secMenuName = secMenu.text().toUpperCase();
          if (StringUtils.isBlank(secMenuName)) {
            continue;
          }

          if (!isValidCategory(secMenuName) || secMenuName.contains("COLLECTIONS")) {
            continue;
          }

          Element secondMenu =
              mmItem.getElementsByAttributeValueContaining("class", "second_level").get(i);

          for (Element navLink :
              secondMenu.getElementsByAttributeValueContaining("class", "cateNaviLink")) {
            Element link = navLink.select("a").first();
            String href = link.attr("href");
            String name = link.text().toUpperCase();

            if (!isValidCategory(name)) {
              continue;
            }

            ClCategoryCrawler category =
                createCategory(gender, secMenuName, name, null, null, href, false);
            if (category != null) {
              logger.info(
                  "Adding Category {}-{}-{}-{} : {}",
                  brandCountry.getBrand().getName(),
                  category.getCat1(),
                  category.getCat2(),
                  category.getCat3(),
                  category.getUrl());
              categories.add(category);
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for Uniqlo!"),
                brandCountry.getStartUrl());
      }
    }
    return categories;
  }
}
