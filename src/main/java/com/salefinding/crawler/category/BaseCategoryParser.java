package com.salefinding.crawler.category;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.*;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Set;

public abstract class BaseCategoryParser implements ICategoryParser {

  private Logger logger = LoggerFactory.getLogger(BaseCategoryParser.class);

  protected ClBrandCountry brandCountry;

  BaseCategoryParser(ClBrandCountry brandCountry) {
    this.brandCountry = brandCountry;
  }

  protected Document getDoc() {
    int retry = 0;
    while (true) {
      try {
        if (PuppeteerUtilMap.usePuppeteer(brandCountry.getId())) {
          return Jsoup.parse(
              UrlHelper.getPuppeteerResponseString(
                  brandCountry.getStartUrl(),
                  brandCountry.getWaitForElement(),
                  brandCountry.getRepeat(),
                  brandCountry.getId(),
                  PuppeteerAction.HTML,
                  false));
        }
        return Jsoup.parse(
            UrlHelper.getResponseString(
                brandCountry.getStartUrl(),
                ProxyUtil.get().getModel(brandCountry.getId()),
                false,
                0));
      } catch (Exception e) {
        if (++retry < 5) {
          logger.error(e.getMessage(), e);
        } else {
          CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
              .getErrorSummaryRepo()
              .saveError(
                  Constants.RUN_UID,
                  "Category getDoc failed 5 times. Restart containers.",
                  e,
                  brandCountry);
          PuppeteerDockerHelper.get().restartPuppeteerContainers(true, true);
          retry = 0;
        }
      }
    }
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    PauseAllProcessesUtil.waitIfLocked();

    return null;
  }

  protected ClCategoryCrawler createCategory(
      String cat1, String cat2, String cat3, String cat4, String cat5, String url)
      throws Exception {
    return createCategory(cat1, cat2, cat3, cat4, cat5, url, true);
  }

  protected ClCategoryCrawler createCategory(
      String cat1,
      String cat2,
      String cat3,
      String cat4,
      String cat5,
      String url,
      boolean getRealUrl)
      throws Exception {
    if ("featured".equalsIgnoreCase(cat2)) {
      return null;
    }

    if (StringUtils.isBlank(url)) {
      throw new Exception("URL is blank.");
    }

    if (url.startsWith("//")) {
      if ("https".equalsIgnoreCase(brandCountry.getStartUrl().substring(0, 5))) {
        url = "https:" + url;
      } else {
        url = "http:" + url;
      }
    }

    if (!url.startsWith(brandCountry.getStartUrl())) { // relative link
      URI uri = URI.create(brandCountry.getStartUrl());
      url = String.format("%s://%s", uri.getScheme(), uri.getHost()) + url;
    }

    // Get a real URL of category url
    if (getRealUrl) {
      CommonUtil.sleep(brandCountry.getPolitenessDelay(), logger);
      url = UrlHelper.getRealUrl(url, null, brandCountry.getRepeat(), brandCountry.getId(), true);
    }

    ClCategoryCrawler category = new ClCategoryCrawler(brandCountry);

    category.setCat1(cat1 != null ? cat1.toUpperCase() : null);
    category.setCat2(cat2 != null ? cat2.toUpperCase() : null);
    category.setCat3(cat3 != null ? cat3.toUpperCase() : null);
    category.setCat4(cat4 != null ? cat4.toUpperCase() : null);
    category.setCat5(cat5 != null ? cat5.toUpperCase() : null);
    category.setUrl(url);

    return category;
  }

  public Set<ClCategoryCrawler> retryParse() {
    RetryManageUtil.increaseRetryTimes(brandCountry.getStartUrl(), true);
    logger.info("RETRY calling to {}", brandCountry.getStartUrl());
    CommonUtil.sleep(3000, logger);
    return parse();
  }

  protected boolean isValidCategory(String cat) {
    return !cat.contains("ALL")
        && !cat.contains("SPECIALS")
        && !cat.contains("OCCASIONS")
        && !cat.contains("NEW ARRIVALS")
        && !cat.contains("FEATURED");
  }
}
