package com.salefinding.crawler.category;

import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class PedroCategoryParser3 extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(PedroCategoryParser3.class);

  public PedroCategoryParser3(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();

    Set<String> cat1s = new HashSet<>();
    cat1s.add("WOMEN");
    cat1s.add("MEN");

    Set<String> cat2s = new HashSet<>();
    cat2s.add("SHOES");
    cat2s.add("BAGS");
    cat2s.add("ACCESSORIES");

    try {
      Document doc = getDoc();
      Element nav =
          doc.getElementsByAttributeValueContaining("class", "menu-for-web")
              .get(0)
              .getElementById("menu_ul_oerlay");
      for (Element menu : nav.children()) {
        String cat1 = menu.select("a.ms-label").first().text().toUpperCase();
        if (cat1s.contains(cat1)) {
          Elements catHolders =
              menu.getElementsByAttributeValueContaining("class", "category-holder");
          for (Element catHolder : catHolders) {
            Elements cats = catHolder.getElementsByAttributeValueContaining("class", "form-group");
            boolean shouldIgnore = false;
            String cat2 = null;
            for (Element cat : cats) {
              if (cat.attr("class").contains("level1")) {
                cat2 = cat.text().toUpperCase();
                if (!cat2s.contains(cat2)) {
                  shouldIgnore = true;
                }
                break;
              }
            }
            if (!shouldIgnore && cat2 != null) {
              for (Element cat : cats) {
                if (!cat.attr("class").contains("level1")) {
                  String cat3 = cat.text().toUpperCase();
                  String url = cat.attr("href");

                  if (StringUtils.isBlank(url)) {
                    logger.error("Category {} has a blank url.", cat3);
                    continue;
                  }

                  if (!isValidCategory(cat3)) {
                    continue;
                  }

                  ClCategoryCrawler category =
                      createCategory(
                          cat1, cat2, cat3, null, null, cat.attr("href") + "?p=1", false);

                  if (!categories.contains(category)) {
                    saveAllPagesOfCategory(categories, category);
                  }
                }
              }
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for Pedro!"),
                brandCountry.getStartUrl());
      }
    }
    return categories;
  }

  private void saveAllPagesOfCategory(
      Set<ClCategoryCrawler> categories, ClCategoryCrawler category) {
    categories.add(category);
    logger.info(
        "Adding Category {}-{}-{}-{} : {}",
        brandCountry.getBrand().getName(),
        category.getCat1(),
        category.getCat2(),
        category.getCat3(),
        category.getUrl());

    try {
      Document doc = getDocFromUrl(category.getUrl());

      Element pagesElement =
          doc.getElementsByAttributeValue("class", "toolbar-bottom")
              .first()
              .getElementsByAttributeValue("class", "pages")
              .first();
      if (pagesElement != null) {
        Elements pages = pagesElement.getElementsByTag("a");
        int count = 0;
        for (Element page : pages) {
          String text = page.text();
          if (StringUtils.isNotBlank(text)) {
            try {
              int temp = Integer.parseInt(text);
              if (temp > count) {
                count = temp;
              }
            } catch (Exception e) {
              logger.error(e.getMessage(), e);
            }
          }
        }

        for (int i = 2; i <= count; i++) {
          ClCategoryCrawler newCat =
              createCategory(
                  category.getCat1(),
                  category.getCat2(),
                  category.getCat3(),
                  null,
                  null,
                  category.getUrl().replace("p=1", "p=" + i),
                  false);

          categories.add(newCat);
          logger.info(
              "Adding Category {}-{}-{}-{} : {}",
              brandCountry.getBrand().getName(),
              newCat.getCat1(),
              newCat.getCat2(),
              newCat.getCat3(),
              newCat.getUrl());
        }
      }
    } catch (Exception e) {
      logger.error("No more pages for the category " + category.getUrl(), e);
    }
  }

  private Document getDocFromUrl(String url) throws Exception {
    return Jsoup.parse(
        UrlHelper.getPuppeteerResponseString(
            url,
            brandCountry.getWaitForElement(),
            brandCountry.getRepeat(),
            brandCountry.getId(),
            PuppeteerAction.HTML,
            true));
  }
}
