package com.salefinding.crawler.category;

import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.HashSet;
import java.util.Set;

@Slf4j
public class PedroCategoryParser4 extends BaseCategoryParser {

  private static final String CATEGORY_XML_URL =
      "https://www.pedroshoes.com/sg/sitemap_2-category.xml";


  public PedroCategoryParser4(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    return new HashSet<>();

  }



  private Document getDocFromUrl(String url) throws Exception {
    return Jsoup.parse(
        UrlHelper.getPuppeteerResponseString(
            url,
            brandCountry.getWaitForElement(),
            brandCountry.getRepeat(),
            brandCountry.getId(),
            PuppeteerAction.HTML,
            true));
  }


}
