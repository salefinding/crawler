package com.salefinding.crawler.category;

import com.salefinding.models.crawler.ClCategoryCrawler;

import java.util.Set;

public interface ICategoryParser {

  Set<ClCategoryCrawler> parse();

  Set<ClCategoryCrawler> retryParse();
}
