package com.salefinding.crawler.category;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class CharlesKeithCategoryParser extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(CharlesKeithCategoryParser.class);

  public CharlesKeithCategoryParser(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();
    Set<String> cat1s = new HashSet<>();
    cat1s.add("SHOES");
    cat1s.add("BAGS");
    cat1s.add("ACCESSORIES");
    cat1s.add("SUNGLASSES");
    cat1s.add("KIDS");
    cat1s.add("JEWELLERY");

    try {
      Document doc = getDoc();
      Element nav = doc.getElementById("menu_ul_oerlay");

      for (Element menu : nav.children()) {
        String cat1 = menu.select("a.ms-label").first().child(0).text().toUpperCase();
        if (cat1s.contains(cat1)) {
          parseMenu(menu, cat1, categories);
        } else if (cat1.equalsIgnoreCase("SALE")) {
          parseSaleCategory(menu, categories);
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for CharlesKeith!"),
                brandCountry.getStartUrl());
      }
    }
    return categories;
  }

  private void parseMenu(Element menu, String cat1, Set<ClCategoryCrawler> categories)
      throws Exception {
    Elements cat2s = menu.select("a.form-group.level1");
    for (Element cat2Element : cat2s) {
      String cat2 = cat2Element.text().toUpperCase();
      if (cat2.contains("ALL") || cat2.contains("SPECIALS") || cat2.contains("OCCASIONS")) {
        continue;
      }
      Element parent = cat2Element.parent();
      for (Element cat3Element : parent.select("a.form-group")) {
        if (!cat3Element.attr("class").toUpperCase().contains("LEVEL1")) {
          String cat3 = cat3Element.text().toUpperCase();
          // TODO we accept all category now
          /*if (cat3.contains("VIEW ALL")) {
              continue;
          }*/
          ClCategoryCrawler category =
              createCategory(cat1, cat2, cat3, null, null, cat3Element.attr("href") + "?p=1");
          if (category != null) {
            categories.add(category);
          }
        }
      }
    }
  }

  private void parseSaleCategory(Element menu, Set<ClCategoryCrawler> categories) throws Exception {
    Set<String> saleCat1s = new HashSet<>();
    saleCat1s.add("SHOES");
    saleCat1s.add("BAGS");
    saleCat1s.add("ACCESSORIES");
    saleCat1s.add("SUNGLASSES");
    saleCat1s.add("KIDS");

    Elements cat2s = menu.select("a.form-group.level1");
    for (Element cat2Element : cat2s) {
      String cat2 = cat2Element.text().toUpperCase();
      if (cat2.equalsIgnoreCase("SALE IN")) {
        Element parent = cat2Element.parent();
        for (Element cat3Element : parent.select("a.form-group")) {
          if (!cat3Element.attr("class").toUpperCase().contains("LEVEL1")) {
            String cat3 = cat3Element.text().toUpperCase();
            if (saleCat1s.contains(cat3)) {
              ClCategoryCrawler category =
                  createCategory(
                      cat3, "ALL SALE", null, null, null, cat3Element.attr("href") + "?p=1");
              if (category != null) {
                categories.add(category);
              }
            }
          }
        }
      }
    }
  }
}
