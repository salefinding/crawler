package com.salefinding.crawler.category;

import com.fasterxml.jackson.databind.JsonNode;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import com.salefinding.utils.SFObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class AdidasCategoryParser3 extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(AdidasCategoryParser3.class);

  public AdidasCategoryParser3(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();
    Set<String> cat1s = new HashSet<>();
    cat1s.add("MEN");
    cat1s.add("WOMEN");
    cat1s.add("KIDS");

    try {
      Document doc = getDoc();

      for (Element e : doc.getElementsByTag("script")) {
        String text = e.html();
        if (text.contains("DATA_STORE") && text.contains("JSON.parse")) {
          text = text.substring(text.indexOf("JSON.parse") + 12, text.length() - 2);
          text = text.replace("\\\"", "\"").replace("\\\\", "\\");
          JsonNode content = SFObjectMapper.toJsonNode(text, false).get("app").get("content");
          for (String cat1 : cat1s) {
            JsonNode cat1Node = content.get("fetch-header-url-" + cat1.toLowerCase());
            Document cat1Doc = Jsoup.parse(cat1Node.get("content").asText());
            parseMenu(
                cat1Doc.getElementsByAttributeValueContaining("class", "main-line").first(),
                cat1,
                categories);
          }
          break;
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for Adidas!"),
                brandCountry.getStartUrl());
      }
    }

    if (categories.size() < 5) {
      return retryParse();
    }

    return categories;
  }

  private void parseMenu(Element menu, String cat1, Set<ClCategoryCrawler> categories)
      throws Exception {
    Elements cat2s = menu.getElementsByAttributeValueContaining("class", "col-5");
    for (Element cat2Element : cat2s) {
      String cat2 = cat2Element.select("div.headline").first().child(0).text().toUpperCase();
      if (!cat2.contains("FEATURED") && !cat2.contains("SPORTS")) {
        Elements cat3s = cat2Element.select("ul>li");
        for (Element cat3Element : cat3s) {
          if (cat3Element.children().size() > 0) {
            String cat3 = cat3Element.child(0).child(0).text().toUpperCase();
            if (!cat3.contains("NEW ARRIVALS") && !cat3.contains("ALL")) {
              String href = cat3Element.child(0).child(0).attr("href");
              if (href.contains("?")) {
                href = href + "&start=0";
              } else {
                href = href + "?start=0";
              }

              ClCategoryCrawler category =
                  createCategory(cat1, cat2, cat3, null, null, href, false);
              if (category != null) {
                categories.add(category);
              }
            }
          }
        }
      }
    }
  }
}
