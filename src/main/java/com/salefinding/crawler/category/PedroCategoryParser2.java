package com.salefinding.crawler.category;

import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class PedroCategoryParser2 extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(PedroCategoryParser2.class);

  public PedroCategoryParser2(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();
    String cat1 =
        brandCountry
            .getStartUrl()
            .substring(brandCountry.getStartUrl().lastIndexOf("/") + 1)
            .toUpperCase();
    if (cat1.contains("?")) {
      cat1 = cat1.substring(0, cat1.indexOf("?"));
    }
    Set<String> cat2s = new HashSet<>();
    cat2s.add("SHOES");
    cat2s.add("BAGS");
    cat2s.add("ACCESSORIES");

    try {
      Document doc = getDoc();
      Element nav =
          doc.getElementsByAttributeValueContaining("class", "menu-for-web")
              .get(0)
              .getElementById("menu_ul_oerlay");
      for (Element menu : nav.children()) {
        String cat2 = menu.select("a.ms-label").first().text().toUpperCase();
        if (cat2s.contains(cat2)) {
          Elements cat3s = menu.getElementsByAttributeValueContaining("class", "form-group");
          for (Element cat3Element : cat3s) {
            if (!cat3Element.attr("class").contains("level1")) {
              String cat3 = cat3Element.text().toUpperCase();
              // TODO we accept all category now
              /*if (StringUtils.isBlank(cat3) || cat3.contains("VIEW ALL")) {
                  continue;
              }*/

              String url = cat3Element.attr("href");
              if (StringUtils.isBlank(url)) {
                logger.error("Category {} has a blank url.", cat3);
                continue;
              }

              ClCategoryCrawler category = new ClCategoryCrawler(brandCountry);
              category.setCat1(cat1);
              category.setCat2(cat2);
              category.setCat3(cat3);
              category.setUrl(cat3Element.attr("href") + "?p=1");

              if (!categories.contains(category)) {
                saveAllPagesOfCategory(categories, category);
              }
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for Pedro!"),
                brandCountry.getStartUrl());
      }
    }
    return categories;
  }

  private void saveAllPagesOfCategory(
      Set<ClCategoryCrawler> categories, ClCategoryCrawler category) {
    try {
      categories.add(category);
      logger.info(
          "Adding Category {}-{}-{}-{} : {}",
          brandCountry.getBrand().getName(),
          category.getCat1(),
          category.getCat2(),
          category.getCat3(),
          category.getUrl());

      Document doc = getDocFromUrl(category.getUrl());

      Element numItems = doc.getElementsByAttributeValueContaining("class", "num-items").first();
      if (numItems != null) {
        String text = numItems.child(0).text();
        int num = Integer.parseInt(text.replaceAll("[^\\d.]", ""));

        if (num > 20) {
          num -= 20;
          int count = 2;
          while (num > 0) {
            ClCategoryCrawler newCat = new ClCategoryCrawler(brandCountry);
            newCat.setCat1(category.getCat1());
            newCat.setCat2(category.getCat2());
            newCat.setCat3(category.getCat3());
            newCat.setUrl(category.getUrl().replace("p=1", "p=" + count));

            categories.add(newCat);
            logger.info(
                "Adding Category {}-{}-{}-{} : {}",
                brandCountry.getBrand().getName(),
                newCat.getCat1(),
                newCat.getCat2(),
                newCat.getCat3(),
                newCat.getUrl());
            count++;
            num -= 20;
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      saveAllPagesOfCategory(categories, category);
    }
  }

  private Document getDocFromUrl(String url) throws Exception {
    return Jsoup.parse(
        UrlHelper.getPuppeteerResponseString(
            url,
            brandCountry.getWaitForElement(),
            brandCountry.getRepeat(),
            brandCountry.getId(),
            PuppeteerAction.HTML,
            true));
  }
}
