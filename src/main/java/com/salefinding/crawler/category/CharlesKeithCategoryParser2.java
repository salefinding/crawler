package com.salefinding.crawler.category;

import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.crawler.utils.UrlHelper;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class CharlesKeithCategoryParser2 extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(CharlesKeithCategoryParser2.class);

  private int pageSize = 30;

  public CharlesKeithCategoryParser2(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();
    Set<String> cat1s = new HashSet<>();
    cat1s.add("SHOES");
    cat1s.add("BAGS");
    cat1s.add("ACCESSORIES");
    cat1s.add("SUNGLASSES");
    cat1s.add("KIDS");
    cat1s.add("JEWELLERY");

    try {
      Document doc = getDoc();
      Element nav = doc.getElementsByAttributeValue("class", "menu-group").get(0);

      for (Element menu :
          nav.getElementsByAttributeValueContaining("class", "site_nav_list-item")) {
        String cat1 =
            menu.getElementsByAttributeValueContaining("class", "site_nav_list-top")
                .first()
                .attr("data-title")
                .toUpperCase();
        if (cat1s.contains(cat1)) {
          parseMenu(menu, cat1, categories);
        } else if (cat1.equalsIgnoreCase("SALE")) {
          parseSaleCategory(menu, categories, cat1s);
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for CharlesKeith!"),
                brandCountry.getStartUrl());
      }
    }
    return categories;
  }

  private void parseMenu(Element menu, String cat1, Set<ClCategoryCrawler> categories)
      throws Exception {
    Elements cat2s =
        menu.getElementsByAttributeValueContaining("class", "nav_dropdown_category-item")
            .first()
            .getElementsByAttributeValueContaining("class", "category_tertiary_item");
    for (Element cat2Element : cat2s) {
      cat2Element = cat2Element.child(0);
      String cat2 = cat2Element.text().toUpperCase();
      if (!isValidCategory(cat2)) {
        continue;
      }

      String url = cat2Element.attr("href");
      if (url.contains("#")) {
        Element parent = cat2Element.parent();
        Element level3 = parent.getElementsByAttributeValueContaining("class", "level-3").first();
        url = level3.child(0).attr("href");
      }

      ClCategoryCrawler category =
          createCategory(cat1, cat2, null, null, null, url + "?start=0&sz=" + pageSize, false);
      if (category != null) {
        saveAllPagesOfCategory(categories, category);
      }
    }
  }

  private void parseSaleCategory(Element menu, Set<ClCategoryCrawler> categories, Set<String> cat1s)
      throws Exception {
    Elements cat1Elements =
        menu.getElementsByAttributeValueContaining("class", "nav_dropdown_category-item")
            .first()
            .getElementsByAttributeValue("class", "category_tertiary_item");
    for (Element cat1Element : cat1Elements) {
      cat1Element = cat1Element.child(0);
      String cat1 = cat1Element.text().toUpperCase();
      if (!cat1s.contains(cat1)) {
        continue;
      }

      ClCategoryCrawler category =
          createCategory(
              cat1,
              "OTHERS",
              null,
              null,
              null,
              cat1Element.attr("href") + "?start=0&sz=" + pageSize,
              false);
      if (category != null) {
        saveAllPagesOfCategory(categories, category);
      }
    }
  }

  private void saveAllPagesOfCategory(
      Set<ClCategoryCrawler> categories, ClCategoryCrawler category) {
    categories.add(category);
    logger.info(
        "Adding Category {}-{}-{}-{} : {}",
        brandCountry.getBrand().getName(),
        category.getCat1(),
        category.getCat2(),
        category.getCat3(),
        category.getUrl());

    try {
      Document doc =
          Jsoup.parse(
              UrlHelper.getPuppeteerResponseString(
                  category.getUrl(),
                  brandCountry.getWaitForElement(),
                  brandCountry.getRepeat(),
                  brandCountry.getId(),
                  PuppeteerAction.HTML,
                  true));

      Element countElement =
          doc.getElementsByAttributeValueContaining("class", "product_list-result_count")
              .first()
              .getElementsByTag("span")
              .first();
      if (countElement != null) {
        String countStr = countElement.text();
        int count = Integer.parseInt(CommonUtil.removeNonNumericChar(countStr));
        int start = 0;
        while (count >= pageSize) {
          start += pageSize;

          ClCategoryCrawler newCat =
              createCategory(
                  category.getCat1(),
                  category.getCat2(),
                  category.getCat3(),
                  null,
                  null,
                  category.getUrl().replace("start=0", "start=" + start),
                  false);
          if (newCat != null) {
            categories.add(newCat);
            logger.info(
                "Adding Category {}-{}-{}-{} : {}",
                brandCountry.getBrand().getName(),
                newCat.getCat1(),
                newCat.getCat2(),
                newCat.getCat3(),
                newCat.getUrl());
          }

          count -= pageSize;
        }
      }
    } catch (Exception e) {
      logger.error("No more pages for the category " + category.getUrl(), e);
    }
  }
}
