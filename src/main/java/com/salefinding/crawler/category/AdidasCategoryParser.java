package com.salefinding.crawler.category;

import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.RetryManageUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class AdidasCategoryParser extends BaseCategoryParser {

  private Logger logger = LoggerFactory.getLogger(AdidasCategoryParser.class);

  public AdidasCategoryParser(ClBrandCountry brandCountry) {
    super(brandCountry);
  }

  @Override
  public Set<ClCategoryCrawler> parse() {
    super.parse();

    Set<ClCategoryCrawler> categories = new HashSet<>();
    Set<String> cat1s = new HashSet<>();
    cat1s.add("MEN");
    cat1s.add("WOMEN");
    cat1s.add("KIDS");

    try {
      Document doc = getDoc();
      Element nav =
          doc.getElementById("header")
              .getElementsByAttributeValueContaining("class", "topcat-nav")
              .first();

      for (Element menu : nav.getElementsByAttributeValueContaining("class", "navigation-gender")) {
        String cat1 = menu.select("a.top-cat-link").first().text().toUpperCase();
        if (cat1s.contains(cat1)) {
          parseMenu(
              menu.getElementsByAttributeValueContaining("class", "main-line").first(),
              cat1,
              categories);
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (RetryManageUtil.canStillRetry(brandCountry.getStartUrl())) {
        return retryParse();
      } else {
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                new Exception("Cannot parsing Category for Adidas!"),
                brandCountry.getStartUrl());
      }
    }
    return categories;
  }

  private void parseMenu(Element menu, String cat1, Set<ClCategoryCrawler> categories)
      throws Exception {
    Elements cat2s = menu.getElementsByAttributeValueContaining("class", "col-5");
    for (Element cat2Element : cat2s) {
      String cat2 = cat2Element.select("div.headline").first().child(0).text();
      Elements cat3s = cat2Element.select("ul>li");
      for (Element cat3Element : cat3s) {
        if (cat3Element.children().size() > 0) {
          String cat3 = cat3Element.child(0).text();
          String href = cat3Element.child(0).attr("href");
          if (!href.startsWith(brandCountry.getStartUrl())) {
            href =
                brandCountry.getStartUrl().substring(0, brandCountry.getStartUrl().length() - 1)
                    + href;
          }
          if (href.contains("?")) {
            href = href + "&start=0";
          } else {
            href = href + "?start=0";
          }

          ClCategoryCrawler category = createCategory(cat1, cat2, cat3, null, null, href);
          if (category != null) {
            categories.add(category);
          }
        }
      }
    }
  }
}
