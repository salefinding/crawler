package com.salefinding.crawler;

import com.salefinding.crawler.transfer.TransferToAPIDB;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendMessagesApplication {

  private static Logger logger = LoggerFactory.getLogger(SendMessagesApplication.class);

  public static void main(String[] args) {
    try {
      logger.info("START TRANSFER TO API DB.");
      TransferToAPIDB.getInstance().transfer(200);
      logger.info("END TRANSFER TO API DB.");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
          .getErrorSummaryRepo()
          .saveError(Constants.RUN_UID, e);
    }

    logger.info("END program!");

    System.exit(0);
  }
}
