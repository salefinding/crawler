package com.salefinding.crawler.models;

import com.salefinding.crawler.helper.docker.PuppeteerDockerHelper;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.ProxyUtil;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class LocalPuppeteerModel extends PuppeteerModel {

  protected int index;

  public LocalPuppeteerModel(String name, String host, int port) {
    super(name, host, port);
  }

  public String getPuppeteerUrl(
      String targetUrl, String waitFor, Boolean repeat, PuppeteerAction action) {
    // String url = super.getPuppeteerUrl(targetUrl, action);
    String url = PuppeteerDockerHelper.get().getPuppeteerUrl(host, port, targetUrl, action);
    if (StringUtils.isNotBlank(targetUrl)) {
      ProxyModel proxy = getProxy();
      if (!PuppeteerDockerHelper.get().usingOpenVpnDocker()
          && proxy != null
          && StringUtils.isNotBlank(proxy.host)) {
        url =
            url
                + "&proxy="
                + URLEncoder.encode(
                    proxy.getProxyUrl().replace("127.0.0.1", Constants.localPuppeteerIp),
                    StandardCharsets.UTF_8);
        if (StringUtils.isNotBlank(proxy.getUsername())) {
          url = url + "&username=" + proxy.getUsername();
          url = url + "&password=" + proxy.getPassword();
        }
      }

      waitFor = waitFor.trim();
      if (StringUtils.isNotBlank(waitFor)) {
        if (StringUtils.isNumeric(waitFor)) {
          url = url + "&waitForTimeout=" + waitFor;
        } else {
          url = url + "&waitForSelector=" + URLEncoder.encode(waitFor, StandardCharsets.UTF_8);
        }
      }

      if (repeat != null && repeat) {
        url = url + "&repeat=true";
      }
    }
    return url;
  }

  public ProxyModel getProxy() {
    return ProxyUtil.get().getByIndex(index);
  }

  @Override
  public String toString() {
    return "LocalPuppeteerModel{"
        + "proxy="
        + getProxy()
        + ", host='"
        + host
        + '\''
        + ", port="
        + port
        + '}';
  }
}
