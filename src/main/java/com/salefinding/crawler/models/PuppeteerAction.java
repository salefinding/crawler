package com.salefinding.crawler.models;

public enum PuppeteerAction {
  HTML("/html"),
  URL("/url"),
  HEALTHCHECK("/healthcheck");

  String path;

  PuppeteerAction(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }
}
