package com.salefinding.crawler.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class NetworkModel {

  protected String name;

  protected String uid;

  protected String protocol;

  @EqualsAndHashCode.Include protected String host;

  @EqualsAndHashCode.Include protected int port;

  protected NetworkModel(String name, String host, int port) {
    this.name = name;
    this.host = host;
    this.port = port;
    this.uid = host + ":" + port;
  }

  public NetworkModel(String name, String protocol, String host, int port) {
    this.name = name;
    this.protocol = protocol;
    this.host = host;
    this.port = port;
  }

  @Override
  public String toString() {
    return "NetworkModel{"
        + "protocol='"
        + protocol
        + '\''
        + ", host='"
        + host
        + '\''
        + ", port="
        + port
        + '}';
  }
}
