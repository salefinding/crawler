package com.salefinding.crawler.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class PuppeteerModel extends NetworkModel {

  private boolean isHttps;

  public PuppeteerModel(String name, String host, int port) {
    super(name, host, port);
    isHttps = false;
  }

  public String getPuppeteerUrl(
      String targetUrl, String waitFor, Boolean repeat, PuppeteerAction action) {
    StringBuilder sb = new StringBuilder();

    if (isHttps) {
      sb.append("https://");
    } else {
      sb.append("http://");
    }

    sb.append(host);
    sb.append(":");
    sb.append(port);

    sb.append(action.getPath());

    if (StringUtils.isNotBlank(targetUrl)) {
      sb.append("/?url=");
      try {
        sb.append(URLEncoder.encode(targetUrl, StandardCharsets.UTF_8));
      } catch (Exception e) {
        sb.append(targetUrl);
      }

      waitFor = waitFor.trim();
      if (StringUtils.isNotBlank(waitFor)) {
        if (StringUtils.isNumeric(waitFor)) {
          sb.append("&waitForTimeout=").append(waitFor);
        } else {
          sb.append("&waitForSelector=").append(waitFor);
        }
      }

      if (repeat != null && repeat) {
        sb.append("&repeat=true");
      }
    }

    return sb.toString();
  }

  @Override
  public String toString() {
    return "PuppeteerModel{"
        + "isHttps="
        + isHttps
        + ", host='"
        + host
        + '\''
        + ", port="
        + port
        + '}';
  }
}
