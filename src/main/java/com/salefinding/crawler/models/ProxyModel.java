package com.salefinding.crawler.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@NoArgsConstructor
public class ProxyModel extends NetworkModel {

  private String username;

  private String password;

  private int pingTime;

  public ProxyModel(String name, String protocol, String host, int port) {
    super(name, protocol, host, port);
  }

  public String getProxyUrl() {
    if (StringUtils.isBlank(protocol)) {
      return String.format("%s:%s", host, port);
    }
    return String.format("%s://%s:%s", protocol, host, port);
  }

  @Override
  public String toString() {
    return "ProxyModel{"
        + "username='"
        + username
        + '\''
        + ", password='"
        + password
        + '\''
        + ", pingTime="
        + pingTime
        + ", protocol='"
        + protocol
        + '\''
        + ", host='"
        + host
        + '\''
        + ", port="
        + port
        + '}';
  }
}
