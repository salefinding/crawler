package com.salefinding.crawler.models;

import com.salefinding.models.crawler.ClBrandCountry;
import com.salefinding.models.crawler.ClCategoryCrawler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Item {

  @EqualsAndHashCode.Include protected Long brandCountryId;

  protected Long brandId;

  protected Integer subBrand;

  protected Long countryId;

  protected String name;

  @EqualsAndHashCode.Include protected String code;

  protected String image;

  protected String url;

  protected String parentUrl;

  protected ClCategoryCrawler categoryCrawler;

  protected Long categoryCrawlerId;

  protected Long categoryId;

  protected BigDecimal price;

  protected BigDecimal oldPrice;

  protected BigDecimal specialPrice;

  protected Integer saleRate;

  protected boolean isAvailable;

  protected Set<Stock> stockList;

  protected Set<Image> imageList;

  protected boolean errorWhileCrawling;

  public Item(ClBrandCountry brandCountry) {
    this.brandCountryId = brandCountry.getId();
    this.brandId = brandCountry.getBrandId();
    this.subBrand = brandCountry.getSubBrand();
    this.countryId = brandCountry.getCountryId();
    this.saleRate = 0;
    this.stockList = new HashSet<>();
    this.imageList = new HashSet<>();
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Stock {

    protected String colorCode;

    protected String sizeCode;

    protected Integer stock;

    protected BigDecimal price;

    protected BigDecimal oldPrice;

    protected BigDecimal specialPrice;

    public Stock(String colorCode, String sizeCode, Integer stock) {
      this.colorCode = colorCode;
      this.sizeCode = sizeCode;
      this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Stock stock = (Stock) o;
      return Objects.equals(colorCode, stock.colorCode) && Objects.equals(sizeCode, stock.sizeCode);
    }

    @Override
    public int hashCode() {
      return Objects.hash(colorCode, sizeCode);
    }
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class Image {

    protected String thumbUrl;

    protected String imgUrl;

    public Image(String url) {
      this.imgUrl = url;
      this.thumbUrl = url;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Image image = (Image) o;
      return Objects.equals(imgUrl, image.imgUrl);
    }

    @Override
    public int hashCode() {
      return Objects.hash(imgUrl);
    }
  }
}
