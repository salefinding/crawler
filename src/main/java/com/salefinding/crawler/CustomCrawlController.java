package com.salefinding.crawler;

import com.salefinding.crawler.models.ProxyModel;
import com.salefinding.crawler.utils.ProxyUtil;
import com.salefinding.models.crawler.ClBrandCountry;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomCrawlController extends CrawlController {

  private static final Logger logger = LoggerFactory.getLogger(CustomCrawlController.class);

  private final CrawlConfig config;

  private ClBrandCountry brandCountry;

  public CustomCrawlController(
      CrawlConfig config, RobotstxtServer robotstxtServer, ClBrandCountry brandCountry)
      throws Exception {
    super(config, null, robotstxtServer);
    this.config = config;
    this.brandCountry = brandCountry;
    this.pageFetcher = createPageFetcher();
  }

  @Override
  protected <T extends WebCrawler> void start(
      final WebCrawlerFactory<T> crawlerFactory, final int numberOfCrawlers, boolean isBlocking) {
    String threadNamePrefix =
        String.format(
            "%s-%s-Crawler-",
            brandCountry.getBrand().getShortCode(), brandCountry.getCountry().getShortCode());
    start(crawlerFactory, numberOfCrawlers, isBlocking, threadNamePrefix);
  }

  private PageFetcher createPageFetcher() {
    if (brandCountry.getFetcherClass() == null
        || !brandCountry.getFetcherClass().toLowerCase().contains("puppeteer")) {
      ProxyModel proxyModel = ProxyUtil.get().getModel(brandCountry.getId());
      config.setProxyHost(proxyModel.getHost());
      config.setProxyPort(proxyModel.getPort());
      config.setProxyUsername(proxyModel.getUsername());
      config.setProxyPassword(proxyModel.getPassword());
      logger.info("brand-country-id {} is using proxy {}.", brandCountry.getId(), proxyModel);
    }

    PageFetcher pageFetcher;
    try {
      pageFetcher = new PageFetcher(config);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      throw new RuntimeException(e);
    }

    try {
      if (StringUtils.isNotBlank(brandCountry.getFetcherClass())) {
        if (brandCountry.getFetcherClass().toLowerCase().contains("puppeteer")) {
          pageFetcher =
              (PageFetcher)
                  Class.forName(brandCountry.getFetcherClass())
                      .getConstructor(ClBrandCountry.class, CrawlConfig.class)
                      .newInstance(brandCountry, config);
        } else {
          pageFetcher =
              (PageFetcher)
                  Class.forName(brandCountry.getFetcherClass())
                      .getConstructor(CrawlConfig.class)
                      .newInstance(config);
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

    return pageFetcher;
  }
}
