package com.salefinding.crawler.helper.docker;

import com.salefinding.crawler.models.NetworkModel;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.*;
import com.salefinding.repositories.CrawlerRepoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class PuppeteerDockerHelper {

  private static final Logger logger = LoggerFactory.getLogger(PuppeteerDockerHelper.class);

  private long lastReloadTime = 0;

  private InstancesStrategy instancesStrategy;

  private boolean isRestarting = false;

  private Set<String> waitForRestartThreads = new HashSet<>();

  private static PuppeteerDockerHelper puppeteerDockerHelper = new PuppeteerDockerHelper();

  public static PuppeteerDockerHelper get() {
    return puppeteerDockerHelper;
  }

  public PuppeteerDockerHelper() {
    instancesStrategy = new InstancesStrategy3();
  }

  public Map<Integer, String> getPupIdMap() {
    return instancesStrategy.getPupIdMap();
  }

  public void scheduleReloadPuppeteerContainers() {
    ScheduleUtil.executor.scheduleWithFixedDelay(
        () -> restartPuppeteerContainers(false, true),
        PropUtil.getInt(Constants.RELOAD_PUPPETEER_INSTANCES_MINUTE, 60),
        PropUtil.getInt(Constants.RELOAD_PUPPETEER_INSTANCES_MINUTE, 60),
        TimeUnit.MINUTES);
  }

  public synchronized void restartPuppeteerContainers(boolean error, boolean lock) {
    /*// Only reload containers if now is greater than 10 minutes from last reload time.
    long curTime = System.currentTimeMillis();
    if (lastReloadTime != 0 && lastReloadTime + 20 * 60 * 1000 > curTime) {
      logger.info("NOT reload docker containers because {} + 60000 < {}", lastReloadTime, curTime);
      return;
    }*/
    isRestarting = true;
    if (lock) {
      PauseAllProcessesUtil.lock();
    }
    addThreadToWaitingList(Thread.currentThread().getName());
    try {
      if (error) {
        logger.error("Restarting docker containers.");
        CrawlerRepoFactory.getInstance(Constants.DB_CRAWLER)
            .getErrorSummaryRepo()
            .saveError(
                Constants.RUN_UID,
                "Restarting docker containers",
                null,
                Thread.currentThread().getName());
      }

      int size = getPupIdMap().size();
      int count = 0;
      while (waitForRestartThreads.size() < size && count < 10) {
        logger.info("Restarting containers. Waiting 6s for all threads ready ...");
        CommonUtil.sleep(6000);
        count++;
      }
      if (count == 10) {
        logger.info("Waited 60s, restart containers.");
      }
      instancesStrategy.restartContainers();

      lastReloadTime = System.currentTimeMillis();
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      try {
        instancesStrategy.removeAllContainers();
      } catch (Exception ex) {
        logger.error(ex.getMessage(), ex);
      }
      System.exit(-1);
    } finally {
      isRestarting = false;
      RetryManageUtil.resetAll();
      waitForRestartThreads.clear();
      if (lock) {
        PauseAllProcessesUtil.unLock();
      }
    }
  }

  public void startPuppeteerContainers(int size, boolean lock) {
    if (lock) {
      PauseAllProcessesUtil.lock();
    }

    try {
      instancesStrategy.startContainers(size);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      try {
        instancesStrategy.removeAllContainers();
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      System.exit(-1);
    } finally {
      if (lock) {
        PauseAllProcessesUtil.unLock();
      }
    }
  }

  public void removeAllPuppeteerContainers() {
    try {
      instancesStrategy.removeAllContainers();
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  public boolean usingOpenVpnDocker() {
    return instancesStrategy.usingOpenVpnDocker();
  }

  public String getPuppeteerUrl(String host, int port, String targetUrl, PuppeteerAction action) {
    return instancesStrategy.getPuppeteerUrl(host, port, targetUrl, action);
  }

  public int addThreadToWaitingList(String threadName) {
    if (isRestarting && !waitForRestartThreads.contains(threadName)) {
      logger.info("Add {} to waiting list.", threadName);
      waitForRestartThreads.add(threadName);
      return 1;
    }
    return 0;
  }

  public void restartContainer(NetworkModel model, boolean change) {
    instancesStrategy.restartContainer(model, change);
  }
}
