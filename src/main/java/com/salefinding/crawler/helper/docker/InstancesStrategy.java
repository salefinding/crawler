package com.salefinding.crawler.helper.docker;

import com.salefinding.crawler.models.NetworkModel;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.PropUtil;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class InstancesStrategy {

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  protected final Map<Integer, String> pupIdMap = new HashMap<>();

  protected final Map<String, String> containerIds = new ConcurrentHashMap<>();

  protected final String PUPPETEER_IMAGE = PropUtil.getString(Constants.DOCKER_PUPPETEER_IMAGE);

  protected final String DOCKER_URI = PropUtil.getString(Constants.DOCKER_URI);

  private boolean useOpenVpn = PropUtil.getBoolean(Constants.DOCKER_USE_OPENVPN, false);

  protected DockerClient docker;

  public InstancesStrategy() {
    try {
      docker = DefaultDockerClient.fromEnv().build();
    } catch (Exception e) {
      docker = DefaultDockerClient.builder().uri(DOCKER_URI).build();
    }
  }

  public abstract void startContainers(int size) throws Exception;

  public abstract void restartContainers() throws Exception;

  public abstract void removeAllContainers() throws Exception;

  public abstract String getPuppeteerUrl(
      String host, int port, String targetUrl, PuppeteerAction action);

  public void removeContainer(String containerId) {
    logger.info("Removing container {} ...", containerId);
    try {
      docker.removeContainer(containerId, DockerClient.RemoveContainerParam.forceKill());
      CommonUtil.sleep(1000);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  public void removeContainerByName(String name) {
    if (containerIds.containsKey(name)) {
      logger.info("Removing container {} ...", name);
      String containerId = containerIds.get(name);
      try {
        docker.removeContainer(containerId, DockerClient.RemoveContainerParam.forceKill());
        containerIds.remove(name);
        CommonUtil.sleep(1000);
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
      }
    } else {
      logger.info("Cannot remove container {} as not created.", name);
    }
  }

  public void restartContainer(NetworkModel model, boolean change) {}

  protected boolean usingOpenVpnDocker() {
    return useOpenVpn;
  }

  public Map<Integer, String> getPupIdMap() {
    return pupIdMap;
  }

  protected String createInstance(DockerClient docker, ContainerConfig containerConfig, String name)
      throws DockerException, InterruptedException {
    ContainerCreation creation = docker.createContainer(containerConfig, name);

    String id = creation.id();

    // Start container
    logger.info("Starting container {} ...", name);
    docker.startContainer(id);

    return id;
  }
}
