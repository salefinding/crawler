package com.salefinding.crawler.helper.docker;

import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.PropUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class SurfsharkProxyConfigHelper {

  public static final String postFix = ".prod.surfshark.com";

  private static final Queue<String> availableNames = new ConcurrentLinkedQueue<>();

  public static void init() {
    availableNames.clear();

    String nameList = PropUtil.getString(Constants.SURF_SHARK_KEY);
    String[] array = nameList.split(",");

    Map<String, Double> serverMap = new ConcurrentHashMap<>();

    ExecutorService executor =
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    for (String name : array) {
      executor.submit(
          () -> {
            double pingTime = CommonUtil.getPingTime(name + postFix, 5, 0);
            if (pingTime > 0 && pingTime < 100) {
              log.info(
                  String.format(
                      "Adding %s to ServerName list as pingTime is %.2f", name, pingTime));
              serverMap.put(name, pingTime);
            }
          });
    }
    CommonUtil.awaitTerminationAfterShutdown(executor);

    serverMap.entrySet().stream()
        .sorted((Map.Entry.comparingByValue()))
        .forEach(entry -> availableNames.add(entry.getKey()));
  }

  public static synchronized String getName() {
    if (availableNames.size() == 0) {
      init();
    }
    return availableNames.poll();
  }
}
