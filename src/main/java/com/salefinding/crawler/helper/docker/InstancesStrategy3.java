package com.salefinding.crawler.helper.docker;

import com.salefinding.crawler.models.NetworkModel;
import com.salefinding.crawler.models.ProxyModel;
import com.salefinding.crawler.models.PuppeteerAction;
import com.salefinding.crawler.utils.*;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;
import org.apache.commons.lang3.StringUtils;

import java.net.Proxy;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstancesStrategy3 extends InstancesStrategy {

  private static final String VPN_IMAGE = "ntanh1402/openvpn-privoxy:latest";

  private static final String PROXY_IMAGE = "shadowsocks/shadowsocks-libev:latest";

  @Override
  public void startContainers(int size) throws Exception {
    if (pupIdMap.size() == 0) {
      List<Container> list =
          docker.listContainers(DockerClient.ListContainersParam.allContainers());
      for (Container container : list) {
        String name = container.names() != null ? container.names().get(0) : "";
        if (name.contains(Constants.PUP_PREFIX) || name.contains(Constants.PROXY_PREFIX)) {
          removeContainer(container.id());
        }
      }
    } else {
      removeAllContainers();
    }

    docker.pull(PUPPETEER_IMAGE);
    docker.pull(PROXY_IMAGE);

    for (int i = 1; i <= size; ) {
      String id = startProxyContainer(docker, i, true, null);
      if (id != null) {
        i++;
      }
    }

    for (int i = 1; i <= size; i++) {
      startPuppeteerContainer(docker, i, true);
    }

    CommonUtil.sleep(Math.max(500 * size, 10000), logger);
  }

  public void restartContainer(NetworkModel model, boolean change) {
    String name = model.getName();
    try {
      removeContainerByName(name);
      if (name.startsWith(Constants.PROXY_PREFIX)) {
        int index =
            Integer.parseInt(name.substring(Constants.PROXY_PREFIX.length(), name.indexOf('-')));
        String newName =
            startProxyContainer(
                docker, index, false, change ? null : name.substring(name.indexOf('-') + 1));
        while (newName == null) {
          newName = startProxyContainer(docker, index, false, null);
        }
        model.setName(newName);
      } else {
        startPuppeteerContainer(
            docker, Integer.parseInt(name.substring(Constants.PUP_PREFIX.length())), false);
      }
    } catch (Exception e) {
      logger.error("Error while restarting container {}", name, e);
    }
  }

  @Override
  public void restartContainers() throws Exception {
    ProxyUtil.get().resetModelList();
    PuppeteerUtil.get().resetModelList();
    LocalPuppeteerUtil.get().resetModelList();
    SurfsharkProxyConfigHelper.init();

    startContainers(PropUtil.getInt(Constants.DOCKER_PUPPETEER_CONTAINER_SIZE, 20));

    ProxyUtil.get().reload();
    PuppeteerUtil.get().reload();
    LocalPuppeteerUtil.get().reload();
    ProxyUtil.get().loadModelList();
    PuppeteerUtil.get().loadModelList();
    LocalPuppeteerUtil.get().loadModelList();
  }

  @Override
  public void removeAllContainers() {
    for (String containerId : containerIds.values()) {
      removeContainer(containerId);
    }
    containerIds.clear();
    pupIdMap.clear();
  }

  @Override
  public String getPuppeteerUrl(String host, int port, String targetUrl, PuppeteerAction action) {
    StringBuilder sb = new StringBuilder();

    sb.append("http://").append(host).append(":").append(port);

    sb.append(action.getPath());

    if (StringUtils.isNotBlank(targetUrl)) {
      sb.append("/?url=").append(URLEncoder.encode(targetUrl, StandardCharsets.UTF_8));
    }

    return sb.toString();
  }

  protected boolean usingOpenVpnDocker() {
    return false;
  }

  private String startProxyContainer(
      DockerClient docker, int index, boolean addToList, String surfSharkServerName)
      throws DockerException, InterruptedException {
    HostConfig.Builder cfgBuilder = HostConfig.builder();

    if (surfSharkServerName == null) {
      surfSharkServerName = SurfsharkProxyConfigHelper.getName();
    }

    Map<String, List<PortBinding>> portBindings = new HashMap<>();
    portBindings.put(
        "1080",
        Collections.singletonList(
            PortBinding.of("0.0.0.0", Constants.STARTING_PROXY_PORT + index)));
    cfgBuilder.portBindings(portBindings);

    ContainerConfig containerConfig =
        ContainerConfig.builder()
            .hostConfig(cfgBuilder.build())
            .image(PROXY_IMAGE)
            .exposedPorts("1080")
            .cmd(
                "ss-local",
                "-s",
                surfSharkServerName + SurfsharkProxyConfigHelper.postFix,
                "-p",
                "50572",
                "-m",
                "aes-256-gcm",
                "-k",
                "RscxZj3bSSHSd9C2gqtgcJMg",
                "-b",
                "0.0.0.0",
                "-l",
                "1080",
                "-t",
                "60",
                "--fast-open")
            .build();
    String name = String.format("%s%d-%s", Constants.PROXY_PREFIX, index, surfSharkServerName);
    String id = createInstance(docker, containerConfig, name);
    CommonUtil.sleep(2000);

    ProxyModel proxy =
        new ProxyModel(
            name, Proxy.Type.SOCKS.name(), Constants.localPuppeteerIp, Constants.STARTING_PROXY_PORT + index);
    proxy.setPingTime(1);

    if (!ProxyUtil.get().isValid(proxy)) {
      logger.error("Proxy server {} is INVALID. Deleting...", name);
      removeContainer(id);
      return null;
    }

    if (addToList) {
      ProxyUtil.get().addModel(proxy);
      logger.info("Added proxy model {}.", name);
    }

    containerIds.put(name, id);

    return name;
  }

  private void startPuppeteerContainer(DockerClient docker, int index, boolean addToList)
      throws DockerException, InterruptedException {
    int hostPort = Constants.STARTING_PORT + index;

    HostConfig.Builder cfgBuilder = HostConfig.builder();

    Map<String, List<PortBinding>> portBindings = new HashMap<>();
    portBindings.put(
        "9000",
        Collections.singletonList(PortBinding.of("0.0.0.0", Constants.STARTING_PORT + index)));
    cfgBuilder.portBindings(portBindings);

    ContainerConfig containerConfig =
        ContainerConfig.builder()
            .hostConfig(cfgBuilder.build())
            .image(PUPPETEER_IMAGE)
            .tty(true)
            .stdinOnce(true)
            .exposedPorts("9000")
            .build();

    String id =
        createInstance(docker, containerConfig, String.format("%s%d", Constants.PUP_PREFIX, index));
    if (addToList) {
      pupIdMap.put(hostPort, id);
    }

    containerIds.put(Constants.PUP_PREFIX + index, id);
  }

  /*
  private String startVpnContainer(DockerClient docker, int index)
      throws DockerException, InterruptedException {
    HostConfig.Builder cfgBuilder = HostConfig.builder();

    String hostPath =
        System.getProperty("user.dir")
            + "/openvpn/"
            + SurfsharkProxyConfigHelper.getName(index - 1);
    new File(hostPath).mkdirs();
    createAuthFile(hostPath);
    createOVPNConfigFile(hostPath, SurfsharkProxyConfigHelper.getName(index - 1));
    logger.info("Container host path: {}", hostPath);
    cfgBuilder.appendBinds(hostPath + ":/app/ovpn/config");
    cfgBuilder.appendBinds("/etc/localtime:/etc/caltime:ro");
    cfgBuilder.capAdd("NET_ADMIN");

    Map<String, List<PortBinding>> portBindings = new HashMap<>();
    portBindings.put(
        "8080",
        Collections.singletonList(
            PortBinding.of("0.0.0.0", Constants.STARTING_PROXY_PORT + index)));
    cfgBuilder.portBindings(portBindings);

    cfgBuilder.devices(
        Device.builder()
            .pathOnHost("/dev/net/tun")
            .pathInContainer("/dev/net/tun")
            .cgroupPermissions("rwm")
            .build());
    cfgBuilder.dns("8.8.8.8", "1.1.1.1");

    ContainerConfig containerConfig =
        ContainerConfig.builder()
            .hostConfig(cfgBuilder.build())
            .image(VPN_IMAGE)
            .tty(true)
            .stdinOnce(true)
            .exposedPorts("8080")
            .build();

    String id =
        createInstance(
            docker, containerConfig, String.format("%s%d", Constants.PROXY_PREFIX, index));
    containerIds.put(Constants.PROXY_PREFIX + index, id);

    ProxyModel proxy =
        new ProxyModel(
            Constants.PROXY_PREFIX + index,
            "http",
            "127.0.0.1",
            Constants.STARTING_PROXY_PORT + index);
    proxy.setPingTime(1);
    ProxyUtil.get().addModel(proxy);

    return id;
  }
  private void createAuthFile(String hostPath) {
    String authFilePath = hostPath + "/auth";
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(authFilePath, false))) {
      writer.write("jKRNuBwFrdrg6KSKfXfgxneN\n");
      writer.write("CMuVB4MbmuyDrMqkk7thB7zk\n");
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
    }
  }

  private void createOVPNConfigFile(String hostPath, String serverName) {
    String authFilePath = hostPath + "/config.ovpn";
    String content =
        "client\n"
            + "dev tun\n"
            + "proto tcp\n"
            + "remote "
            + serverName
            + ".prod.surfshark.com 1443\n"
            + "resolv-retry infinite\n"
            + "remote-random\n"
            + "nobind\n"
            + "tun-mtu 1500\n"
            + "tun-mtu-extra 32\n"
            + "mssfix 1450\n"
            + "persist-key\n"
            + "persist-tun\n"
            + "ping 15\n"
            + "ping-restart 0\n"
            + "ping-timer-rem\n"
            + "reneg-sec 0\n"
            + "\n"
            + "remote-cert-tls server\n"
            + "\n"
            + "auth-user-pass /app/ovpn/config/auth\n"
            + "\n"
            + "#comp-lzo\n"
            + "verb 3\n"
            + "pull\n"
            + "fast-io\n"
            + "cipher AES-256-CBC\n"
            + "\n"
            + "auth SHA512\n"
            + "\n"
            + "<ca>\n"
            + "-----BEGIN CERTIFICATE-----\n"
            + "MIIFTTCCAzWgAwIBAgIJAMs9S3fqwv+mMA0GCSqGSIb3DQEBCwUAMD0xCzAJBgNV\n"
            + "BAYTAlZHMRIwEAYDVQQKDAlTdXJmc2hhcmsxGjAYBgNVBAMMEVN1cmZzaGFyayBS\n"
            + "b290IENBMB4XDTE4MDMxNDA4NTkyM1oXDTI4MDMxMTA4NTkyM1owPTELMAkGA1UE\n"
            + "BhMCVkcxEjAQBgNVBAoMCVN1cmZzaGFyazEaMBgGA1UEAwwRU3VyZnNoYXJrIFJv\n"
            + "b3QgQ0EwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDEGMNj0aisM63o\n"
            + "SkmVJyZPaYX7aPsZtzsxo6m6p5Wta3MGASoryRsBuRaH6VVa0fwbI1nw5ubyxkua\n"
            + "Na4v3zHVwuSq6F1p8S811+1YP1av+jqDcMyojH0ujZSHIcb/i5LtaHNXBQ3qN48C\n"
            + "c7sqBnTIIFpmb5HthQ/4pW+a82b1guM5dZHsh7q+LKQDIGmvtMtO1+NEnmj81BAp\n"
            + "FayiaD1ggvwDI4x7o/Y3ksfWSCHnqXGyqzSFLh8QuQrTmWUm84YHGFxoI1/8AKdI\n"
            + "yVoB6BjcaMKtKs/pbctk6vkzmYf0XmGovDKPQF6MwUekchLjB5gSBNnptSQ9kNgn\n"
            + "TLqi0OpSwI6ixX52Ksva6UM8P01ZIhWZ6ua/T/tArgODy5JZMW+pQ1A6L0b7egIe\n"
            + "ghpwKnPRG+5CzgO0J5UE6gv000mqbmC3CbiS8xi2xuNgruAyY2hUOoV9/BuBev8t\n"
            + "tE5ZCsJH3YlG6NtbZ9hPc61GiBSx8NJnX5QHyCnfic/X87eST/amZsZCAOJ5v4EP\n"
            + "SaKrItt+HrEFWZQIq4fJmHJNNbYvWzCE08AL+5/6Z+lxb/Bm3dapx2zdit3x2e+m\n"
            + "iGHekuiE8lQWD0rXD4+T+nDRi3X+kyt8Ex/8qRiUfrisrSHFzVMRungIMGdO9O/z\n"
            + "CINFrb7wahm4PqU2f12Z9TRCOTXciQIDAQABo1AwTjAdBgNVHQ4EFgQUYRpbQwyD\n"
            + "ahLMN3F2ony3+UqOYOgwHwYDVR0jBBgwFoAUYRpbQwyDahLMN3F2ony3+UqOYOgw\n"
            + "DAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAgEAn9zV7F/XVnFNZhHFrt0Z\n"
            + "S1Yqz+qM9CojLmiyblMFh0p7t+Hh+VKVgMwrz0LwDH4UsOosXA28eJPmech6/bjf\n"
            + "ymkoXISy/NUSTFpUChGO9RabGGxJsT4dugOw9MPaIVZffny4qYOc/rXDXDSfF2b+\n"
            + "303lLPI43y9qoe0oyZ1vtk/UKG75FkWfFUogGNbpOkuz+et5Y0aIEiyg0yh6/l5Q\n"
            + "5h8+yom0HZnREHhqieGbkaGKLkyu7zQ4D4tRK/mBhd8nv+09GtPEG+D5LPbabFVx\n"
            + "KjBMP4Vp24WuSUOqcGSsURHevawPVBfgmsxf1UCjelaIwngdh6WfNCRXa5QQPQTK\n"
            + "ubQvkvXONCDdhmdXQccnRX1nJWhPYi0onffvjsWUfztRypsKzX4dvM9k7xnIcGSG\n"
            + "EnCC4RCgt1UiZIj7frcCMssbA6vJ9naM0s7JF7N3VKeHJtqe1OCRHMYnWUZt9vrq\n"
            + "X6IoIHlZCoLlv39wFW9QNxelcAOCVbD+19MZ0ZXt7LitjIqe7yF5WxDQN4xru087\n"
            + "FzQ4Hfj7eH1SNLLyKZkA1eecjmRoi/OoqAt7afSnwtQLtMUc2bQDg6rHt5C0e4dC\n"
            + "LqP/9PGZTSJiwmtRHJ/N5qYWIh9ju83APvLm/AGBTR2pXmj9G3KdVOkpIC7L35dI\n"
            + "623cSEC3Q3UZutsEm/UplsM=\n"
            + "-----END CERTIFICATE-----\n"
            + "</ca>\n"
            + "key-direction 1\n"
            + "<tls-auth>\n"
            + "#\n"
            + "# 2048 bit OpenVPN static key\n"
            + "#\n"
            + "-----BEGIN OpenVPN Static key V1-----\n"
            + "b02cb1d7c6fee5d4f89b8de72b51a8d0\n"
            + "c7b282631d6fc19be1df6ebae9e2779e\n"
            + "6d9f097058a31c97f57f0c35526a44ae\n"
            + "09a01d1284b50b954d9246725a1ead1f\n"
            + "f224a102ed9ab3da0152a15525643b2e\n"
            + "ee226c37041dc55539d475183b889a10\n"
            + "e18bb94f079a4a49888da566b9978346\n"
            + "0ece01daaf93548beea6c827d9674897\n"
            + "e7279ff1a19cb092659e8c1860fbad0d\n"
            + "b4ad0ad5732f1af4655dbd66214e552f\n"
            + "04ed8fd0104e1d4bf99c249ac229ce16\n"
            + "9d9ba22068c6c0ab742424760911d463\n"
            + "6aafb4b85f0c952a9ce4275bc821391a\n"
            + "a65fcd0d2394f006e3fba0fd34c4bc4a\n"
            + "b260f4b45dec3285875589c97d3087c9\n"
            + "134d3a3aa2f904512e85aa2dc2202498\n"
            + "-----END OpenVPN Static key V1-----\n"
            + "</tls-auth>\n";
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(authFilePath, false))) {
      writer.write(content);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
    }
  }*/
}
