package com.salefinding.crawler.helper;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.ManagedInstance;
import com.salefinding.crawler.utils.CommonUtil;
import com.salefinding.crawler.utils.Constants;
import com.salefinding.crawler.utils.PropUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GCloudComputeHelper {

  private static final Logger logger = LoggerFactory.getLogger(GCloudComputeHelper.class);

  private static String PROJECT = PropUtil.getString(Constants.GCLOUD_PROJECT);

  private Compute compute;

  private static GCloudComputeHelper gCloudCompute = new GCloudComputeHelper();

  public static GCloudComputeHelper getInstance() {
    return gCloudCompute;
  }

  public GCloudComputeHelper() {
    // initComputeAPI();
  }

  public static void main(String[] args) {
    GCloudComputeHelper.getInstance()
        .resizeGroupInstance(2, "asia-southeast1-a", "puppeteer-group");
    System.out.println(GCloudComputeHelper.getInstance().getIPListOfInstances("puppeteer-group"));

    GCloudComputeHelper.getInstance()
        .resizeGroupInstance(0, "asia-southeast1-a", "puppeteer-group");
    System.out.println(GCloudComputeHelper.getInstance().getIPListOfInstances("puppeteer-group"));
  }

  private void initComputeAPI() {
    try {
      String tokenFilePath = PropUtil.getString(Constants.GCLOUD_KEY);

      HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      JacksonFactory jacksonFactory = JacksonFactory.getDefaultInstance();
      GoogleCredential credential =
          GoogleCredential.fromStream(new FileInputStream(tokenFilePath))
              .createScoped(
                  Arrays.asList(
                      "https://www.googleapis.com/auth/compute",
                      "https://www.googleapis.com/auth/logging.write",
                      "https://www.googleapis.com/auth/servicecontrol",
                      "https://www.googleapis.com/auth/service.management.readonly",
                      "https://www.googleapis.com/auth/taskqueue"));

      compute = new Compute.Builder(httpTransport, jacksonFactory, credential).build();
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  public synchronized List<String> getIPListOfInstances(String groupName) {
    List<String> ipList = new ArrayList<>();

    /*try {
        InstanceList list = compute.instances().list(PROJECT, PropUtil.getString(Constants.GCLOUD_ZONE_NAME)).execute();

        if (list.getItems() != null) {
            for (Instance instance : list.getItems()) {
                if ("RUNNING".equalsIgnoreCase(instance.getStatus())) {
                    if (StringUtils.isNotBlank(groupName) && instance.getName().startsWith(groupName)) {
                        ipList.add(instance.getNetworkInterfaces().get(0).getAccessConfigs().get(0).getNatIP());
                    }
                }
            }
        }
    } catch (IOException e) {
        logger.error(e.getMessage(), e);
    }*/

    return ipList;
  }

  public void resizeGroupInstance(int newSize, String zoneName, String groupName) {
    try {
      compute.instanceGroupManagers().resize(PROJECT, zoneName, groupName, newSize).execute();
      CommonUtil.sleep(5000, logger);

      boolean finished = false;
      while (!finished) {
        finished = true;
        List<ManagedInstance> list =
            compute
                .instanceGroupManagers()
                .listManagedInstances(PROJECT, zoneName, groupName)
                .execute()
                .getManagedInstances();
        if (list != null) {
          for (ManagedInstance instance : list) {
            if (!"NONE".equalsIgnoreCase(instance.getCurrentAction())) {
              finished = false;
            }
          }
          logger.info("Waiting for resizing group '{}' to {} instances ...", groupName, newSize);
          CommonUtil.sleep(1000);
        }
      }
      if (newSize > 0) {
        logger.info("Waiting 20 seconds for starting Puppeteer ...");
        CommonUtil.sleep(20000);
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }
}
