lazy val commonSettings = Seq(
  organization := "com.salefinding",
  version := "0.1.0-SNAPSHOT"
)

packMain := Map(
  "crawler" -> "com.salefinding.crawler.CrawlerApplication",
  "transfer" -> "com.salefinding.crawler.TransferToApiDBApplication",
  "transferToWp" -> "com.salefinding.crawler.TransferToWpApplication"
)

packJvmOpts := Map(
  "crawler" -> Seq("-Djavax.net.ssl.trustStore=cacerts -Djavax.net.ssl.trustStorePassword=password -javaagent:lib/ebean-agent-11.38.1.jar"),
  "transfer" -> Seq("-Djavax.net.ssl.trustStore=cacerts -Djavax.net.ssl.trustStorePassword=password -javaagent:lib/ebean-agent-11.38.1.jar"),
  "transferToWp" -> Seq("-Djavax.net.ssl.trustStore=cacerts -Djavax.net.ssl.trustStorePassword=password -javaagent:lib/ebean-agent-11.38.1.jar")
)

packResourceDir += (baseDirectory.value / "config" -> "")

//resolvers += "Boundless repository" at "https://repo.boundlessgeo.com/main/"
resolvers += Resolver.mavenLocal

lazy val models = ProjectRef(file("model"), "models")

lazy val crawler = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "crawler",
    libraryDependencies ++= Seq(
      "mysql" % "mysql-connector-java" % "8.0.16",

      "edu.uci.ics" % "crawler4j" % "4.5.0-SNAPSHOT",
      "com.sleepycat" % "je" % "18.3.12",

      "org.jsoup" % "jsoup" % "1.12.1",
      "com.squareup.okhttp3" % "okhttp" % "3.14.1",
      "com.spotify" % "docker-client" % "8.16.0",
      "com.google.apis" % "google-api-services-compute" % "v1-rev213-1.25.0",
      "com.cloudinary" % "cloudinary-http44" % "1.22.1",
      "com.hierynomus" % "sshj" % "0.27.0"
    )
  )
  .enablePlugins(PackPlugin)
  .aggregate(models)
  .dependsOn(models)